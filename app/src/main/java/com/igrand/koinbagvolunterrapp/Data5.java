package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

class Data5 {


    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("surname")
    @Expose
    public String surname;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("middle_name")
    @Expose
    public String middleName;
    @SerializedName("relation")
    @Expose
    public String relation;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("photo")
    @Expose
    public String photo;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;
    @SerializedName("tenth_marks")
    @Expose
    public String tenthMarks;
    @SerializedName("inter_marks")
    @Expose
    public String interMarks;
    @SerializedName("degree_marks")
    @Expose
    public String degreeMarks;
    @SerializedName("contact_name")
    @Expose
    public String contactName;
    @SerializedName("contact_type")
    @Expose
    public String contactType;
    @SerializedName("contact_number")
    @Expose
    public String contactNumber;
    @SerializedName("date_of_birth")
    @Expose
    public String dateOfBirth;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("district")
    @Expose
    public String district;
    @SerializedName("revenue_devision")
    @Expose
    public String revenueDevision;
    @SerializedName("mandal")
    @Expose
    public String mandal;
    @SerializedName("village")
    @Expose
    public String village;
    @SerializedName("addressline1")
    @Expose
    public String addressline1;
    @SerializedName("addressline2")
    @Expose
    public String addressline2;
    @SerializedName("addressline3")
    @Expose
    public String addressline3;
    @SerializedName("pancard")
    @Expose
    public String pancard;
    @SerializedName("panphoto")
    @Expose
    public String panphoto;
    @SerializedName("aadharcard")
    @Expose
    public String aadharcard;
    @SerializedName("aadharphoto")
    @Expose
    public String aadharphoto;
    @SerializedName("tenth_photo")
    @Expose
    public String tenthPhoto;
    @SerializedName("inter_photo")
    @Expose
    public String interPhoto;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("title", title).append("firstName", firstName).append("surname", surname).append("lastName", lastName).append("middleName", middleName).append("relation", relation).append("email", email).append("photo", photo).append("mobileNumber", mobileNumber).append("tenthMarks", tenthMarks).append("interMarks", interMarks).append("degreeMarks", degreeMarks).append("contactName", contactName).append("contactType", contactType).append("contactNumber", contactNumber).append("dateOfBirth", dateOfBirth).append("state", state).append("district", district).append("revenueDevision", revenueDevision).append("mandal", mandal).append("village", village).append("addressline1", addressline1).append("addressline2", addressline2).append("addressline3", addressline3).append("pancard", pancard).append("panphoto", panphoto).append("aadharcard", aadharcard).append("aadharphoto", aadharphoto).append("tenthPhoto", tenthPhoto).append("interPhoto", interPhoto).toString();
    }
}
