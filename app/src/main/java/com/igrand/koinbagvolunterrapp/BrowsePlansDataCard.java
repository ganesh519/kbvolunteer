package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.APIClient1;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.igrand.koinbagvolunterrapp.Activities.AppController.TAG;

public class BrowsePlansDataCard extends BaseActivity {

    ImageView back2;
    ApiInterface apiInterface;
    Bundle bundle;
    TextView type1,circle1;
    RecyclerView recycler_databrowse;
    BrowseRecyclerAdapter1 allRecyclerAdapter1;
    ArrayList<TopupResponse> topupResponseArrayList=new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_plans_data_card);

        type1=findViewById(R.id.type1);
        circle1=findViewById(R.id.circle1);
        recycler_databrowse=findViewById(R.id.recycler_databrowse);


        String APIID = "AP135358";
        String PASSWORD = "ki832h74td3ff";
        String Operator_Code = getIntent().getStringExtra("Operator");
        String Circle_Code = getIntent().getStringExtra("Circle");
        final String MobileNumber = getIntent().getStringExtra("Mobile");
        String Type = getIntent().getStringExtra("Type");
        String Circle = getIntent().getStringExtra("State");
        String PageID = "0";
        String FORMAT = "JSON/XML";



        type1.setText(Type);
        circle1.setText(Circle);



        back2 = findViewById(R.id.back2);
        back2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(BrowsePlans.this,OperatorActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);*/

                finish();
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(BrowsePlansDataCard.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();


        apiInterface = APIClient1.getClient().create(ApiInterface.class);
        Call<BrowseFetch> call = apiInterface.browseplans(APIID, PASSWORD, Operator_Code, Circle_Code, MobileNumber, PageID, FORMAT);
        call.enqueue(new Callback<BrowseFetch>() {
            @Override
            public void onResponse(Call<BrowseFetch> call, Response<BrowseFetch> response) {

                if (response.isSuccessful()) ;
                BrowseFetch operatorFetch = response.body();

                if (response.code() == 200) {
                    progressDialog.dismiss();

                    List<PlanDescription> planDescriptions = operatorFetch.planDescription;

                    if(planDescriptions!=null){

                        for (int i=0; i< planDescriptions.size();i++) {

                            if(planDescriptions.get(i).rechargeType.equals("4G"))


                            {

                                Log.e(TAG, "onResponse:"+planDescriptions.get(i).id );

                                topupResponseArrayList.add(new TopupResponse(planDescriptions.get(i).rechargeShortDesc,planDescriptions.get(i).rechargeAmount,
                                        planDescriptions.get(i).rechargeTalktime,planDescriptions.get(i).rechargeValidity,planDescriptions.get(i).rechargeLongDesc));

                                allRecyclerAdapter1 = new BrowseRecyclerAdapter1(BrowsePlansDataCard.this, topupResponseArrayList, MobileNumber);

                            } else {
                                Toast.makeText(BrowsePlansDataCard.this, "No Data...", Toast.LENGTH_SHORT).show();
                            }

                        }
                        recycler_databrowse.setLayoutManager(new LinearLayoutManager(BrowsePlansDataCard.this));
                        recycler_databrowse.setAdapter(allRecyclerAdapter1);
                        Toast.makeText(BrowsePlansDataCard.this, "Success..", Toast.LENGTH_SHORT).show();

                    } else
                    {
                        Toast.makeText(BrowsePlansDataCard.this, "No Data Found...", Toast.LENGTH_SHORT).show();
                    }





                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    //Toast.makeText(MobileRechargeActivity.this, "Volunteer Not Exist ,Please Contact Koinbag Ngo..", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<BrowseFetch> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(BrowsePlansDataCard.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });





    }
}
