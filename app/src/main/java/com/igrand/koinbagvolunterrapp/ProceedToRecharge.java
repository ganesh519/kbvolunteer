package com.igrand.koinbagvolunterrapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.igrand.koinbagvolunterrapp.Client.APIClient1;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProceedToRecharge extends BaseActivity {

    Button proceedtorecharge;
    TextView changeoperator, browseplans, fname, sname, umob, type, state, longde, otype,comma,dash;
    ImageView back5, imgnet;
    ApiInterface apiInterface;
    PrefManager prefManager;
    EditText editamount;
    String Id, f_name, surname, mobile1, walletbals, userid, photo, Operator_Code, Circle_Code, oCODE, cCode, LongDesc, Type, Circle,
            amount, Type_Id, operator, circle_code, recharge_type, MobileNumber, Typee, operator_image, operator_name, operator_type, circle_name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proceed_to_recharge);

        changeoperator = findViewById(R.id.changeoperator);
        fname = findViewById(R.id.fname);
        sname = findViewById(R.id.sname);
        browseplans = findViewById(R.id.browseplans);
        umob = findViewById(R.id.umob);
        type = findViewById(R.id.type);
        state = findViewById(R.id.stateid);
        editamount = findViewById(R.id.editamount);
        longde = findViewById(R.id.longde);
        otype = findViewById(R.id.otype);
        imgnet = findViewById(R.id.imgnet);
        comma = findViewById(R.id.comma);
        dash = findViewById(R.id.dash);




        if (getIntent() != null) {

            Operator_Code = getIntent().getStringExtra("Operator");
            Circle_Code = getIntent().getStringExtra("Circle");
            operator_name = getIntent().getStringExtra("Operatorname");
            operator_type = getIntent().getStringExtra("Operatortype");
            operator_image = getIntent().getStringExtra("Operatorimage");
            circle_name = getIntent().getStringExtra("CircleName");



            Picasso.get().load(operator_image).error(R.drawable.kb).into(imgnet);

            type.setText(operator_name);
            otype.setText(operator_type);
            state.setText(circle_name);


        }

        Typee = getIntent().getStringExtra("Type");

        amount = getIntent().getStringExtra("Operate");
        LongDesc = getIntent().getStringExtra("LongDesc");


        prefManager = new PrefManager(ProceedToRecharge.this);

        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");
        f_name = profile.get("f_name");
        surname = profile.get("Surname");
        mobile1 = profile.get("Mobile");
        photo = profile.get("Photo");
        userid = profile.get("Userid");
        walletbals = profile.get("Walletbals");




        MobileNumber = getIntent().getStringExtra("Mobile");

        umob.setText(MobileNumber);
        fname.setText(f_name);
        sname.setText(surname);
        editamount.setText(amount);
        longde.setText(LongDesc);

        /*otype.setText(operator_type);
        type.setText(operator_name);*/


        //getprepaid(Typee);


        final ProgressDialog progressDialog = new ProgressDialog(ProceedToRecharge.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Operators> call = apiInterface.operators();
        call.enqueue(new Callback<Operators>() {
            @Override
            public void onResponse(Call<Operators> call, Response<Operators> response) {
                // if (response.isSuccessful()) ;
                Operators walletResponse1 = response.body();
                if (walletResponse1 != null) {


                    StatusOperator statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog.dismiss();

                        comma.setVisibility(View.VISIBLE);
                        dash.setVisibility(View.VISIBLE);


                        List<DataOperator> data7 = walletResponse1.data;

                        for (int i = 0; i <= data7.size(); i++) {


                            if(Operator_Code.equals("API Not Provided")) {

                                Toast.makeText(ProceedToRecharge.this, "API Not Provided", Toast.LENGTH_SHORT).show();
                            } else {

                                if (data7.get(i).operatorCode.equals(Operator_Code)) {


                                    type.setText(data7.get(i).operatorName);
                                    otype.setText(data7.get(i).operatorType);
                                    Type_Id = data7.get(i).operatorType;
                                    recharge_type = data7.get(i).operatorType;

                                    if(data7.get(i).image.equals("")) {

                                        imgnet.setImageResource(R.drawable.kb);


                                    } else {

                                        Picasso.get().load(data7.get(i).image).error(R.drawable.kb).into(imgnet);
                                    }




                                    Type = type.getText().toString();
                                    Circle = state.getText().toString();

                                    operator = data7.get(i).operatorCode + "@" + data7.get(i).operatorName;

                                    break;
                                }


                            }



                        }

                    } else if (statusDataResponse7.code != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(ProceedToRecharge.this, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(ProceedToRecharge.this, "No Data", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<Operators> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast = Toast.makeText(ProceedToRecharge.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });


        final ProgressDialog progressDialog1 = new ProgressDialog(ProceedToRecharge.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Circles> call1 = apiInterface.circles();
        call1.enqueue(new Callback<Circles>() {
            @Override
            public void onResponse(Call<Circles> call, Response<Circles> response) {
                // if (response.isSuccessful()) ;
                Circles walletResponse1 = response.body();
                if (walletResponse1 != null) {


                    StatusCircle statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog1.dismiss();


                        List<DataCircle> data7 = walletResponse1.data;

                        if(Circle_Code.equals("API Not Provided")) {

                            Toast.makeText(ProceedToRecharge.this, "API Not Provided", Toast.LENGTH_SHORT).show();
                        }  else {

                            for (int i = 0; i <= data7.size(); i++) {

                                String circlecode = data7.get(i).circleCode;

                                circle_code = circlecode + "@" + data7.get(i).circleName;



                                if (circlecode.equals(Circle_Code)) {

                                    state.setText(data7.get(i).circleName);

                                    break;
                                }


                            }


                        }



                    } else if (statusDataResponse7.code != 200) {
                        progressDialog1.dismiss();
                        Toast.makeText(ProceedToRecharge.this, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                } else {
                    progressDialog1.dismiss();
                    Toast.makeText(ProceedToRecharge.this, "No Data", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<Circles> call, Throwable t) {
                progressDialog1.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast = Toast.makeText(ProceedToRecharge.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });


        changeoperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getprepaid(Typee);
                Intent intent = new Intent(ProceedToRecharge.this, OperatorActivity.class);
                intent.putExtra("TYPEE", Typee);
                intent.putExtra("Mobile", MobileNumber);

                startActivity(intent);
            }
        });
        browseplans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(ProceedToRecharge.this, BrowsePlans1.class);
                intent.putExtra("Operator", Operator_Code);
                intent.putExtra("Circle", Circle_Code);
                intent.putExtra("Mobile", MobileNumber);
                intent.putExtra("Type", Type);
                intent.putExtra("State", Circle);

                startActivity(intent);
            }
        });

        back5 = findViewById(R.id.back5);
        back5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(ProceedToRecharge.this,BrowsePlans.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);*/

                finish();
            }
        });


        proceedtorecharge = findViewById(R.id.proceedtorecharge);
        proceedtorecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount = editamount.getText().toString();

                if (amount.equals("") || amount.equals("0")) {

                    Toast.makeText(ProceedToRecharge.this, "Please Enter Amount", Toast.LENGTH_SHORT).show();
                } else {

                    Intent intent = new Intent(ProceedToRecharge.this, PaymentMobileRechargeActivity.class);
                    //intent.putExtra("Amount",AMount);
                    intent.putExtra("Amount", amount);
                    intent.putExtra("Operator", operator);
                    intent.putExtra("CircleCode", circle_code);
                    intent.putExtra("RechargeType", recharge_type);
                    intent.putExtra("MobileNumber", MobileNumber);

                    startActivity(intent);


                }

               /* BottomSheetDialog dialog = new BottomSheetDialog(ProceedToRecharge.this);
                dialog.setContentView(R.layout.activity_alert_dialog);
                //dialog.show();
                new CustomBottomSheetDialogFragment2().show(getSupportFragmentManager(), "Dialog");*/
            }
        });

    }


}
