package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class WaterConsumerDetails extends BaseActivity {

    Button proceedpaywater;
    ImageView backblack2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_consumer_details);

        proceedpaywater=findViewById(R.id.proceedpaywater);
        proceedpaywater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(WaterConsumerDetails.this,Payment.class);
                startActivity(intent);
            }
        });
        backblack2=findViewById(R.id.backblack2);
        backblack2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(WaterConsumerDetails.this,WaterBill.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
