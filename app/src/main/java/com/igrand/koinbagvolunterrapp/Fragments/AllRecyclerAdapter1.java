package com.igrand.koinbagvolunterrapp.Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.Data7;
import com.igrand.koinbagvolunterrapp.NavigationDrawerDashboard;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.ProfileImage;
import com.igrand.koinbagvolunterrapp.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllRecyclerAdapter1  extends RecyclerView.Adapter<AllRecyclerAdapter1.Holder> {

    List<Data7> data7;
    Context context;
    ApiInterface apiInterface;
    String id;
    TextView textView;
    PrefManager prefManager;

    public AllRecyclerAdapter1(Context context, List<Data7> data7) {

        this.context=context;
        this.data7=data7;
    }

    @NonNull
    @Override
    public AllRecyclerAdapter1.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview10, viewGroup, false);

        return new Holder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final AllRecyclerAdapter1.Holder holder, final int i) {

        String status1=data7.get(i).status;

        if(status1.equals("2")) {
            holder.reject1.setTextColor(Color.parseColor("#1380BE"));
            holder.reject_amount.setVisibility(View.GONE);
            holder.money1.setText(data7.get(i).amount);
            holder.request_date.setText(data7.get(i).requestedDate);
            holder.update_date.setVisibility(View.GONE);
            holder.rs.setVisibility(View.GONE);
            holder.rejectimage1.setVisibility(View.GONE);
            holder.reject1.setText("Waiting for Approval");

        }   else if (status1.equals("1")) {

            holder.reject_amount.setVisibility(View.VISIBLE);
            holder.rejectimage1.setVisibility(View.VISIBLE);
            holder.rejectimage1.setImageResource(R.drawable.approved);
            holder.reject1.setText("Approved Money");
            holder.reject1.setTextColor(Color.parseColor("#00A219"));
            holder.reject_amount.setText(data7.get(i).amount);
            holder.money1.setText(data7.get(i).amount);
            holder.request_date.setText(data7.get(i).requestedDate);
            holder.update_date.setVisibility(View.VISIBLE);
            holder.rs.setVisibility(View.VISIBLE);
            holder.update_date.setText(data7.get(i).updatedDate);

        }
        else if (status1.equals("0")) {
            holder.reject_amount.setVisibility(View.VISIBLE);
            holder.rejectimage1.setVisibility(View.VISIBLE);
            holder.reject1.setText("Rejected");
            holder.rejectimage1.setImageResource(R.drawable.rejected);
            holder.reject1.setTextColor(Color.parseColor("#FF0000"));
            holder.reject_amount.setText(data7.get(i).amount);
            holder.money1.setText(data7.get(i).amount);
            holder.request_date.setText(data7.get(i).requestedDate);
            holder.update_date.setVisibility(View.VISIBLE);
            holder.rs.setVisibility(View.VISIBLE);
            holder.update_date.setText(data7.get(i).updatedDate);
        }




        holder.i1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialogbox2);
                dialog.show();
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                final TextView reqdate1, reqrs1, reqdes1, appdes, appdate, reqrej;
                final ImageView image2, rejectimage, dot1, dot3;
                final CardView linear6;
                final LinearLayout linear5;

                final View dot2;
                final RelativeLayout relativelayout;

                reqdate1 = dialog.findViewById(R.id.reqdate12);
                reqrs1 = dialog.findViewById(R.id.reqrs12);
                reqdes1 = dialog.findViewById(R.id.reqdes12);
                //image200=dialog.findViewById(R.id.image200);
                appdes = dialog.findViewById(R.id.reqrejdes12);
                appdate = dialog.findViewById(R.id.reqrejdate12);
                image2 = dialog.findViewById(R.id.image2);
                rejectimage = dialog.findViewById(R.id.rejectimage);
                reqrej = dialog.findViewById(R.id.reqrej);
                linear5 = dialog.findViewById(R.id.linear50);
                linear6 = dialog.findViewById(R.id.linear60);
                /*dot1=dialog.findViewById(R.id.dot1);
                dot3=dialog.findViewById(R.id.dot3);
                dot2=dialog.findViewById(R.id.dot2);*/
                relativelayout = dialog.findViewById(R.id.relativelayout);


                prefManager = new PrefManager(context);
                HashMap<String, String> profile = prefManager.getUserDetails();
                id = profile.get("id");
                String request_id = data7.get(i).request_id;

                final ProgressDialog progressDialog1 = new ProgressDialog(context);
                progressDialog1.setMessage("Loading.....");
                progressDialog1.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<StatusResponse10> call1 = apiInterface.Statuss10(id, request_id);
                call1.enqueue(new Callback<StatusResponse10>() {
                    @Override
                    public void onResponse(Call<StatusResponse10> call, Response<StatusResponse10> response) {
                        if (response.isSuccessful()) ;

                        StatusResponse10 statusResponse = response.body();
                        if (statusResponse != null) {

                            StatusDataResponse10 statusDataResponse = statusResponse.status;

                            if (statusDataResponse.code == 200) {
                                progressDialog1.dismiss();
                                relativelayout.setVisibility(View.VISIBLE);

                                Data10 data = statusResponse.data;
                                String amount = data.amount;
                                String created_at = data.createdAt;
                                String updated_at = data.updatedAt;
                                String description = data.description;
                                String attachments = data.attachments;
                                String updateDescription = data.updateDescription;


                                String status1 = data.status;

                                if (status1.equals("0")) {

                                    reqdate1.setText(created_at);
                                    reqrs1.setText(amount);
                                    reqdes1.setText(description);
                                    appdes.setText(updateDescription);
                                    appdate.setText(updated_at);
                                    Picasso.get().load(attachments).into(image2);

                                    image2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {


                                            image2.buildDrawingCache();
                                            Bitmap bitmap = image2.getDrawingCache();
                                            Intent intent = new Intent(context, ProfileImage.class);

                                            ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                            bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                            intent.putExtra("byteArray", _bs.toByteArray());
                                            context.startActivity(intent);

                                        }
                                    });

                                } else if (status1.equals("1")) {

                                    reqrej.setText("Approved");
                                    reqrej.setTextColor(Color.parseColor("#00A219"));
                                    rejectimage.setImageResource(R.drawable.approved);
                                    reqdate1.setText(created_at);
                                    reqrs1.setText(amount);
                                    reqdes1.setText(description);
                                    appdes.setText(updateDescription);
                                    appdate.setText(updated_at);
                                    Picasso.get().load(attachments).into(image2);

                                    image2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {


                                            image2.buildDrawingCache();
                                            Bitmap bitmap = image2.getDrawingCache();
                                            Intent intent = new Intent(context, ProfileImage.class);

                                            ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                            bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                            intent.putExtra("byteArray", _bs.toByteArray());
                                            context.startActivity(intent);

                                        }
                                    });

                                } else if (status1.equals("2")) {


                                    linear5.setVisibility(View.GONE);
                                    linear6.setVisibility(View.GONE);
                               /* dot1.setVisibility(View.GONE);
                                dot2.setVisibility(View.GONE);
                                dot3.setVisibility(View.GONE);*/
                                    reqdate1.setText(created_at);
                                    reqrs1.setText(amount);
                                    reqdes1.setText(description);
                                    appdes.setText(updateDescription);
                                    appdate.setText(updated_at);
                                    Picasso.get().load(attachments).into(image2);

                                    image2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {


                                            image2.buildDrawingCache();
                                            Bitmap bitmap = image2.getDrawingCache();
                                            Intent intent = new Intent(context, ProfileImage.class);

                                            ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                            bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                            intent.putExtra("byteArray", _bs.toByteArray());
                                            context.startActivity(intent);

                                        }
                                    });


                                }


                            } else if (statusDataResponse.code == 409) {
                                progressDialog1.dismiss();
                                Toast.makeText(context, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                            }

                        }
                        }




                    @Override
                    public void onFailure(Call<StatusResponse10> call, Throwable t) {
                        progressDialog1.dismiss();
                        //Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                        Toast toast = Toast.makeText(context,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();

                    }

                });






                ImageView close=(ImageView) dialog.findViewById(R.id.close1);

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });

            }

        });


}

    @Override
    public int getItemCount() {
        return data7.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView request_date,amount,reject_amount,update_date,money1,reject1,rs;
        ImageView i1,rejectimage1;
        CardView card0;
        public Holder(@NonNull View itemView) {
            super(itemView);

            reject_amount=itemView.findViewById(R.id.reject_amount110);
            request_date=itemView.findViewById(R.id.request_date1);
            update_date=itemView.findViewById(R.id.update_date1);
            money1=itemView.findViewById(R.id.money110);
            i1=itemView.findViewById(R.id.i1110);
            reject1=itemView.findViewById(R.id.reject110);
            rejectimage1=itemView.findViewById(R.id.rejectimage110);
            card0=itemView.findViewById(R.id.card0);
            rs=itemView.findViewById(R.id.rs);


        }
    }
}
