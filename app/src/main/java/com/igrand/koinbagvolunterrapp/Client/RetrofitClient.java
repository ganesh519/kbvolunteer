package com.igrand.koinbagvolunterrapp.Client;

import com.igrand.koinbagvolunterrapp.ApiInterface;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


    public class RetrofitClient {

        public static final String BASE_URL = "https://www.koinbag.com/kbonline/api/services/";


        private static RetrofitClient mInstance;
        private Retrofit retrofit;

        private RetrofitClient() {

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(
                            new Interceptor() {
                                @Override
                                public Response intercept(Chain chain) throws IOException {
                                    Request request = chain.request().newBuilder().build();
                                    return chain.proceed(request);
                                }
                            }).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }

        public static synchronized RetrofitClient getInstance() {
            if (mInstance == null) {
                mInstance = new RetrofitClient();
            }
            return mInstance;
        }

        public ApiInterface getApi() {
            return retrofit.create(ApiInterface.class);
        }
    }
