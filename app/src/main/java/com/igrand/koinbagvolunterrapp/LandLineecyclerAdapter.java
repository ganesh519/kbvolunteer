package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class LandLineecyclerAdapter extends RecyclerView.Adapter<LandLineecyclerAdapter.Holder> {

    List<PrepaidResponse.DataBean> dataBeans;
    Context context;
    String operatorcode,operatorname;
    public LandLineecyclerAdapter(List<PrepaidResponse.DataBean> dataBeanList, SelectOperatorLandline selectOperatorLandline) {

        this.dataBeans=dataBeanList;
        this.context=selectOperatorLandline;
    }

    @NonNull
    @Override
    public LandLineecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_operator, viewGroup, false);

        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LandLineecyclerAdapter.Holder holder, final int i) {
        holder.txt_name.setText(dataBeans.get(i).getOperator_name());
        operatorcode=dataBeans.get(i).getOperator_code();
        operatorname=dataBeans.get(i).getOperator_name();

        if (dataBeans.get(i).getImage().isEmpty()) {
            holder.image_network.setImageResource(R.drawable.kb);
        } else {
            Picasso.get().load(dataBeans.get(i).getImage()).error(R.drawable.kb).into(holder.image_network);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context, LandLine.class);
                intent.putExtra("OperatorCode",dataBeans.get(i).getOperator_code());
                intent.putExtra("OperatorName",dataBeans.get(i).getOperator_name());
                if(dataBeans.get(i).getImage().isEmpty()) {

                    intent.putExtra("Image1",R.drawable.kb);

                } else {

                    intent.putExtra("Image",dataBeans.get(i).getImage());

                }

                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView txt_name;
        ImageView image_network;

        public Holder(@NonNull View itemView) {
            super(itemView);
            image_network=itemView.findViewById(R.id.image_network);
            txt_name=itemView.findViewById(R.id.txt_name);

        }
    }
}
