package com.igrand.koinbagvolunterrapp.Activities;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.BaseActivity;
import com.igrand.koinbagvolunterrapp.CustomBottomSheetDialogFragment21;
import com.igrand.koinbagvolunterrapp.CustomBottomSheetDialogFragment22;
import com.igrand.koinbagvolunterrapp.MetroRechargePay;
import com.igrand.koinbagvolunterrapp.R;

public class ApplyPromocode2 extends BaseActivity {

    ImageView back11;

    TextView apply11,applypromocode11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_promocode2);

        back11=findViewById(R.id.back001);
        applypromocode11=findViewById(R.id.applypromocode11);
        back11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ApplyPromocode2.this,MetroRechargePay.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
        apply11=findViewById(R.id.apply11);
        apply11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialog dialog = new BottomSheetDialog(ApplyPromocode2.this);
                dialog.setContentView(R.layout.activity_promo2);
                //dialog.show();
                new CustomBottomSheetDialogFragment21().show(getSupportFragmentManager(), "Dialog");
            }
        });
        applypromocode11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(ApplyPromocode1.this,LuckyKB1.class);
                startActivity(intent);*/

                BottomSheetDialog dialog = new BottomSheetDialog(ApplyPromocode2.this);
                dialog.setContentView(R.layout.activity_lucky_kb2);
                //dialog.show();
                new CustomBottomSheetDialogFragment22().show(ApplyPromocode2.this.getSupportFragmentManager(), "Dialog");
                dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);

            }
        });

    }
}
