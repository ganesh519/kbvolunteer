package com.igrand.koinbagvolunterrapp;

import java.util.List;

public class BrowsePlansHeadingResponse {


    /**
     * status : {"code":200,"message":"Browse Plane Type Data"}
     * data : [{"key":"International Roaming"},{"key":"Talktime"},{"key":"Unlimited Packs"},{"key":"Smart Recharge"},{"key":"ISD Pack"},{"key":"Data"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Browse Plane Type Data
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * key : International Roaming
         */

        private String key;
        public boolean isSelected;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
