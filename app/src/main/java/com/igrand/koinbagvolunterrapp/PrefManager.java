package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class PrefManager {


    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "KoinBagVolunteer";

    // All Shared Preferences Keys
    private static final String KEY_IS_WAITING_FOR_SMS = "IsWnaitingForSms";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_ID = "id";
    private static final String KEY_NGOID = "ngoid";
    private static final String KEY_FIRSTNAME = "f_name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_MOBILE = "mobile";
    private static final String KEY_USERID = "userid";
    private static final String KEY_SURNAME="surname";
    private static final String KEY_WALLETBALS="walletsbals";
    private static final String KEY_NGONAME="ngoname";
    private static final String KEY_PHOTO="photo";
    private static final String KEY_CREATED_ID="created_id";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setIsWaitingForSms(boolean isWaiting) {
        editor.putBoolean(KEY_IS_WAITING_FOR_SMS, isWaiting);
        editor.commit();
    }

    public boolean isWaitingForSms() {
        return pref.getBoolean(KEY_IS_WAITING_FOR_SMS, false);
    }


    public void createLogin(String id, String ngoid, String name, String surname, String mobile, String wallet, String userid, String ngo_name, String photo, String created_id) {
        editor.putString(KEY_ID, id);
        editor.putString(KEY_NGOID, ngoid);
        editor.putString(KEY_FIRSTNAME, name);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_SURNAME,surname);
        editor.putString(KEY_WALLETBALS,wallet);
        editor.putString(KEY_USERID,userid);
        editor.putString(KEY_NGONAME,ngo_name);
        editor.putString(KEY_PHOTO,photo);
        editor.putString(KEY_CREATED_ID,created_id);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);


        editor.commit();
    }

   /* public void Login(String token){
        editor.putString(KEY_TOKEN, token);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);
        editor.commit();
    }
*/
    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> profile = new HashMap<>();
        profile.put("id", pref.getString(KEY_ID, null));
        profile.put("ngoid", pref.getString(KEY_NGOID, null));
        profile.put("f_name", pref.getString(KEY_FIRSTNAME, null));
        profile.put("Mobile", pref.getString(KEY_MOBILE, null));
        profile.put("Surname",pref.getString(KEY_SURNAME,null));
        profile.put("Userid",pref.getString(KEY_USERID,null));
        profile.put("NgoName",pref.getString(KEY_NGONAME,null));
        profile.put("Photo",pref.getString(KEY_PHOTO,null));
        profile.put("Walletbals",pref.getString(KEY_WALLETBALS,null));
        profile.put("created_id",pref.getString(KEY_CREATED_ID,null));

        return profile;
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

}
