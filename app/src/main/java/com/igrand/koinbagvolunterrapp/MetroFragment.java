package com.igrand.koinbagvolunterrapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("ValidFragment")
public class MetroFragment extends Fragment {
    public MetroFragment() {

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_metro, container, false);

        TextView sourcestation=(TextView) v.findViewById(R.id.sourcestation);
        Button searchsoute=(Button)v.findViewById(R.id.searchroute);
        LinearLayout recharge=(LinearLayout)v.findViewById(R.id.recharge);
        sourcestation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Fragment sourceStation=new SourceStation();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                //fragmentTransaction.add(R.id.container4, planDetailsFragment);
                //fragmentTransaction.addToBackStack(null);
                BottomSheetDialog dialog = new BottomSheetDialog(getContext());
                dialog.setContentView(R.layout.fragment_source_station);
                //dialog.show();
                new CustomBottomSheetDialogFragment16().show(getActivity().getSupportFragmentManager(), "Dialog");
                dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);

                fragmentTransaction.commit();
            }
        });



        searchsoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment metroStation=new MetroStation();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                //fragmentTransaction.add(R.id.container4, planDetailsFragment);
                //fragmentTransaction.addToBackStack(null);

                BottomSheetDialog dialog = new BottomSheetDialog(getContext());
                dialog.setContentView(R.layout.fragment_metro_station);
                //dialog.show();
                new CustomBottomSheetDialogFragment17().show(getActivity().getSupportFragmentManager(), "Dialog");
                dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);

                fragmentTransaction.commit();

            }
        });

        recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),MetroRecharge.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
        return v;
    }
}
