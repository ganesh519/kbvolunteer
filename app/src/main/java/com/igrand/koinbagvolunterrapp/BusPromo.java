package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class BusPromo extends BaseActivity {

    Button proceedtopaybus;
    TextView applypromo0;
    ImageView backblack14;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_promo);

        proceedtopaybus=findViewById(R.id.proceedtopaybus);
        backblack14=findViewById(R.id.backblack14);
        proceedtopaybus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BottomSheetDialog dialog = new BottomSheetDialog(BusPromo.this);
                dialog.setContentView(R.layout.activity_finalpayment);
                dialog.show();
                new CustomBottomSheetDialogFragment14().show(getSupportFragmentManager(), "Dialog");

            }
        });

        applypromo0=findViewById(R.id.applypromo0);
        applypromo0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(BusPromo.this,BusPromoApplied.class);
                startActivity(intent);
            }
        });
        backblack14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BusPromo.this,TravellerInfoBus.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
