package com.igrand.koinbagvolunterrapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.igrand.koinbagvolunterrapp.Adapters.MyAdapter2;
import com.igrand.koinbagvolunterrapp.Client.APIClient1;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.Responses.BrowsePlansResponseMobile;
import com.igrand.koinbagvolunterrapp.Responses.DataResponseMobile;
import com.igrand.koinbagvolunterrapp.Responses.InternationalRoaming;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BrowsePlans extends BaseActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    ImageView back2;
    ApiInterface apiInterface;
    ViewPagerAdapterNotifications pagerAdapterNotifications;
    Bundle bundle;
    TextView type1,circle1;
    String recharge_type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_plans);
        /*tabLayout = (TabLayout) findViewById(R.id.tabLayout3);
        viewPager = (ViewPager) findViewById(R.id.viewPager3);*/

        type1 = findViewById(R.id.type1);
        circle1 = findViewById(R.id.circle1);


        String APIID = "AP135358";
        String PASSWORD = "ki832h74td3ff";
        String Operator_Code = getIntent().getStringExtra("Operator");
        String Circle_Code = getIntent().getStringExtra("Circle");
        String MobileNumber = getIntent().getStringExtra("Mobile");
        String Type = getIntent().getStringExtra("Type");
        String Circle = getIntent().getStringExtra("State");
        String PageID = "0";
        String FORMAT = "JSON/XML";


        type1.setText(Type);
        circle1.setText(Circle);


        back2 = findViewById(R.id.back2);
        back2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(BrowsePlans.this,OperatorActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);*/

                finish();
            }
        });




        SharedPreferences sharedPreferences = getSharedPreferences("Data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Operator", Operator_Code);
        editor.putString("Circle", Circle_Code);
        editor.putString("Mobile", MobileNumber);
        editor.apply();


        final ProgressDialog progressDialog = new ProgressDialog(BrowsePlans.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BrowsePlansResponseMobile> call = apiInterface.browseplansrecharge(Operator_Code, Circle_Code, MobileNumber);
        call.enqueue(new Callback<BrowsePlansResponseMobile>() {
            @Override
            public void onResponse(Call<BrowsePlansResponseMobile> call, Response<BrowsePlansResponseMobile> response) {

                if (response.isSuccessful()) ;
                BrowsePlansResponseMobile operatorFetch = response.body();

                if (response.code() == 200) {
                    progressDialog.dismiss();


                    //DataResponseMobile dataResponseMobile = operatorFetch.data;

                   // List<InternationalRoaming> internationalRoamings = dataResponseMobile.getInternationalRoaming();



                }
            }


            @Override
            public void onFailure(Call<BrowsePlansResponseMobile> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(BrowsePlans.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }


                  /*  for (int i = 0; i < internationalRoamings.size(); i++) {

                        tabLayout.addTab(tabLayout.newTab().setText(internationalRoamings.get(i).getRechargeType()));
                        pagerAdapterNotifications = new ViewPagerAdapterNotifications(getSupportFragmentManager(),tabLayout.getTabCount());
                        viewPager.setAdapter(pagerAdapterNotifications);
                        tabLayout.setupWithViewPager(viewPager);
                        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFD71B"));
                        tabLayout.setTabTextColors(Color.parseColor("#80F8F8F8"), Color.parseColor("#FFD71B"));



                        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {

                                viewPager.setCurrentItem(tab.getPosition());
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {

                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });

                    }*/

                    /*for (int i = 0; i <= internationalRoamings.size(); i++) {

                        recharge_type = internationalRoamings.get(i).getRechargeType();


                        if (internationalRoamings.contains(recharge_type)) {

                            pagerAdapterNotifications.addFragment(new TopUp(), recharge_type);
                            viewPager.setAdapter(pagerAdapterNotifications);
                            tabLayout.setupWithViewPager(viewPager);


                        } else {
                            return;

                        }

                    }*/


            //BrowsePlansResponseMobile.DataBeanX.InternationalRoamingBean internationalRoamingBean=;


            //  boolean MySortStrings =planDescriptions.get(i).rechargeType.equals("top");

/*
if(planDescriptions!=null){

    for (int i=0; i< planDescriptions.size();i++) {

        if(planDescriptions.get(i).rechargeType.equals("top"))


        {

            Log.e(TAG, "onResponse:"+planDescriptions.get(i).id );

            topupResponseArrayList.add(new TopupResponse(planDescriptions.get(i).rechargeShortDesc,planDescriptions.get(i).rechargeAmount,
                    planDescriptions.get(i).rechargeTalktime,planDescriptions.get(i).rechargeValidity,planDescriptions.get(i).rechargeLongDesc));

            allRecyclerAdapter1 = new BrowseRecyclerAdapter1(getContext(), topupResponseArrayList,MobileNumber);

        } else {
            Toast.makeText(getContext(), "No TopUp's...", Toast.LENGTH_SHORT).show();
        }

    }
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerView.setAdapter(allRecyclerAdapter1);
    Toast.makeText(getContext(), "Success..", Toast.LENGTH_SHORT).show();

} else
{
    Toast.makeText(getContext(), "No Data Found...", Toast.LENGTH_SHORT).show();
}





                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    //Toast.makeText(MobileRechargeActivity.this, "Volunteer Not Exist ,Please Contact Koinbag Ngo..", Toast.LENGTH_SHORT).show();


*/




        //  pagerAdapterNotifications.addFragment(new TopUp(), "TopUp");

      /*  pagerAdapterNotifications.addFragment(new SpecialRecharge(), "Special Recharge");
        pagerAdapterNotifications.addFragment(new FourG(), "2G/3G/4G");
        pagerAdapterNotifications.addFragment(new FullTalkTime(), "FullTalkTime");*/




/*
        tabLayout.addTab(tabLayout.newTab().setText("TopUp"));
        tabLayout.addTab(tabLayout.newTab().setText("Special Recharge"));
        tabLayout.addTab(tabLayout.newTab().setText("2G"));
        tabLayout.addTab(tabLayout.newTab().setText("3G"));
        tabLayout.addTab(tabLayout.newTab().setText("4G"));
        tabLayout.addTab(tabLayout.newTab().setText("FullTalkTime"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayout.setTabTextColors(Color.parseColor("#33000000"), Color.parseColor("#FFFFFF"));*/


      //  final MyAdapter2 adapter = new MyAdapter2(this, getSupportFragmentManager(), tabLayout.getTabCount());





    public class ViewPagerAdapterNotifications extends FragmentPagerAdapter {
        int mNumOfTabs;
        Fragment fragment = null;

       /* private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
*/
        public ViewPagerAdapterNotifications(FragmentManager fragmentManager,int NumOfTabs) {
            super(fragmentManager);
            this.mNumOfTabs = NumOfTabs;

        }


        @Override
        public Fragment getItem(int position) {

            for (int i = 0; i < mNumOfTabs ; i++) {
                if (i == position) {
                   // fragment = YourFragment.newInstance();
                    break;
                }
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return super.getPageTitle(position);
        }

       /* public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }*/

    }

}