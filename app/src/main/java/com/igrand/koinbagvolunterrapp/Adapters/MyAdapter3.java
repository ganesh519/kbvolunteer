package com.igrand.koinbagvolunterrapp.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.igrand.koinbagvolunterrapp.BestOffersFragment1;
import com.igrand.koinbagvolunterrapp.PopularFragment1;
import com.igrand.koinbagvolunterrapp.SpecialRechargeFragment;

public class MyAdapter3 extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;
    public MyAdapter3(Context context, FragmentManager supportFragmentManager, int tabCount) {

        super(supportFragmentManager);
        myContext = context;
        this.totalTabs = tabCount;
    }

    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                BestOffersFragment1 bestOffersFragment1 = new BestOffersFragment1();
                return bestOffersFragment1;
            case 1:
                PopularFragment1 popularFragment1 = new PopularFragment1();
                return popularFragment1;
            case 2:
                SpecialRechargeFragment specialRechargeFragment = new SpecialRechargeFragment();
                return specialRechargeFragment;

            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;

    }
}

