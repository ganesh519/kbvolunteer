package com.igrand.koinbagvolunterrapp.Activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.BaseActivity;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankDetailPage extends BaseActivity {

    RecyclerView recycler_bank;
    String id,created_id,bank_name,account_name,account_no,branch,ifsc_code;
    RecyclerBankDetails recyclerBankDetails;
    ApiInterface apiInterface;
    ImageView backbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_detail_page);
        recycler_bank=findViewById(R.id.recycler_bank);
        backbutton=findViewById(R.id.backpage00);


        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        if(getIntent()!=null) {

            id=getIntent().getStringExtra("ID");
            created_id=getIntent().getStringExtra("Created_id");
        }




        final ProgressDialog progressDialog4 = new ProgressDialog(BankDetailPage.this);
        progressDialog4.setMessage("Loading.....");
        progressDialog4.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SpinnerModel0> call3 = apiInterface.spinner0(created_id,id);
        call3.enqueue(new Callback<SpinnerModel0>() {
            @Override
            public void onResponse(Call<SpinnerModel0> call, Response<SpinnerModel0> response) {
                if (response.isSuccessful()) ;

                SpinnerModel0 statusResponse = response.body();

                if(statusResponse!=null) {

                    StatusSpinner0 statusDataResponse = statusResponse.status;

                    if (statusDataResponse.code == 200) {

                        progressDialog4.dismiss();

                        List<DataSpinner0> dataSpinner0s=response.body().data;

                        for (int i=0;i<=dataSpinner0s.size();i++) {

                            account_name=dataSpinner0s.get(i).getAccountName();
                            bank_name=dataSpinner0s.get(i).getBankName();
                            account_no=dataSpinner0s.get(i).getAccountNo();
                            branch=dataSpinner0s.get(i).getBranch();
                            ifsc_code=dataSpinner0s.get(i).getIfscCode();

                            break;


                        }



                        recycler_bank.setLayoutManager(new LinearLayoutManager(BankDetailPage.this));
                        recyclerBankDetails = new RecyclerBankDetails(getApplicationContext(),dataSpinner0s);
                        recycler_bank.setAdapter(recyclerBankDetails);


                    } else if (statusDataResponse.code == 409) {
                        progressDialog4.dismiss();

                        Toast.makeText(getApplicationContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                    }

                } else {

                    progressDialog4.dismiss();
                    Toast.makeText(BankDetailPage.this, "No Bank Details...", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<SpinnerModel0> call, Throwable t) {

                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });



    }
}
