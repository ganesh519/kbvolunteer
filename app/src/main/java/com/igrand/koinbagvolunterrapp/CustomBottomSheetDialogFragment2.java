package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomBottomSheetDialogFragment2 extends BottomSheetDialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_alert_dialog, container, false);
        Button proceedtopay = (Button)v.findViewById(R.id.proceedtopay);
        TextView promocode=(TextView)v.findViewById(R.id.promocode);

    proceedtopay.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent intent=new Intent(getActivity(),Payment.class);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        }
    });

    ImageView imageView=(ImageView)v.findViewById(R.id.exit3);
        imageView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    });

        return v;
    }

}

