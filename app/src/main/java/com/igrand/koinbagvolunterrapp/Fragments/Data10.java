package com.igrand.koinbagvolunterrapp.Fragments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data10 {

    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("update_description")
    @Expose
    public String updateDescription;
    @SerializedName("attachments")
    @Expose
    public String attachments;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("amount", amount).append("createdAt", createdAt).append("updatedAt", updatedAt).append("status", status).append("description", description).append("updateDescription", updateDescription).append("attachments", attachments).toString();
    }
}