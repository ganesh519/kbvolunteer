package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.Adapters.MyAdapter1;
import com.igrand.koinbagvolunterrapp.Fragments.AllFragment1;
import com.igrand.koinbagvolunterrapp.Fragments.AllFragment2;
import com.igrand.koinbagvolunterrapp.Fragments.ApprovedFragment1;
import com.igrand.koinbagvolunterrapp.Fragments.FailedFragment2;
import com.igrand.koinbagvolunterrapp.Fragments.FragmentWallet1;
import com.igrand.koinbagvolunterrapp.Fragments.RejectedFragment1;
import com.igrand.koinbagvolunterrapp.Fragments.RequestsFragment1;

import java.util.ArrayList;
import java.util.List;

public class FragmentDigital extends Fragment {



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_digital, container, false);
        TextView textviewdigital;
        Typeface typeface;
        ImageView toggledigital;
        final DrawerLayout drawerLayout;
        ViewPagerAdapterNotifications pagerAdapterNotifications;


        drawerLayout=(DrawerLayout)getActivity().findViewById(R.id.drawer_layout);
        toggledigital=(ImageView)v.findViewById(R.id.toggledigital);
        toggledigital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });

        textviewdigital=(TextView)v.findViewById(R.id.textviewdigital1);
        typeface=Typeface.createFromAsset(getActivity().getAssets(),"font/Avenir-Heavy.ttf");
        textviewdigital.setTypeface(typeface);

        TabLayout tabLayout1;
        final ViewPager viewPager1;

        tabLayout1 = (TabLayout)v.findViewById(R.id.tabLayout1);
        viewPager1 = (ViewPager)v.findViewById(R.id.viewPager1);


        tabLayout1.setSelectedTabIndicatorColor(Color.parseColor("#FFD71B"));
        tabLayout1.setTabTextColors(Color.parseColor("#80F8F8F8"), Color.parseColor("#FFD71B"));


        pagerAdapterNotifications = new ViewPagerAdapterNotifications(getChildFragmentManager());
        pagerAdapterNotifications.addFragment(new AllFragment1(), "All");
        pagerAdapterNotifications.addFragment(new SuccessFragment(), "Success");
        pagerAdapterNotifications.addFragment(new ProcessingFragment(), "Pending");
        pagerAdapterNotifications.addFragment(new FailedFragment(), "Failed");

        viewPager1.setAdapter(pagerAdapterNotifications);
        tabLayout1.setupWithViewPager(viewPager1);

        return v;

    }



    public class ViewPagerAdapterNotifications extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapterNotifications(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

    }

}



