package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Adapters.CirclesRecyclerAdapter;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomBottomSheetDialogFragmentData extends BottomSheetDialogFragment {

    RecyclerView recycler_circle;
    ApiInterface apiInterface;
    CirclesRecyclerAdapterData circlesRecyclerAdapter;
    String opertatortype,opertatorname,opertatorimage,opertatorcode,MobileNumber;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_select_circle, container, false);

        recycler_circle=v.findViewById(R.id.recycler_circle);


        Bundle bundle = getArguments();

        if (bundle != null){

            opertatorname=bundle.getString("Operator_name");
            opertatortype=bundle.getString("Operator_type");
            opertatorimage=bundle.getString("Operator_image");
            opertatorcode=bundle.getString("Operator_code");
            MobileNumber = bundle.getString("Mobile");

        }



        final ProgressDialog progressDialog1 = new ProgressDialog(getContext());
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Circles> call1 = apiInterface.circles();
        call1.enqueue(new Callback<Circles>() {
            @Override
            public void onResponse(Call<Circles> call, Response<Circles> response) {
                if (response.isSuccessful()) ;
                Circles walletResponse1 = response.body();
                if (walletResponse1 != null) {


                    StatusCircle  statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog1.dismiss();


                        List<DataCircle> data7 = walletResponse1.data;


                        recycler_circle.setLayoutManager(new LinearLayoutManager(getContext()));
                        circlesRecyclerAdapter=new CirclesRecyclerAdapterData(data7,getActivity(),opertatorname,opertatortype,opertatorimage,opertatorcode,MobileNumber);
                        recycler_circle.setAdapter(circlesRecyclerAdapter);

                    } else if (statusDataResponse7.code != 200) {
                        progressDialog1.dismiss();
                        Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                }
                else {
                    progressDialog1.dismiss();
                    Toast.makeText(getContext(), "No Data", Toast.LENGTH_SHORT).show();
                }
            }



            @Override
            public void onFailure(Call<Circles> call, Throwable t) {
                progressDialog1.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(getContext(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });






        ImageView imageView=(ImageView)v.findViewById(R.id.exit1);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(getActivity(),OperatorActivity.class);
                startActivity(intent);*/
                dismiss();
            }
        });
        return v;
    }


}

