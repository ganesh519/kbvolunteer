package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.Adapters.MyAdapter6;

public class Flight extends BaseActivity {

    TabLayout tabLayout4;
    ViewPager viewPager4;
    TextView tabContent;
    ImageView backblack;

    private int[] tabIcons = {
            R.drawable.flight2,
            R.drawable.bus1,
            R.drawable.train1
    };
    int i=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight);

        tabLayout4 = (TabLayout) findViewById(R.id.tabLayout4);
        viewPager4 = (ViewPager) findViewById(R.id.viewPager4);
        tabContent=findViewById(R.id.tabContent);
        backblack=findViewById(R.id.backblack);
        tabLayout4.addTab(tabLayout4.newTab().setText("Flight"));
        tabLayout4.addTab(tabLayout4.newTab().setText("Bus"));
        tabLayout4.addTab(tabLayout4.newTab().setText("Metro"));
        tabLayout4.setTabGravity(TabLayout.GRAVITY_FILL);
        setupTabIcons();

        backblack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Flight.this,NavigationDrawerDashboard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        final MyAdapter6 adapter = new MyAdapter6(this,getSupportFragmentManager(), tabLayout4.getTabCount());


        viewPager4.setAdapter(adapter);
        viewPager4.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout4));

        tabLayout4.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager4.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



    }

    private void setupTabIcons() {
        tabLayout4.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout4.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout4.getTabAt(2).setIcon(tabIcons[2]);

    }
}
