package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data{

    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("firstName", firstName).append("mobileNumber", mobileNumber).toString();
    }

}

