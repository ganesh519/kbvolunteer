package com.igrand.koinbagvolunterrapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.igrand.koinbagvolunterrapp.Activities.CustomDialogActivity1;
import com.igrand.koinbagvolunterrapp.Activities.Login;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WelcomeBack extends BaseActivity {

    Button proceed1;
    TextView loginotp, forgetPassword, welcomeback, willsmith, enter,firstname;
    EditText enterpin;
    ImageView backpage00;
    Typeface typeface, typeface1, typeface2,typeface3;
    ApiInterface apiInterface;
    PrefManager prefManager;
    LoginResponse1.StatusBean statusBean;
    LoginResponse2.StatusBean2 statusBean1;
    String f_name,surname;
    TextInputLayout text_input_layout;
    Context context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_back);

        prefManager=new PrefManager(WelcomeBack.this);

        proceed1 = findViewById(R.id.proceed1);
        loginotp = findViewById(R.id.loginotp);
        backpage00 = findViewById(R.id.backpage00);
        forgetPassword = findViewById(R.id.forgetPassword);
        welcomeback = findViewById(R.id.welcomeback);
        willsmith = findViewById(R.id.willsmith);
        enterpin = findViewById(R.id.enterpin);
        firstname=findViewById(R.id.firstname);
        text_input_layout=findViewById(R.id.text_input_layout);

      /*  InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
*/


        enterpin.setTextColor(Color.parseColor("#000000"));

       /* enterpin.setInputType(InputType.TYPE_NULL);

        enterpin.setInputType(InputType.TYPE_CLASS_TEXT);
        enterpin.requestFocus();
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.showSoftInput(enter, InputMethodManager.SHOW_FORCED);*/

        enterpin.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do something, e.g. set your TextView here via .setText()
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        enterpin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String value=String.valueOf(s.length());

                if (value.equals("6")) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//Hide:
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {



            }
        });



//        int length=enterpin.getText().length();
//        if(length==5) {
//            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        }



       final String name1 = getIntent().getStringExtra("Name1");
        final String surname = getIntent().getStringExtra("SurName1");

        firstname.setText(name1);
        willsmith.setText(surname);

        typeface = Typeface.createFromAsset(getAssets(), "font/Avenir-Black.ttf");
        typeface1 = Typeface.createFromAsset(getAssets(), "font/Avenir-Heavy.ttf");
        typeface2 = Typeface.createFromAsset(getAssets(), "font/Avenir-Light.ttf");
        typeface3 = Typeface.createFromAsset(getAssets(), "font/Avenir-Medium.ttf");

        welcomeback.setTypeface(typeface);
        willsmith.setTypeface(typeface1);
        loginotp.setTypeface(typeface2);
        forgetPassword.setTypeface(typeface2);
        text_input_layout.setTypeface(typeface3);


        proceed1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String mobile_number = getIntent().getStringExtra("Mobile_number");
                final String password = enterpin.getText().toString();

                if(password.equals("")) {

                    Toast toast= Toast.makeText(WelcomeBack.this,
                            "Please Enter Password", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }

                else
                {

                    Intent intent=new Intent(WelcomeBack.this, CustomDialogActivity1.class);
                    intent.putExtra("Mobile",mobile_number);
                    intent.putExtra("Pass",password);
                    startActivity(intent);

                }
            }
        });



        backpage00.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeBack.this, Login.class);
                intent.putExtra("Home",false);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        loginotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String mobile_number = getIntent().getStringExtra("Mobile_number");

                Intent intent = new Intent(WelcomeBack.this, CustomDialogActivity2.class);
                intent.putExtra("Mobile_number", mobile_number);
                intent.putExtra("Name1",name1);
                intent.putExtra("SurName1",surname);
                startActivity(intent);
            }
        });

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String mobile_number = getIntent().getStringExtra("Mobile_number");

                //Toast.makeText(WelcomeBack.this, statusBean1.getMessage(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(WelcomeBack.this, CustomDialogActivity3.class);
                intent.putExtra("Mobile_number", mobile_number);
                intent.putExtra("Name1", name1);
                intent.putExtra("SurName1", surname);
                //intent.putExtra("OTP", otp);
                startActivity(intent);
/*
                final Dialog progressDialog = new Dialog(WelcomeBack.this, R.style.Theme_AppCompat_Light_NoActionBar);
                progressDialog.setContentView(R.layout.custom_dialog);
                Window window = progressDialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                LottieAnimationView animationView = progressDialog.findViewById(R.id.lottie_view);
                animationView.setAnimation("refresh.json");
                animationView.playAnimation();
                animationView.loop(true);
                progressDialog.show();
                TextView text = (TextView) progressDialog.findViewById(R.id.text);
                ImageView image = (ImageView) progressDialog.findViewById(R.id.image);

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<LoginResponse2> call = apiInterface.Statuss2(mobile_number);
                call.enqueue(new Callback<LoginResponse2>() {
                    @Override
                    public void onResponse(Call<LoginResponse2> call, Response<LoginResponse2> response) {

                        if (response.code() == 200) {

                            progressDialog.dismiss();
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            LoginResponse2.DataBean2 dataBean = response.body().getData();
                            //Integer otp = dataBean.otp;

                            //Toast.makeText(WelcomeBack.this, statusBean1.getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(WelcomeBack.this, OTP.class);
                            intent.putExtra("Mobile_number", mobile_number);
                            intent.putExtra("Name1",name1);
                            intent.putExtra("SurName1",surname);
                            //intent.putExtra("OTP", otp);
                            startActivity(intent);

                        }else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(WelcomeBack.this, "Please Check the OTP", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<LoginResponse2> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(WelcomeBack.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

            }*/

            }
        });
    }



//    private final TextWatcher mTextEditorWatcher = new TextWatcher() {
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//        }
//
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            //This sets a textview to the current length
//            String value=String.valueOf(s.length());
//
//            if (value.equals("6")) {
//                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//            }
//
//        }
//
//        public void afterTextChanged(Editable s) {
//
//
//        }
//    };



    @Override
    public void onBackPressed() {
        Intent intent=new Intent(WelcomeBack.this, Login.class);
        intent.putExtra("Home",false);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        super.onBackPressed();
    }
}
