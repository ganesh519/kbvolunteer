package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment {
    PrefManager prefManager;
     ImageView drawerLayout,bell;
     DrawerLayout drawerLayout1;
    ApiInterface apiInterface;
    TextView notificationcount;
    NestedScrollView nested;
    GridView gridView;
    CustomGridViewAdapter customGridAdapter;
    boolean isImageFitToScreen;



     TextView hello, walletbalance, transactions, commissions, n2891, n2892, n2890, tmobile, tdth, telectricity, tlandline,
            tbroadband, tgas, tdatacard, tmetro, tbus, tflight, twater, thotel, tinsurance, trecentpayments, tleo, tnum, twillsmith, tnum1,
            tjhonnydeep, tnum2, trock, tnum3, thome, tdigital, twallet, tprofile,volunteername11;
    Typeface typeface, typeface1, typeface2;
    String Id,f_name,surname,mobile1,walletbals,userid,photo;

    LinearLayout viewProfile;
    ImageView toggle11, rock,walletTransactions, digitalTransactions, home, profile, mobile, dth, electricity, water, flight,landline,
    broadband,gas,datacard,metro,bus,hotel,insurance;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.home, container, false);




        drawerLayout1=(DrawerLayout)getActivity().findViewById(R.id.drawer_layout);
        rock = (ImageView) view.findViewById(R.id.rock);
        notificationcount = (TextView) view.findViewById(R.id.notificationcount);
        nested=view.findViewById(R.id.nested);
        hello = (TextView) view.findViewById(R.id.hello);
        bell = (ImageView) view.findViewById(R.id.bell);
        TextView volunteername1 = (TextView) view.findViewById(R.id.volunteername);
        walletbalance = (TextView) view.findViewById(R.id.walletbalance);
        transactions = (TextView) view.findViewById(R.id.transactions);
        commissions = (TextView) view.findViewById(R.id.commissions);
        n2890 = (TextView) view.findViewById(R.id.n2890);
        n2891 = (TextView) view.findViewById(R.id.n2891);
        n2892 = (TextView) view.findViewById(R.id.n2892);
        trecentpayments = (TextView) view.findViewById(R.id.recentpayments);
        tleo = (TextView) view.findViewById(R.id.tleo);
        tnum = (TextView) view.findViewById(R.id.tnum);
        twillsmith = (TextView) view.findViewById(R.id.twillsmith);
        tnum1 = (TextView) view.findViewById(R.id.tnum1);
        tjhonnydeep = (TextView) view.findViewById(R.id.tjhonnydeep);
        tnum2 = (TextView) view.findViewById(R.id.tnum2);
        trock = (TextView) view.findViewById(R.id.trock);
        tnum3 = (TextView) view.findViewById(R.id.tnum3);
        volunteername11 = (TextView) view.findViewById(R.id.volunteername11);

        drawerLayout=(ImageView)view.findViewById(R.id.toggle11);
        drawerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout1.openDrawer(Gravity.START);
            }
        });

        final ProgressDialog progressDialog3 = new ProgressDialog(getContext());
        progressDialog3.setMessage("Loading.....");
        progressDialog3.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusResponse13> call3 = apiInterface.Statuss14();
        call3.enqueue(new Callback<StatusResponse13>() {
            @Override
            public void onResponse(Call<StatusResponse13> call, Response<StatusResponse13> response) {
                if (response.isSuccessful()) ;

                StatusResponse13 statusResponse = response.body();

                if(statusResponse!=null) {

                    StatusDataResponse13 statusDataResponse = statusResponse.status;

                    if (statusDataResponse.code == 200) {
                        progressDialog3.dismiss();


                        List<Data13> data = statusResponse.data;
                        gridView = (GridView)view.findViewById(R.id.gridView);
                        customGridAdapter = new CustomGridViewAdapter(getContext(), R.layout.row_grid, data);
                        gridView.setAdapter(customGridAdapter);



                    } else if (statusDataResponse.code == 409) {
                        progressDialog3.dismiss();
                        Toast.makeText(getContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    progressDialog3.dismiss();
                    Toast.makeText(getActivity(), "No Data Found...", Toast.LENGTH_SHORT).show();
                }
                }



            @Override
            public void onFailure(Call<StatusResponse13> call, Throwable t) {
                progressDialog3.dismiss();
                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(getActivity(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });



        volunteername1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        prefManager=new PrefManager(getActivity());

        HashMap<String, String> profile=prefManager.getUserDetails();
        Id=profile.get("id");
        f_name=profile.get("f_name");
        surname=profile.get("Surname");
        mobile1=profile.get("Mobile");
        photo=profile.get("Photo");
        userid=profile.get("Userid");
        walletbals=profile.get("Walletbals");


        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getActivity(),Notifications.class);
                intent.putExtra("Id",Id);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


            }
        });


        volunteername1.setText(f_name);
        volunteername11.setText(surname);

        Picasso.get().load(photo).error(R.drawable.kb).into(rock);

        rock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rock.buildDrawingCache();
                Bitmap bitmap = rock.getDrawingCache();
                Intent intent=new Intent(getActivity(),ProfileImage.class);

                ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                intent.putExtra("byteArray", _bs.toByteArray());
                startActivity(intent);
                }
        });

        final ProgressDialog progressDialog1 = new ProgressDialog(getContext());
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusResponse9> call1 = apiInterface.Statuss9(Id);
        call1.enqueue(new Callback<StatusResponse9>() {
            @Override
            public void onResponse(Call<StatusResponse9> call, Response<StatusResponse9> response) {
                if (response.isSuccessful()) ;

                StatusResponse9 statusResponse = response.body();

                if(statusResponse!=null) {

                    StatusDataResponse9 statusDataResponse = statusResponse.status;

                    if (statusDataResponse.code == 200) {
                        progressDialog1.dismiss();
                        nested.setVisibility(View.VISIBLE);

                        Data9 data = statusResponse.data;
                        String walletBal = data.walletBal;
                        String transaction = data.transaction;
                        String commission = data.commission;
                        String notificaton=data.notifications;


                        n2890.setText(walletBal);
                        n2891.setText(transaction);
                        n2892.setText(commission);
                        notificationcount.setText(notificaton);

                        //Toast.makeText(getContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                        //    Intent intent = new Intent(NavigationDrawerDashboard.this, PersonalDetails.class);

                    } else if (statusDataResponse.code == 409) {
                        progressDialog1.dismiss();
                        nested.setVisibility(View.GONE);
                        Toast.makeText(getContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    progressDialog1.dismiss();
                    Toast.makeText(getActivity(), "Data not found...", Toast.LENGTH_SHORT).show();
                }

                }




            @Override
            public void onFailure(Call<StatusResponse9> call, Throwable t) {
                nested.setVisibility(View.GONE);
                progressDialog1.dismiss();
//                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(getActivity(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });

        return view;

    }
}
