package com.igrand.koinbagvolunterrapp.Activities;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.igrand.koinbagvolunterrapp.BaseActivity;
import com.igrand.koinbagvolunterrapp.NavigationDrawerDashboard;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.R;


public class Splash extends BaseActivity {

    private static int splashscreentimeout=2000;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        prefManager=new PrefManager(Splash.this);



            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {


                    if (prefManager.isLoggedIn()){

                        Intent intent = new Intent(Splash.this, NavigationDrawerDashboard.class);
                        startActivity(intent);

                    }
                    else {
                        Intent intent=new Intent(Splash.this,Login.class);
                        intent.putExtra("Home",false);
                        startActivity(intent);
                    }

                }
            },splashscreentimeout);


    }

    protected void onLeaveThisActivity() {
    }

    protected void onStartNewActivity() {
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }
}

