package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.Activities.AirtelTV;

public class CustomBottomSheetDialogFragment6 extends BottomSheetDialogFragment {

    TextView validity,longd,amount;
    Button selectplan1;
    ImageView exit;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_plandth, container, false);


        longd=v.findViewById(R.id.longd);
        validity=v.findViewById(R.id.validity);
        amount=v.findViewById(R.id.amount);
        selectplan1=v.findViewById(R.id.selectplan1);
        exit=v.findViewById(R.id.exit);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        SharedPreferences sharedPreferences=getActivity().getSharedPreferences("BrowseData", Context.MODE_PRIVATE);
        final String Amount=sharedPreferences.getString("Amount","'");
        String Circle_Code=sharedPreferences.getString("TalkTime","'");
        final String MobileNumber=sharedPreferences.getString("Validity","'");
        final String longdes=sharedPreferences.getString("Long","'");
        final String mobilenumber=sharedPreferences.getString("Mobile","'");
        final String operatorName=sharedPreferences.getString("OperatorName","'");
        final String MobileNumber1=sharedPreferences.getString("Mobile","'");


        SharedPreferences sharedPreferences1=getActivity().getSharedPreferences("Data", Context.MODE_PRIVATE);
        final String Operator_Code1=sharedPreferences1.getString("Operator","'");
        final String Circle_Code1=sharedPreferences1.getString("Circle","'");


        longd.setText(longdes);
        validity.setText(MobileNumber);
        amount.setText(Amount);

        selectplan1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), AirtelTV.class);
                intent.putExtra("Operator",Operator_Code1);
                intent.putExtra("Amount",Amount);
                intent.putExtra("OperatorName",operatorName);
                intent.putExtra("Mobile",MobileNumber1);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });




        return v;
    }


}
