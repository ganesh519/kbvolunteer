package com.igrand.koinbagvolunterrapp.Activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Adapters.CirclesRecyclerAdapter;
import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.Circles;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.DataCircle;
import com.igrand.koinbagvolunterrapp.R;
import com.igrand.koinbagvolunterrapp.StatusCircle;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CirclesList extends AppCompatActivity {

    RecyclerView recycler_circle;
    ApiInterface apiInterface;
    CirclesRecyclerAdapter circlesRecyclerAdapter;
    String opertatortype,opertatorname,opertatorimage,opertatorcode;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circles_list);

        recycler_circle=findViewById(R.id.recycler_circle1);


        opertatorcode = getIntent().getStringExtra("Operator");
        opertatorname = getIntent().getStringExtra("Operatorname");
        opertatortype = getIntent().getStringExtra("Operatortype");
        opertatorimage = getIntent().getStringExtra("Operatorimage");








        final ProgressDialog progressDialog1 = new ProgressDialog(CirclesList.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Circles> call1 = apiInterface.circles();
        call1.enqueue(new Callback<Circles>() {
            @Override
            public void onResponse(Call<Circles> call, Response<Circles> response) {
                if (response.isSuccessful()) ;
                Circles walletResponse1 = response.body();
                if (walletResponse1 != null) {


                    StatusCircle statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog1.dismiss();


                        List<DataCircle> data7 = walletResponse1.data;


                        recycler_circle.setLayoutManager(new LinearLayoutManager(CirclesList.this));
                       // circlesRecyclerAdapter=new CirclesRecyclerAdapter(data7,getApplicationContext(),opertatorname,opertatortype,opertatorimage,opertatorcode,MobileNumber);
                       // recycler_circle.setAdapter(circlesRecyclerAdapter);

                    } else if (statusDataResponse7.code != 200) {
                        progressDialog1.dismiss();
                        Toast.makeText(CirclesList.this, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                }
                else {
                    progressDialog1.dismiss();
                    Toast.makeText(CirclesList.this, "No Data", Toast.LENGTH_SHORT).show();
                }
            }



            @Override
            public void onFailure(Call<Circles> call, Throwable t) {
                progressDialog1.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(CirclesList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });






        ImageView imageView=findViewById(R.id.exit1);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(getActivity(),OperatorActivity.class);
                startActivity(intent);*/
                finish();
            }
        });
    }
}
