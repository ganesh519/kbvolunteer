package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomBottomSheetDialogFragmentboard extends BottomSheetDialogFragment {

    ApiInterface apiInterface;
    RecyclerView recycler_circle;
    BoardRecyclerAdapterelectricity dthRecyclerAdapter;
    ImageView exit9;
    String circlename,circlecode;
    FrameLayout frame3;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.activity_select_circle2, container, false);

        circlename = getArguments().getString("CircleName");
        circlecode = getArguments().getString("CircleCode");

//        Bundle bundle = getArguments();
//
//        if (bundle != null) {
//
//            String circlename = bundle.getString("CircleName");
//            String circlecode = bundle.getString("CircleCode");
//
//        }

        recycler_circle = v.findViewById(R.id.recycler_circle);
        exit9 = v.findViewById(R.id.exit9);
        frame3 = v.findViewById(R.id.frame3);

        exit9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();


        String type = "electricity";
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
       // Call<PrepaidResponse> call = apiInterface.DTH(type);
        Call<PrepaidResponse1> call = apiInterface.boardcodes();

        call.enqueue(new Callback<PrepaidResponse1>() {
            @Override
            public void onResponse(Call<PrepaidResponse1> call, Response<PrepaidResponse1> response) {

                if (response.isSuccessful()) ;

                PrepaidResponse1 prepaidResponse = response.body();

                if (prepaidResponse != null) {

                    PrepaidResponse1.StatusBean statusBean = prepaidResponse.getStatus();
                    if (statusBean.getCode() == 200) {
                        progressDialog.dismiss();
                        frame3.setVisibility(View.VISIBLE);

                        List<PrepaidResponse1.DataBean> dataBeanList = prepaidResponse.getData();


                        //RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 3);
                        recycler_circle.setLayoutManager(new LinearLayoutManager(getContext()));
                        dthRecyclerAdapter = new BoardRecyclerAdapterelectricity(dataBeanList, getActivity(),circlename,circlecode);
                        recycler_circle.setAdapter(dthRecyclerAdapter);


                    } else if (statusBean.getCode() == 400) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), statusBean.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                    if (response.code() == 200) {

                        progressDialog.dismiss();


                    } else if (response.code() == 401) {
                        progressDialog.dismiss();

                    } else if (response.code() == 400) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "respond", Toast.LENGTH_SHORT).show();

                    } else if (response.code() == 500) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "true", Toast.LENGTH_SHORT).show();


                    }

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "No Data Found...", Toast.LENGTH_SHORT).show();
                }

                }



            @Override
            public void onFailure(Call<PrepaidResponse1> call, Throwable t) {
                progressDialog.dismiss();
                //Log.e(TAG, "failed"+t.getMessage() );
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


        return v;
    }
}