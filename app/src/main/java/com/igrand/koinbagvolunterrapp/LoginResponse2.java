package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class LoginResponse2 {

    @SerializedName("status")
    private StatusBean2 status;
    @SerializedName("data")
    private DataBean2 data;

    public StatusBean2 getStatus() {
        return status;
    }

    public void setStatus(StatusBean2 status) {
        this.status = status;
    }

    public DataBean2 getData() {
        return data;
    }

    public void setData(DataBean2 data) {
        this.data = data;
    }


    public static class StatusBean2 {

        @SerializedName("code")
        private int code;
        @SerializedName("message")
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean2 {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("first_name")
        @Expose
        public String firstName;
        @SerializedName("surname")
        @Expose
        public String surname;
        @SerializedName("mobile_number")
        @Expose
        public String mobileNumber;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("ngo_name")
        @Expose
        public String ngoName;
        @SerializedName("ngo_id")
        @Expose
        public String ngoId;
        @SerializedName("photo")
        @Expose
        public String photo;
        @SerializedName("wallet_bal")
        @Expose
        public String walletBal;

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("id", id).append("firstName", firstName).append("surname", surname).append("mobileNumber", mobileNumber).append("userId", userId).append("ngoName", ngoName).append("ngoId", ngoId).append("photo", photo).append("walletBal", walletBal).toString();
        }
    }

    }
