package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class BrowseDthRecyclerAdapter extends RecyclerView.Adapter<BrowseDthRecyclerAdapter.Holder> {

    List<PlanDescription> planDescriptions;
    Context context;
    String Operatorname,MobileNumber;

    public BrowseDthRecyclerAdapter(Context applicationContext, List<PlanDescription> planDescriptions, String operator_Name,String mobileNumber) {

        this.planDescriptions=planDescriptions;
        this.context=applicationContext;
        this.Operatorname=operator_Name;
        this.MobileNumber=mobileNumber;
    }



    @NonNull
    @Override
    public BrowseDthRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.browseplan, viewGroup, false);
        return new Holder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull BrowseDthRecyclerAdapter.Holder holder, int i) {

        final String amount=planDescriptions.get(i).rechargeAmount;
        final String talktime=planDescriptions.get(i).rechargeTalktime;
        final String validity=planDescriptions.get(i).rechargeValidity;
        final String longdes=planDescriptions.get(i).rechargeLongDesc;

        holder.desclong.setText(planDescriptions.get(i).rechargeLongDesc);
        holder.rechargeamount.setText(planDescriptions.get(i).rechargeAmount);


        holder.listlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences=context.getSharedPreferences("BrowseData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Amount",amount);
                editor.putString("TalkTime",talktime);
                editor.putString("Validity",validity);
                editor.putString("Long",longdes);
                editor.putString("OperatorName",Operatorname);
                editor.putString("Mobile",MobileNumber);


                editor.apply();



                BottomSheetDialog dialog = new BottomSheetDialog(context);
                dialog.setContentView(R.layout.fragment_plandth);
                //dialog.show();
                new CustomBottomSheetDialogFragment6().show(((AppCompatActivity)context).getSupportFragmentManager(), "Dialog");
                dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return planDescriptions.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        LinearLayout listlayout;
        TextView desclong,rechargeamount,validity;
        public Holder(@NonNull View itemView) {
            super(itemView);
            listlayout=itemView.findViewById(R.id.listlayout);
            desclong=itemView.findViewById(R.id.desclong);
            rechargeamount=itemView.findViewById(R.id.rechargeamount);
            validity=itemView.findViewById(R.id.validity);

        }
    }
}
