package com.igrand.koinbagvolunterrapp.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.igrand.koinbagvolunterrapp.BestOffersFragment;
import com.igrand.koinbagvolunterrapp.FullTalktimeFragment;
import com.igrand.koinbagvolunterrapp.PopularFragment;

public class MyAdapter2 extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;
    public MyAdapter2(Context context, FragmentManager supportFragmentManager, int tabCount) {

        super(supportFragmentManager);
        myContext = context;
        this.totalTabs = tabCount;
    }

    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                BestOffersFragment bestOffersFragment = new BestOffersFragment();
                return bestOffersFragment;
            case 1:
                PopularFragment popularFragment = new PopularFragment();
                return popularFragment;
            case 2:
                FullTalktimeFragment fullTalktimeFragment = new FullTalktimeFragment();
                return fullTalktimeFragment;

            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;

    }
}

