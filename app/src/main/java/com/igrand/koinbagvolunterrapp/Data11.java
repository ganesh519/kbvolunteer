package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data11 {

    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("commission")
    @Expose
    public String commission;
    @SerializedName("order_id")
    @Expose
    public String orderId;
    @SerializedName("created_at")
    @Expose
    public String createdAt;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("amount", amount).append("commission", commission).append("orderId", orderId).append("createdAt", createdAt).toString();
    }
}
