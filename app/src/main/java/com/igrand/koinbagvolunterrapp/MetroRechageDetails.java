package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MetroRechageDetails extends BaseActivity {

    Button getcardbalance;
    ImageView backblack18;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metro_rechage_details);

        getcardbalance=findViewById(R.id.getcardbalance);
        backblack18=findViewById(R.id.backblack18);
        getcardbalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MetroRechageDetails.this,MetroRechargePay.class);
                startActivity(intent);
            }
        });
        backblack18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MetroRechageDetails.this,MetroRecharge.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
