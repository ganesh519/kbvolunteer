package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Activities.RechargePaymentMobileResponse;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import org.w3c.dom.Text;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class Payment extends BaseActivity {

    private static final String TAG = "payment";
    Button payment;
    ImageView backbutton;
    String amount,operator,circle_code,recharge_type,mobile_number,id,walletbals;
    TextView finalamount,walbal;
    PrefManager prefManager;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);


        payment=findViewById(R.id.payment);
        backbutton=findViewById(R.id.back);
        finalamount=findViewById(R.id.finalamount);
        walbal=findViewById(R.id.walbal);

        prefManager=new PrefManager(getApplicationContext());
        HashMap<String, String> profile=prefManager.getUserDetails();
        id=profile.get("id");
        walletbals=profile.get("Walletbals");



        amount=getIntent().getStringExtra("Amount");
        operator=getIntent().getStringExtra("Operator");
        circle_code=getIntent().getStringExtra("CircleCode");
        recharge_type=getIntent().getStringExtra("RechargeType");
        mobile_number=getIntent().getStringExtra("MobileNumber");


        finalamount.setText(amount);
        walbal.setText(walletbals);


        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent=new Intent(Payment.this,ProceedToRecharge.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);*/
                finish();
            }
        });

        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               String amount=finalamount.getText().toString().trim();
               if(amount.equals("")||amount.equals(null)) {

                   Toast.makeText(Payment.this, "Please enter amount...", Toast.LENGTH_SHORT).show();


               } else {

                   getRecharge(id,amount,operator,circle_code,recharge_type,mobile_number);
               }





            }
        });
    }

    private void getRecharge(String id, String amount, String operator, String circle_code, String recharge_type, String mobile_number) {


        final ProgressDialog progressDialog=new ProgressDialog(Payment.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();



        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<RechargePaymentMobileResponse> call = apiInterface.MobileRecharge(id, amount, operator, circle_code,recharge_type,mobile_number);
        call.enqueue(new Callback<RechargePaymentMobileResponse>() {
            @Override
            public void onResponse(Call<RechargePaymentMobileResponse> call, Response<RechargePaymentMobileResponse> response) {

                if (response.isSuccessful()) ;
                //RechargePaymentMobileResponse operatorFetch = response.body();

               // RechargePaymentMobileResponse.StatusBean statusBean=operatorFetch.getStatus();

                if (response.code() == 200) {
                  //  Log.e(TAG, "onResponse"+statusBean.getMessage() );

                    progressDialog.dismiss();
                    Toast.makeText(Payment.this,"Payment Success", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(Payment.this,DigitalTransactions1.class);
                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                } else if (response.code() == 401) {
                  //  Log.e(TAG, "onResponse:"+statusBean.getMessage() );
                    progressDialog.dismiss();
                    Toast.makeText(Payment.this, "Payment failed", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(Payment.this,MobileRechargeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);


                } else if(response.code() ==400) {
                    progressDialog.dismiss();
                    Toast.makeText(Payment.this, "respond", Toast.LENGTH_SHORT).show();

                }
                else if(response.code() == 500){
                    progressDialog.dismiss();
                    Toast.makeText(Payment.this, "true", Toast.LENGTH_SHORT).show();


                }

            }


            @Override
            public void onFailure(Call<RechargePaymentMobileResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e(TAG, "failed"+t.getMessage() );
                 Toast.makeText(Payment.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });





    }
}
