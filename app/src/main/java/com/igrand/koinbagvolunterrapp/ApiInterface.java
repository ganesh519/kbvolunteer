package com.igrand.koinbagvolunterrapp;

import android.graphics.Bitmap;

import com.igrand.koinbagvolunterrapp.Activities.RechargePaymentMobileResponse;
import com.igrand.koinbagvolunterrapp.Activities.SpinnerModel0;
import com.igrand.koinbagvolunterrapp.Fragments.StatusResponse10;
import com.igrand.koinbagvolunterrapp.Fragments.WalletResponse1;
import com.igrand.koinbagvolunterrapp.Fragments.WalletResponse2;
import com.igrand.koinbagvolunterrapp.Responses.BrowsePlansResponseMobile;

import java.io.File;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-check-phone")
    Call<LoginResponse> Statuss(@Field("mobile_number") String mobile_number);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-login")
    Call<LoginResponse1> Statuss1(@Field("mobile_number") String mobile_number,@Field("password") String password);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-forgot-password")
    Call<LoginResponse2> Statuss2(@Field("mobile_number") String mobile_number);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-check-otp")
    Call<LoginResponse2> Statuss3(@Field("mobile_number") String mobile_number,@Field("otp") String otp);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-reset-password")
    Call<StatusResponse4> Statuss4(@Field("mobile_number") String mobile_number,@Field("password") String password);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-profile")
    Call<StatusResponse5> Statuss5(@Field("id") String id);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-change-password")
    Call<StatusResponse6> Statuss6(@Field("id") String id,@Field("password") String password, @Field("new_password") String new_password);



    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-transaction-view")
    Call<WalletResponse> Statuss7(@Field("id") String id);

    @Multipart
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-request")
    Call<StatusResponse8> Statuss8(@Part MultipartBody.Part file,
                                   @Part("id") RequestBody id,
                                   @Part("amount") RequestBody amount,
                                   @Part("description") RequestBody description,
                                   @Part("bank_details") RequestBody bank_details,
                                   @Part("ref_number") RequestBody ref_number);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-request")
    Call<StatusResponse8> Statuss0(@Field("id") String id,
                                  @Field("amount") String amount,
                                  @Field("description") String description);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-home")
    Call<StatusResponse9> Statuss9(@Field("id") String id);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-transaction-details")
    Call<StatusResponse10> Statuss10(@Field("id") String id,@Field("request_id") String request_id);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-commission")
    Call<LoginResponseCommision> Statuss11(@Field("id") String id);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-login-withotp")
    Call<LoginResponse2> Statuss12(@Field("mobile_number") String mobile_number);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-notifications")
    Call<StatusResponse12> Statuss13(@Field("id") String id);



    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @GET("service-images")
    Call<StatusResponse13> Statuss14();

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-transaction-status")
    Call<WalletResponse1> WalletResponse(@Field("id") String id);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-digital-transaction-view")
    Call<WalletResponse2> WalletResponse1(@Field("id") String id);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("bank-details")
    Call<SpinnerModel> spinner(@Field("id")String id,@Field("created_id") String created_id);


    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @GET("CyrusOperatorFatchAPI.aspx")
    Call<OperatorFetch> operator(@Query("APIID") String APIID, @Query("PASSWORD") String PASSWORD, @Query("MOBILENUMBER") String MOBILENUMBER,
                                @Query("FORMAT") String FORMAT);

    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @GET("CyrusPlanFatchAPI.aspx")
    Call<BrowseFetch> browseplans(@Query("APIID") String APIID, @Query("PASSWORD") String PASSWORD,
                                    @Query("Operator_Code") String Operator_Code, @Query("Circle_Code") String Circle_Code,
                                    @Query("MobileNumber") String MobileNumber,
                                    @Query("PageID") String PageID,
                                    @Query("FORMAT") String FORMAT);


    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @GET("BBPSverify")
    Call<ElectricityFetch> electrictydetails(@Query("tokenkey") String tokenkey, @Query("ReferenceNo") String ReferenceNo,
                                  @Query("BoardCode") String BoardCode);











    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("browse-plan-types")
    Call<BrowsePlansHeadingResponse> browseplanHeadings(@Field("operator_code") String operator_code, @Field("circle_code") String circle_code,
                                  @Field("mobile_number") String mobile_number);


    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @GET("operators")
    Call<Operators> operators();

    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @GET("circles")
    Call<Circles> circles();



    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @GET("board-codes")
    Call<PrepaidResponse1> boardcodes();



    @FormUrlEncoded
    @POST("voulnteer-recharge")
    Call<RechargePaymentMobileResponse>MobileRecharge(@Field("id") String id,@Field("amount") String amount,
                                                       @Field("operator") String operator,@Field("circle_code") String circle_code,
                                                       @Field("recharge_type") String recharge_type,@Field("mobile_number") String mobile_number);


    @FormUrlEncoded
    @POST("voulnteer-dth")
    Call<RechargePaymentMobileResponse>DTHRecharge(@Field("id") String id,@Field("amount") String amount,
                                                       @Field("operator") String operator,@Field("circle_code") String circle_code,
                                                       @Field("recharge_type") String recharge_type,@Field("mobile_number") String mobile_number);


    @FormUrlEncoded
    @POST("voulnteer-data-card")
    Call<RechargePaymentMobileResponse>DataCardRecharge(@Field("id") String id,@Field("amount") String amount,
                                                     @Field("operator") String operator,@Field("circle_code") String circle_code,
                                                     @Field("recharge_type") String recharge_type,@Field("mobile_number") String mobile_number);


    @FormUrlEncoded
    @POST("voulnteer-electricity")
    Call<RechargePaymentMobileResponse>ElectricityRecharge(@Field("id") String id,@Field("amount") String amount,
                                                       @Field("operator") String operator,@Field("circle_code") String circle_code,
                                                       @Field("recharge_type") String recharge_type,@Field("mobile_number") String mobile_number);




    @FormUrlEncoded
    @POST("voulnteer-landline")
    Call<RechargePaymentMobileResponse>LandLineRecharge(@Field("id") String id,@Field("amount") String amount,
                                                           @Field("operator") String operator,@Field("circle_code") String circle_code,
                                                           @Field("recharge_type") String recharge_type,@Field("mobile_number") String mobile_number);


    @FormUrlEncoded
    @POST("voulnteer-broad-band")
    Call<RechargePaymentMobileResponse>BroadBand(@Field("id") String id,@Field("amount") String amount,
                                                        @Field("operator") String operator,@Field("circle_code") String circle_code,
                                                        @Field("recharge_type") String recharge_type,@Field("mobile_number") String mobile_number);



    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("operator-type")
    Call<PrepaidResponse> Prepaid(@Field("type") String type);





    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("operator-type")
    Call<PrepaidResponse> DTH(@Field("type") String type);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("operator-type")
    Call<PrepaidResponse> Datacard(@Field("type") String type);



    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @GET("CyrusPlanFatchAPI.aspx")
    Call<BrowseFetch> browseplansdth(@Query("APIID") String APIID, @Query("PASSWORD") String PASSWORD,
                                  @Query("Operator_Code") String Operator_Code, @Query("Circle_Code") String Circle_Code,
                                     @Query("MobileNumber") String MobileNumber,
                                     @Query("PageID") String PageID,
                                  @Query("FORMAT") String FORMAT);



    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("browse-plans")
    Call<BrowsePlansResponseMobile> browseplansrecharge(@Field("operator_code") String operator_code,
                                                @Field("circle_code") String circle_code,
                                                @Field("mobile_number") String mobile_number);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("plan-fetch.php ")
    Call<BrowseFetch> browseplansdth1(@Field("oc") String oc,
                                                        @Field("phone") String phone);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("plan-fetch.php ")
    Call<BrowsePlansResponseMobile>browseplansdth2(@Field("oc") String oc,
                                      @Field("phone") String phone);



    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("bank-details")
    Call<SpinnerModel0> spinner0(@Field("created_id")String created_id, @Field("id") String id);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-electricity")
    Call<ElectricityPayment>electrictypaymentdetails(@Field("id")String id, @Field("amount") String amount,@Field("req_id")String req_id, @Field("number") String number,@Field("customer_name")String customer_name, @Field("mobile_number") String mobile_number,
                                                     @Field("operator")String operator);

}





