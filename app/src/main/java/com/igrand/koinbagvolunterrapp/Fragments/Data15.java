package com.igrand.koinbagvolunterrapp.Fragments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class Data15 {
    @SerializedName("all")
    @Expose
    public List<All> all = null;
    @SerializedName("pending")
    @Expose
    public List<Pending> pending=null;
    @SerializedName("failed")
    @Expose
    public List<Failure> failed=null ;
    @SerializedName("success")
    @Expose
    public List<Success> success = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("all", all).append("pending", pending).append("failed", failed).append("success", success).toString();
    }

}
