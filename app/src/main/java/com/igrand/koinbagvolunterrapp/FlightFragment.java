package com.igrand.koinbagvolunterrapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

@SuppressLint("ValidFragment")
public class FlightFragment extends Fragment {
    public FlightFragment() {

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_flight, container, false);


        Button searchflight=(Button)v.findViewById(R.id.searchflight);
        searchflight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(getActivity().getApplicationContext(),SearchFlights.class);
                startActivity(intent);*/
                Fragment metroStation=new SearchFlightsFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

                BottomSheetDialog dialog = new BottomSheetDialog(getContext());
                dialog.setContentView(R.layout.fragment_search_flights);
                //dialog.show();
                new CustomBottomSheetDialogFragment8().show(getActivity().getSupportFragmentManager(), "Dialog");
                dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);


                fragmentTransaction.commit();

            }
        });
        return v;
    }
}
