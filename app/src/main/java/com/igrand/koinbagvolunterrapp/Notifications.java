package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notifications extends BaseActivity {

    private static final String MY_PREFS_NAME = "MyPrefsFile";
    ImageView back12;
    ApiInterface apiInterface;
    Integer size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);



        final String id = getIntent().getStringExtra("Id");

        final ProgressDialog progressDialog=new ProgressDialog(Notifications.this);
        progressDialog.setMessage("Loading.........");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusResponse12> call = apiInterface.Statuss13(id);
        call.enqueue(new Callback<StatusResponse12>() {
            @Override
            public void onResponse(Call<StatusResponse12> call, Response<StatusResponse12> response) {
                if (response.isSuccessful()) ;

                StatusResponse12 statusResponse = response.body();
                if(statusResponse!=null) {


                    StatusDataResponse12 statusDataResponse = statusResponse.status;

                    if (statusDataResponse.code == 200) {
                        progressDialog.dismiss();

                        List<Data12> walletdata = statusResponse.data;


                        if (statusResponse.data != null) {


                            RecyclerView recyclerView = findViewById(R.id.recyclerview1);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Notifications.this);
                            recyclerView.setLayoutManager(linearLayoutManager);
                            RecyclerAdapterWallet4 adapterWallet = new RecyclerAdapterWallet4(getApplicationContext(), walletdata);
                            recyclerView.setAdapter(adapterWallet);


                        }
                    } else if (statusDataResponse.code == 409) {
                        progressDialog.dismiss();
                        Toast.makeText(Notifications.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }
                else {

                    progressDialog.dismiss();
                    Toast.makeText(Notifications.this, "No Data", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<StatusResponse12> call, Throwable t) {
                progressDialog.dismiss();
                //Toast.makeText(Notifications.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(Notifications.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });

        back12=findViewById(R.id.back12);
        back12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
