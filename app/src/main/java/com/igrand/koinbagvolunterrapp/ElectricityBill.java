package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.Adapters.MyAdapter5;

import java.util.HashMap;

public class ElectricityBill extends BaseActivity {

    Button electricitybutton1;
    ImageView backelectric;
    TabLayout tabLayout3;
    ViewPager viewPager3;

    String statename,stateboard,amount,id,operator,circle_code,circlecode,operatorcode,recharge_type,mobile_number,refno,token,
            customername,customeraddress,duedate,billamount,req_id,consumer_name,consumer_mobile,ConsumerName,ConsumerMobile;
    PrefManager prefManager;
    EditText stateid,boardid,consumerid;
    TextView name,address,duedate1,amount1,amountelec,consumername,consumermobile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_electricity_bill);

        amountelec=findViewById(R.id.amountelec);
        stateid=findViewById(R.id.stateid);
        boardid=findViewById(R.id.boardid);
        consumerid=findViewById(R.id.consumerid);
        consumername = findViewById(R.id.consumername);
        consumermobile= findViewById(R.id.consumermobile);


        name=findViewById(R.id.name);
        address=findViewById(R.id.address);
        duedate1=findViewById(R.id.duedate);
        amount1=findViewById(R.id.billamount);

        prefManager=new PrefManager(getApplicationContext());
        HashMap<String, String> profile=prefManager.getUserDetails();



        statename = getIntent().getStringExtra("StateName");
        stateboard = getIntent().getStringExtra("StateBoard");
        mobile_number = getIntent().getStringExtra("StateConsumer");
        circlecode = getIntent().getStringExtra("CircleCode");


        customername = getIntent().getStringExtra("costumer_name");
        customeraddress = getIntent().getStringExtra("customer_address");
        duedate = getIntent().getStringExtra("duedate");
        billamount = getIntent().getStringExtra("billamount");
        operatorcode = getIntent().getStringExtra("OperatorCode");

        req_id = getIntent().getStringExtra("req_id");
        refno = getIntent().getStringExtra("RefNo");
        token = getIntent().getStringExtra("Token");



        name.setText(customername);
        address.setText(customeraddress);
        duedate1.setText(duedate);
        amount1.setText(billamount);
        amountelec.setText(billamount);

        stateid.setText(statename);
        boardid.setText(stateboard);
        consumerid.setText(mobile_number);


        id=profile.get("id");

        operator=operatorcode + "@" + stateboard;
        circle_code=circlecode + "@" + statename;
        recharge_type="electricity";




        electricitybutton1=findViewById(R.id.electricitybutton1);
       // tabLayout3=(TabLayout)findViewById(R.id.tabLayout33);
        backelectric=findViewById(R.id.backelectric);
        backelectric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //viewPager3=(ViewPager)findViewById(R.id.viewPager33);
        electricitybutton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConsumerName = consumername.getText().toString();
                ConsumerMobile = consumermobile.getText().toString();



                Intent intent=new Intent(ElectricityBill.this,Paymentelectricity.class);


                amount=amountelec.getText().toString();
                mobile_number=consumerid.getText().toString();
                intent.putExtra("Amount",amount);
                intent.putExtra("Operator",operator);
                intent.putExtra("CircleCode",circle_code);
                intent.putExtra("RechargeType",recharge_type);
                intent.putExtra("MobileNumber",mobile_number);
                intent.putExtra("req_id",req_id);
                intent.putExtra("refno",refno);
                intent.putExtra("token",token);
                intent.putExtra("consumer_name", ConsumerName);
                intent.putExtra("consumer_mobile", ConsumerMobile);


                startActivity(intent);
            }
        });

    }
}
