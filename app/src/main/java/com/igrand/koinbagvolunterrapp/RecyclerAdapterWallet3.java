package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class RecyclerAdapterWallet3 extends RecyclerView.Adapter<RecyclerAdapterWallet3.ViewHolderClass> {

    Context context;

    List<LoginResponseCommision.DataBean3> walletdata;


    public RecyclerAdapterWallet3(Context context, List<LoginResponseCommision.DataBean3> walletdata) {

        this.context=context;
        this.walletdata=walletdata;

    }

    @NonNull
    @Override
    public RecyclerAdapterWallet3.ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.recycler_list3,viewGroup,false);
        ViewHolderClass viewHolderClass=new ViewHolderClass(view);
        return viewHolderClass;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterWallet3.ViewHolderClass viewHolderClass, int i) {

        viewHolderClass.amount11.setText(walletdata.get(i).amount);
        viewHolderClass.commission11.setText(walletdata.get(i).commission);
        viewHolderClass.orderid11.setText(walletdata.get(i).orderId);
        viewHolderClass.createdat11.setText(walletdata.get(i).createdAt);


    }

    @Override
    public int getItemCount() {
        return walletdata.size();
    }

    public class ViewHolderClass extends RecyclerView.ViewHolder {

        TextView amount11,commission11,orderid11,createdat11;

        public ViewHolderClass(@NonNull View itemView) {
            super(itemView);

            amount11=itemView.findViewById(R.id.amount11);
            commission11=itemView.findViewById(R.id.commission11);
            orderid11=itemView.findViewById(R.id.orderid11);
            createdat11=itemView.findViewById(R.id.created_at11);



        }
    }
}
