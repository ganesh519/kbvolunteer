package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomBottomSheetDialogFragment1 extends BottomSheetDialogFragment {

    String Amount,TalkTime,Validity,Longs,Mobile,Circle_Code,MobileNumber,longdes,mobilenumber;
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_plan_details, container, false);

        Button selectplan = (Button)v.findViewById(R.id.selectplan);

        TextView amount=v.findViewById(R.id.amount);
        LinearLayout texts=v.findViewById(R.id.texts);
        LinearLayout data=v.findViewById(R.id.data);
        TextView calls=v.findViewById(R.id.calls);
        TextView validity=v.findViewById(R.id.validity);
        TextView longdesc=v.findViewById(R.id.longdesc);


        Bundle arguments = this.getArguments();

        if (arguments != null) {
            Amount = getArguments().getString("Amount");
            Circle_Code = getArguments().getString("TalkTime");
            MobileNumber = getArguments().getString("Validity");
            longdes = getArguments().getString("Long");
            mobilenumber = getArguments().getString("Mobile");



        }


       /* SharedPreferences sharedPreferences=getActivity().getSharedPreferences("BrowseData", Context.MODE_PRIVATE);
        final String Amount=sharedPreferences.getString("Amount","'");
        String Circle_Code=sharedPreferences.getString("TalkTime","'");
        final String MobileNumber=sharedPreferences.getString("Validity","'");
        final String longdes=sharedPreferences.getString("Long","'");
        final String mobilenumber=sharedPreferences.getString("Mobile","'");
*/

        SharedPreferences sharedPreferences1=getActivity().getSharedPreferences("Data", Context.MODE_PRIVATE);
        final String Operator_Code1=sharedPreferences1.getString("Operator","'");
        final String Circle_Code1=sharedPreferences1.getString("Circle","'");


        amount.setText(Amount);
        validity.setText(Validity);
        longdesc.setText(longdes);
        calls.setText(Mobile);

        selectplan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getActivity(),ProceedToRecharge.class);
                intent.putExtra("Operate",Amount);
                intent.putExtra("Operator",Operator_Code1);
                intent.putExtra("Circle",Circle_Code1);
                intent.putExtra("LongDesc",longdes);
                intent.putExtra("Mobile",mobilenumber);

                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        ImageView imageView=(ImageView)v.findViewById(R.id.exit2);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();

            }
        });

        return v;
    }

}
