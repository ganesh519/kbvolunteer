package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class SelectedSeat extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_selected_seat, container, false);

        Button selectedseat=(Button)v.findViewById(R.id.selectedseat);
        selectedseat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getActivity(),TravellerInfoBus.class);
                startActivity(intent);
            }
        });


        return v;
    }
}
