package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class CirclesRecyclerAdapterelectricity extends RecyclerView.Adapter<CirclesRecyclerAdapterelectricity.Holder> {

    List<DataCircle> dataBeanList;
    Context context;

    public CirclesRecyclerAdapterelectricity(List<DataCircle> data7, FragmentActivity activity) {
        this.dataBeanList=data7;
        this.context=activity;
    }

    @NonNull
    @Override
    public CirclesRecyclerAdapterelectricity.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_circles, viewGroup, false);

        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CirclesRecyclerAdapterelectricity.Holder holder, final int i) {

        holder.txt_circle.setText(dataBeanList.get(i).circleName);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context,Electricity.class);
                intent.putExtra("CircleName",dataBeanList.get(i).circleName);
                intent.putExtra("CircleCode",dataBeanList.get(i).circleCode);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });





    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

   public class Holder extends RecyclerView.ViewHolder{
        TextView txt_circle;
        // ImageView image_network;

        public Holder(@NonNull View itemView) {
            super(itemView);

            //  image_network=itemView.findViewById(R.id.image_network);
            txt_circle=itemView.findViewById(R.id.txt_circle);

        }
    }
}
