package com.igrand.koinbagvolunterrapp.Adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.BrowsePlans1;
import com.igrand.koinbagvolunterrapp.BrowsePlansHeadingResponse;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.Fragments.Approved;
import com.igrand.koinbagvolunterrapp.Fragments.Data10;
import com.igrand.koinbagvolunterrapp.Fragments.StatusDataResponse10;
import com.igrand.koinbagvolunterrapp.Fragments.StatusResponse10;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.ProfileImage;
import com.igrand.koinbagvolunterrapp.R;
import com.igrand.koinbagvolunterrapp.TopupResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BrowsePlanHeaderRecyclerAdapter extends RecyclerView.Adapter<BrowsePlanHeaderRecyclerAdapter.Holder> {


    List<BrowsePlansHeadingResponse.DataBean> dataBeanList;
    Context context;
    BrowsePlans1 browsePlans1;
    boolean click = true;
    private int lastPosition = -1;


    public BrowsePlanHeaderRecyclerAdapter(List<BrowsePlansHeadingResponse.DataBean> dataBeanList, Context applicationContext, BrowsePlans1 browsePlans1) {
        this.dataBeanList=dataBeanList;
        this.context=applicationContext;
        this.browsePlans1=browsePlans1;
    }

    @NonNull
    @Override
    public BrowsePlanHeaderRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_plansheading, viewGroup, false);

        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final BrowsePlanHeaderRecyclerAdapter.Holder holder, final int position) {


        holder.txt_heading.setText(dataBeanList.get(position).getKey());

        if (position == 0){


            SharedPreferences sharedPreferences = context.getSharedPreferences("Data", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("b_type", dataBeanList.get(position).getKey());
            editor.apply();
           //notifyDataSetChanged();
            //browsePlans1.cat(dataBeanList.get(position).getKey(), context);


        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                for (int i = 0; i < dataBeanList.size(); i++) {
                    if (i == position) {
                        dataBeanList.get(i).isSelected = true;


                        //holder.text_name.setTextColor(Color.parseColor("#ff0000"));
                    }
                    else {
                        dataBeanList.get(i).isSelected = false;
                        //  holder.text_name.setTextColor(Color.parseColor("#000000"));

                    }

                }


                SharedPreferences sharedPreferences = context.getSharedPreferences("Data", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("b_type", dataBeanList.get(position).getKey());
                editor.apply();
                notifyDataSetChanged();
                browsePlans1.cat(dataBeanList.get(position).getKey(), context);
                //dataBeanList.clear();




            }
        });


        if (dataBeanList.get(position).isSelected) {
//            holder.tabs.setBackgroundColor(Color.parseColor("#ff0000"));
            //holder.tabs.setBackgroundResource(R.drawable.button_background);
            holder.txt_heading.setTextColor(Color.parseColor("#FFD71B"));
            holder.view.setVisibility(View.VISIBLE);

        } else {
//            holder.tabs.setBackgroundColor(Color.parseColor("#FFFFFF"));
            //holder.tabs.setBackgroundResource(R.drawable.buttonblue);
            holder.txt_heading.setTextColor(Color.parseColor("#FFFFFF"));
            holder.view.setVisibility(View.GONE);


        }
    }


    private void setAnimation(View itemView, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(itemView.getContext(), android.R.anim.slide_in_left);
            itemView.startAnimation(animation);
            lastPosition = position;
        }
    }



    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        TextView request_date,amount,txt_heading,updated_date,money3;
        ImageView image;
        RelativeLayout tabs;
        View view;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txt_heading=itemView.findViewById(R.id.txt_heading);
            tabs=itemView.findViewById(R.id.tabs);
            view=itemView.findViewById(R.id.view);


        }
    }
}
