package com.igrand.koinbagvolunterrapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.DataCircle;
import com.igrand.koinbagvolunterrapp.ProceedToRecharge;
import com.igrand.koinbagvolunterrapp.R;

import java.util.List;

public class CirclesRecyclerAdapter extends RecyclerView.Adapter<CirclesRecyclerAdapter.Holder> {
    List<DataCircle> dataBeanList;
    Context context;
    String operatorname,operatortype,operatorimage,operatorcode,mobilenumber;



    public CirclesRecyclerAdapter(List<DataCircle> data7, Context context, String opertatorname, String opertatortype, String opertatorimage, String opertatorcode, String mobileNumber) {
        this.dataBeanList=data7;
        this.context=context;
        this.operatorname=opertatorname;
        this.operatortype=opertatortype;
        this.operatorimage=opertatorimage;
        this.operatorcode=opertatorcode;
        this.mobilenumber=mobileNumber;
    }


    @NonNull
    @Override
    public CirclesRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_circles, viewGroup, false);

        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CirclesRecyclerAdapter.Holder holder, final int i) {

        holder.txt_circle.setText(dataBeanList.get(i).circleName);




        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context, ProceedToRecharge.class);
                intent.putExtra("Operatorname",operatorname);
                intent.putExtra("Operatortype",operatortype);
                intent.putExtra("Operatorimage",operatorimage);
                intent.putExtra("Operator",operatorcode);
                intent.putExtra("Circle",dataBeanList.get(i).circleCode);
                intent.putExtra("CircleName",dataBeanList.get(i).circleName);
                intent.putExtra("Mobile",mobilenumber);

                context.startActivity(intent);
                
            }
        });



    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }
    class Holder extends RecyclerView.ViewHolder{
        TextView txt_circle;
       // ImageView image_network;

        public Holder(@NonNull View itemView) {
            super(itemView);

          //  image_network=itemView.findViewById(R.id.image_network);
            txt_circle=itemView.findViewById(R.id.txt_circle);

        }
    }
}
