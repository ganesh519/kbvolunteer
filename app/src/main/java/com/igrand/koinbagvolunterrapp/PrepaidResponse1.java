package com.igrand.koinbagvolunterrapp;

import java.util.List;

public class PrepaidResponse1 {


    /**
     * status : {"code":200,"message":"Board Codes Data"}
     * data : [{"id":"1","board_name":"Ajmer Vidyut Vitran Nigam Limited (AVVNL)","board_code":"BBPSAJVVNL"},{"id":"2","board_name":"Assam Power Distribution Company Ltd (RAPDR)","board_code":"BBPSASSAM"},{"id":"3","board_name":"Assam Power Distribution Company Ltd (NON-RAPDR)","board_code":"BBPSASMPNR"},{"id":"4","board_name":"Bangalore Electricity Supply Co. Ltd (BESCOM)","board_code":"BBPSBESCOM"},{"id":"5","board_name":"B.E.S.T. Undertaking","board_code":"BESTINSTA"},{"id":"6","board_name":"Bharatpur Electricity Services Ltd","board_code":"BBPSBRTELE"},{"id":"7","board_name":"Bikaner Electricity Supply Limited (BkESL)","board_code":"BBPSBKESL"},{"id":"8","board_name":"Brihan Mumbai Electric Supply and Transport Undertaking","board_code":"BBPSBEST"},{"id":"9","board_name":"BSES (Rajdhani) Delhi(NCR)","board_code":"BBPSBRAJ"},{"id":"10","board_name":"BSES (Yamuna) Delhi(NCR)","board_code":"BBPSBYPL"},{"id":"11","board_name":"Calcutta Electric Supply Corporation (CESC)","board_code":"BBPSCESC"},{"id":"12","board_name":"Jodhpur Vidhyut Vitran Nigam Ltd.","board_code":"BBPSJDVVNL"},{"id":"13","board_name":"Jharkhand Bijli Vitran Nigam Limited (JBVNL)","board_code":"BBPSJBVNL"},{"id":"14","board_name":"Jaipur Vidyut Vitran Nigam (JVVNL)","board_code":"BBPSJVVNL"},{"id":"15","board_name":"Kota Electricity Distribution Limited (KEDL)","board_code":"BBPSKOTAEL"},{"id":"16","board_name":"M.P. Madhya Kshetra Vidyut Vitaran - RURAL","board_code":"BBPSMPMKR"},{"id":"17","board_name":"M.P. Madhya Kshetra Vidyut Vitaran - URBAN","board_code":"BBPSMPMKU"},{"id":"18","board_name":"M.P. Paschim Kshetra Vidyut Vitaran","board_code":"BBPSMPPK"},{"id":"19","board_name":"Madhya Gujarat Vij Company Limited (MGVCL)","board_code":"BBPSMGVCL"},{"id":"20","board_name":"Maharashtra State Electricity Distbn Co Ltd","board_code":"BBPSMSEDC"},{"id":"21","board_name":"MP Paschim Kshetra Vidyut Vitaran Co. Ltd (Indore)","board_code":"MPPKVINSTA"},{"id":"22","board_name":"MSEB / MSEDC Ltd. Maharashtra","board_code":"MSEBMUM"},{"id":"23","board_name":"Muzaffarpur Vidyut Vitran Limited","board_code":"BBPSMUZVVL"},{"id":"24","board_name":"NESCO, Odisha","board_code":"BBPSNESCO"},{"id":"25","board_name":"Noida Power","board_code":"BBPSNPCL"},{"id":"26","board_name":"North Bihar Power Distribution Company Ltd","board_code":"BBPSNBIHPD"},{"id":"27","board_name":"Paschim Gujarat Vij Company Limited (PGVCL)","board_code":"BBPSPGVCL"},{"id":"28","board_name":"Punjab State Power Corporation Ltd (PSPCL)","board_code":"BBPSPSPCL"},{"id":"29","board_name":"Reliance Energy","board_code":"BBPSREL"},{"id":"30","board_name":"SNDL Nagpur","board_code":"BBPSSNDLNG"},{"id":"31","board_name":"South Bihar Power Distribution Company Ltd","board_code":"BBPSSBIHPD"},{"id":"32","board_name":"SOUTHCO, Odisha","board_code":"BBPSSOUTHC"},{"id":"33","board_name":"Southern Power Distribution Company of AP Ltd","board_code":"BBPSSPDCAP"},{"id":"34","board_name":"Tata Power ? Delhi","board_code":"BBPSTATPD"},{"id":"35","board_name":"Tata Power ? Mumbai","board_code":"BBPSTATPM"},{"id":"36","board_name":"Tamil Nadu Electricity Board (TNEB)","board_code":"BBPSTNEB"},{"id":"37","board_name":"Torrent Power ? Agra","board_code":"BBPSTORPAG"},{"id":"38","board_name":"Torrent Power ? Ahmedabad","board_code":"BBPSTORPAH"},{"id":"39","board_name":"Torrent Power ? Bhiwandi","board_code":"BBPSTORPBH"},{"id":"40","board_name":"Torrent Power ? Surat","board_code":"BBPSTORPSU"},{"id":"41","board_name":"TSSPDCL","board_code":"APCPDCL"},{"id":"42","board_name":"TP Ajmer Distribution Ltd (TPADL)","board_code":"BBPSTPAJMR"},{"id":"43","board_name":"Tripura Electricity Corp Ltd","board_code":"BBPSTRIPEL"},{"id":"44","board_name":"Uttar Gujarat Vij Company Limited (UGVCL)","board_code":"BBPSUGVCL"},{"id":"45","board_name":"Uttar Haryana Bijli Vitran Nigam (UHBVN)","board_code":"BBPSUHBVN"},{"id":"46","board_name":"Uttar Pradesh Power Corp Ltd (UPPCL) - URBAN","board_code":"BBPSUPPOCL"},{"id":"47","board_name":"Uttar Pradesh Power Corp Ltd (UPPCL) - RURAL","board_code":"BBPSUPPCLR"},{"id":"48","board_name":"Uttarakhand Power Corporation Limited","board_code":"BBPSUPPCL"},{"id":"49","board_name":"WESCO Utility","board_code":"BBPSWESCO"},{"id":"50","board_name":"West Bengal State Electricity Distribution Co. Ltd (WBSEDCL)","board_code":"BBPSWBSEDCL"},{"id":"51","board_name":"Maharashtra State Electricity Distbn Co Ltd","board_code":"BBPSMSEDCL"},{"id":"52","board_name":"Telangana State Southern Power Distribution Company Ltd.","board_code":"BBPSTSSPDCL"},{"id":"53","board_name":"Delhi Jal Board","board_code":"BBPSDJB"},{"id":"54","board_name":"MaharashtraElectricity","board_code":"BBPSBESTINSTA"},{"id":"55","board_name":"Andhra_Pradesh_Southern_Power","board_code":"BBPSAPSPDCL"},{"id":"56","board_name":"Karnataka_Hubli_Electricity","board_code":"BBPSHESCOM"},{"id":"57","board_name":"Karnataka_Gulbarga_Electricity","board_code":"BBPSGESCOM"},{"id":"58","board_name":"Karnataka_Chamundeshwari_Electricity","board_code":"BBPSCHESC"},{"id":"59","board_name":"Maharashtra_2461_KANDHAR","board_code":"BBPSMSEDCL2461KANDHAR"},{"id":"60","board_name":"Indraprasth_Gas","board_code":"BBPSINPGAS"},{"id":"61","board_name":"Gurat_Gas","board_code":"BBPSGUJGAS"},{"id":"62","board_name":"Dakshin_Gujarat_Vij_Company_Ltd","board_code":"BBPSDGVCL"},{"id":"63","board_name":"JharkhandElectricityGovindrpr","board_code":"BBPSJBVNLGOVINDPUR"},{"id":"64","board_name":"Andhra_Pradesh_Southern_Power","board_code":"BBPSAPSPDCL"},{"id":"65","board_name":"Himachal Electricity","board_code":"BBPSHPSEBL"},{"id":"66","board_name":"Hyderabad Metropolitan Water Supply and Sewerage Board","board_code":"BBPSHMWSSB"},{"id":"67","board_name":"Municipal Corporation of Gurugram","board_code":"BBPSMCGUR"},{"id":"68","board_name":"Urban Improvement Trust (UIT) - Bhiwadi","board_code":"BBPSUITB"},{"id":"69","board_name":"Uttarakhand Jal Sansthan","board_code":"BBPSUJSO"},{"id":"70","board_name":"Adani Gas ? Haryana","board_code":"BBPSADAHA"},{"id":"71","board_name":"harotar Gas Sahakari Mandali Limited","board_code":"BBPSCHAROG"},{"id":"72","board_name":"Haryana City Gas - Kapil Chopra Enterprise","board_code":"BBPSHARCG"},{"id":"73","board_name":"Mahanagar Gas Limited","board_code":"BBPSMGL"},{"id":"74","board_name":"Maharashtra Natural Gas Limited (MNGL)","board_code":"BBPSMNGL"},{"id":"75","board_name":"Sabarmati Gas Limited (SGL)","board_code":"BBPSSGL"},{"id":"76","board_name":"Siti Energy - Uttar Pradesh","board_code":"BBPSSITIUT"},{"id":"77","board_name":"Tripura Natural Gas","board_code":"BBPSTRIGAS"},{"id":"78","board_name":"Unique Central Piped Gases Pvt Ltd (UCPGPL)","board_code":"BBPSUCPGPL"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Board Codes Data
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * board_name : Ajmer Vidyut Vitran Nigam Limited (AVVNL)
         * board_code : BBPSAJVVNL
         */

        private String id;
        private String board_name;
        private String board_code;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBoard_name() {
            return board_name;
        }

        public void setBoard_name(String board_name) {
            this.board_name = board_name;
        }

        public String getBoard_code() {
            return board_code;
        }

        public void setBoard_code(String board_code) {
            this.board_code = board_code;
        }
    }
}
