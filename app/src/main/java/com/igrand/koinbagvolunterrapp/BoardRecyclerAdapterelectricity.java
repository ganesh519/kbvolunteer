package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import static android.app.Activity.RESULT_OK;

public class BoardRecyclerAdapterelectricity extends RecyclerView.Adapter<BoardRecyclerAdapterelectricity.Holder> {

    List<PrepaidResponse1.DataBean> dataBeans;
    Context context;
    String circlename,circlecode;


    public BoardRecyclerAdapterelectricity(List<PrepaidResponse1.DataBean> dataBeanList, FragmentActivity activity, String circlename, String circlecode) {
        this.dataBeans=dataBeanList;
        this.context=activity;
        this.circlename=circlename;
        this.circlecode=circlecode;
    }

    @NonNull
    @Override
    public BoardRecyclerAdapterelectricity.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_circles1, viewGroup, false);

        return new Holder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull BoardRecyclerAdapterelectricity.Holder holder, final int i) {

        holder.txt_circle.setText(dataBeans.get(i).getBoard_name());
        holder.txt_circle1.setText(dataBeans.get(i).getBoard_code());





//        Picasso.get().load(image).error(R.drawable.kb).into(holder.imgele);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,Electricity.class);
                intent.putExtra("OperatorName",dataBeans.get(i).getBoard_name());
                intent.putExtra("OperatorCode",dataBeans.get(i).getBoard_code());
                intent.putExtra("CircleName",circlename);
                intent.putExtra("CircleCode",circlecode);
                intent.putExtra("Activity","Electricity");

                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

        public class Holder extends RecyclerView.ViewHolder{
            TextView txt_circle,txt_circle1;
            ImageView imgele;
            // ImageView image_network;

            public Holder(@NonNull View itemView) {
                super(itemView);

                //  image_network=itemView.findViewById(R.id.image_network);
                txt_circle=itemView.findViewById(R.id.txt_circle);
                txt_circle1=itemView.findViewById(R.id.txt_circle1);


            }
        }
    }
