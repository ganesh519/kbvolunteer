package com.igrand.koinbagvolunterrapp.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.BaseActivity;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.LoginResponse;
import com.igrand.koinbagvolunterrapp.NetworkChecking;
import com.igrand.koinbagvolunterrapp.R;
import com.igrand.koinbagvolunterrapp.WelcomeBack;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomDialogActivity extends BaseActivity implements ConnectivityReceiver.ConnectivityReceiverListener{
    ApiInterface apiInterface;
    LoginResponse.StatusBean statusBean;
    AppController appController;
    private boolean checkInternet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);

//        checkInternet = NetworkChecking.isConnected(getApplicationContext());
//        if (checkInternet) {
//            init();
//
//        }
//        else {
//            setContentView(R.layout.internet);
//            Button tryButton = (Button) findViewById(R.id.btnTryagain);
//            tryButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
////                    invalidateOptionsMenu();
//                    Intent intent = new Intent(getApplicationContext(), Login.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                    //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                }
//            });
//        }

       /* appController = (AppController) getApplication();
        if (appController.isConnection()) {

            init();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), Login.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }
        }*/



        checkConnection();

    }

    private void checkConnection() {

        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);



    }

    private void showSnack(boolean isConnected) {

        if(isConnected) {

            String mobile_number = getIntent().getStringExtra("Mobile");
            if (!mobile_number.isEmpty()) {


                final Dialog progressDialog = new Dialog(CustomDialogActivity.this, R.style.Theme_AppCompat_Light_NoActionBar);
                progressDialog.setContentView(R.layout.custom_dialog);
                Window window = progressDialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                LottieAnimationView animationView = progressDialog.findViewById(R.id.lottie_view);
                animationView.setAnimation("refresh.json");
                animationView.playAnimation();
                animationView.loop(true);
                progressDialog.show();
                TextView text = (TextView) progressDialog.findViewById(R.id.text);
                ImageView image = (ImageView) progressDialog.findViewById(R.id.image);


                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<LoginResponse> call = apiInterface.Statuss(mobile_number);
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                            LoginResponse.DataBean dataBean = response.body().getData();
                            Intent intent = new Intent(CustomDialogActivity.this, WelcomeBack.class);
                            intent.putExtra("Name1", dataBean.getFirstName());
                            intent.putExtra("SurName1", dataBean.getSurname());
                            intent.putExtra("Mobile_number", dataBean.getMobileNumber());
                            startActivity(intent);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(CustomDialogActivity.this, "Volunteer does not Exist ,Please Contact Koinbag..", Toast.LENGTH_SHORT).show();
                            finish();

                        }

                    }


                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                        Toast toast= Toast.makeText(CustomDialogActivity.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            } else {
                Toast.makeText(getApplicationContext(), "Please enter MobileNumber", Toast.LENGTH_LONG).show();
            }

        }

        else {

            setContentView(R.layout.internet);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}










