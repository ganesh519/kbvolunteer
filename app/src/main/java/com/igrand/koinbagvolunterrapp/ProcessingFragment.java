package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.Fragments.Data15;
import com.igrand.koinbagvolunterrapp.Fragments.Pending;
import com.igrand.koinbagvolunterrapp.Fragments.ProcessingRecyclerAdapter;
import com.igrand.koinbagvolunterrapp.Fragments.StatusDataResponse15;
import com.igrand.koinbagvolunterrapp.Fragments.Success;
import com.igrand.koinbagvolunterrapp.Fragments.WalletResponse2;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

public class ProcessingFragment extends Fragment {

    ApiInterface apiInterface;
    RecyclerView recycler_success;
    PrefManager prefManager;
    String Id;
    ProcessingRecyclerAdapter processingRecyclerAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View v = inflater.inflate(R.layout.fragment_process, container, false);

        recycler_success=v.findViewById(R.id.recycler_process);

        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");


       final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletResponse2> call = apiInterface.WalletResponse1(Id);
        call.enqueue(new Callback<WalletResponse2>() {
            @Override
            public void onResponse(Call<WalletResponse2> call, Response<WalletResponse2> response) {
               // if (response.isSuccessful()) ;

                    WalletResponse2 walletResponse1 = response.body();
                    if(walletResponse1!=null) {


                        StatusDataResponse15 statusDataResponse7 = walletResponse1.status;
                        if (statusDataResponse7.code == 200) {
                            progressDialog.dismiss();

                            Data15 data15 = walletResponse1.data;

                            if (walletResponse1.data.pending != null) {


                                List<Pending> proceesingList = data15.pending;

                                recycler_success.setLayoutManager(new LinearLayoutManager(getActivity()));
                                processingRecyclerAdapter = new ProcessingRecyclerAdapter(getContext(), proceesingList, statusDataResponse7, walletResponse1);
                                recycler_success.setAdapter(processingRecyclerAdapter);


                            }
                        } else {
                            progressDialog.dismiss();
                            //Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                        }
                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                    }

                }



            @Override
            public void onFailure(Call<WalletResponse2> call, Throwable t) {
                progressDialog.dismiss();
               // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(getActivity(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

        return v;
    }
}
