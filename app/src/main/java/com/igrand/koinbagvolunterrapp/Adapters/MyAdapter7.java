package com.igrand.koinbagvolunterrapp.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.igrand.koinbagvolunterrapp.CustomBottomSheetDialogFragment10;
import com.igrand.koinbagvolunterrapp.FilterFragment;
import com.igrand.koinbagvolunterrapp.SortFragment;

public class MyAdapter7 extends FragmentPagerAdapter {

    private CustomBottomSheetDialogFragment10 myContext;
    int totalTabs;
    public MyAdapter7(CustomBottomSheetDialogFragment10 context, FragmentManager supportFragmentManager, int tabCount) {

        super(supportFragmentManager);
        myContext = context;
        this.totalTabs = tabCount;
    }

    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                SortFragment sortFragment = new SortFragment();
                return sortFragment;
            case 1:
                FilterFragment filterFragment = new FilterFragment();
                return filterFragment;


            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;

    }
}

