package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class StatusResponse1 {

    @SerializedName("status")
    @Expose
    public StatusDataResponse1 status;
    @SerializedName("data")
    @Expose
    public Data1 data;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }

}
