package com.igrand.koinbagvolunterrapp.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Adapters.RequestRecyclerAdapter;
import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.R;
import com.igrand.koinbagvolunterrapp.StatusDataResponse7;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

public class RequestsFragment1 extends Fragment {
    RecyclerView recycler_request;
    PrefManager prefManager;
    ApiInterface apiInterface;
    String Id;
    List<Proceesing> proceesingList;
    RequestRecyclerAdapter requestRecyclerAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_request, container, false);
        recycler_request = v.findViewById(R.id.recycler_request);

        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");


        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletResponse1> call = apiInterface.WalletResponse(Id);
        call.enqueue(new Callback<WalletResponse1>() {
            @Override
            public void onResponse(Call<WalletResponse1> call, Response<WalletResponse1> response) {
               // if (response.isSuccessful()) ;


                WalletResponse1 walletResponse1 = response.body();

                if (walletResponse1!=null) {
                    StatusDataResponse7 statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog.dismiss();
//                    Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                        Data14 data14List = walletResponse1.data;

                        if (walletResponse1.data.proceesing != null) {

                            proceesingList = data14List.proceesing;

                            recycler_request.setLayoutManager(new LinearLayoutManager(getActivity()));
                            requestRecyclerAdapter = new RequestRecyclerAdapter(getContext(), proceesingList);
                            recycler_request.setAdapter(requestRecyclerAdapter);
                        } else {
                            progressDialog.dismiss();
                            //Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                        }




                }else if (statusDataResponse7.code != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                }

                }

                else {

                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<WalletResponse1> call, Throwable t) {
                progressDialog.dismiss();
               // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(getActivity(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();



            }
        });


        return v;
    }
}
