package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Adapters.OperatorRecyclerAdapter;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OperatorActivityDataCard extends BaseActivity {


    RecyclerView recycler_operator;
    String typee,MobileNumber;
    ApiInterface apiInterface;
    OperatorRecyclerAdapterDataCard operatorRecyclerAdapter;
    ImageView back4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operator_data_card);

        back4=findViewById(R.id.back4);
        back4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        MobileNumber = getIntent().getStringExtra("Mobile");

        if (getIntent() != null){
            typee=getIntent().getStringExtra("TYPEE");
        }

        recycler_operator=findViewById(R.id.recycler_operator);

        getprepaid(typee);


    }

    private void getprepaid(String type) {

        final ProgressDialog progressDialog=new ProgressDialog(OperatorActivityDataCard.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();



        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PrepaidResponse> call = apiInterface.Prepaid(type);
        call.enqueue(new Callback<PrepaidResponse>() {
            @Override
            public void onResponse(Call<PrepaidResponse> call, Response<PrepaidResponse> response) {

                if (response.isSuccessful()) ;

                PrepaidResponse prepaidResponse=response.body();

                PrepaidResponse.StatusBean statusBean=prepaidResponse.getStatus();
                if (statusBean.getCode() == 200){
                    progressDialog.dismiss();

                    List<PrepaidResponse.DataBean> dataBeanList=prepaidResponse.getData();

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
                    recycler_operator.setLayoutManager(mLayoutManager);
                    operatorRecyclerAdapter=new OperatorRecyclerAdapterDataCard(dataBeanList,OperatorActivityDataCard.this,MobileNumber);
                    recycler_operator.setAdapter(operatorRecyclerAdapter);

                    // homeRecycler.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
                    //homeRecycler.setItemAnimator(new DefaultItemAnimator());

                    //RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
                    //popularRecycler.setLayoutManager(mLayoutManager1);
                    // popularRecycler.setItemAnimator(new DefaultItemAnimator());

                }
                else if (statusBean.getCode() == 400){
                    progressDialog.dismiss();
                    Toast.makeText(OperatorActivityDataCard.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();
                }



                if (response.code() == 200) {

                    progressDialog.dismiss();







                } else if (response.code() == 401) {
                    progressDialog.dismiss();

                } else if(response.code() ==400) {
                    progressDialog.dismiss();
                    Toast.makeText(OperatorActivityDataCard.this, "respond", Toast.LENGTH_SHORT).show();

                }
                else if(response.code() == 500){
                    progressDialog.dismiss();
                    Toast.makeText(OperatorActivityDataCard.this, "true", Toast.LENGTH_SHORT).show();


                }

            }


            @Override
            public void onFailure(Call<PrepaidResponse> call, Throwable t) {
                progressDialog.dismiss();
                //Log.e(TAG, "failed"+t.getMessage() );
                Toast.makeText(OperatorActivityDataCard.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });






    }
}
