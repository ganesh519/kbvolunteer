package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class CancellationFees extends BaseActivity {

    LinearLayout farebreakup,reschedule1,baggage1;

    ImageView backfare2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancellation_fees);

        farebreakup=findViewById(R.id.farebrakup);
        reschedule1=findViewById(R.id.reschedule1);
        baggage1=findViewById(R.id.baggage1);
        backfare2=findViewById(R.id.backfare2);

        farebreakup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CancellationFees.this,FareBreakup.class);
                startActivity(intent);
            }
        });

        reschedule1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CancellationFees.this,RescheduleFees.class);
                startActivity(intent);
            }
        });

        baggage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CancellationFees.this,Baggage.class);
                startActivity(intent);
            }
        });


        backfare2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CancellationFees.this,PromoAppliedFlights.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
