package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Commissions1 extends BaseActivity {

    ImageView back11;
    RecyclerView recyclerview;
    ApiInterface apiInterface;
    PrefManager prefManager;
    String Id,f_name,surname,email,mobile,walletbals,userid;
    LoginResponseCommision.StatusBean3 statusBean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commissions1);

        back11=findViewById(R.id.back11);
        back11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();

            }
        });


         //final String id = getIntent().getStringExtra("Id");

        prefManager=new PrefManager(getApplicationContext());
        HashMap<String, String> profile=prefManager.getUserDetails();
        Id=profile.get("id");


        final ProgressDialog progressDialog=new ProgressDialog(Commissions1.this);
        progressDialog.setMessage("Loading.........");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponseCommision> call = apiInterface.Statuss11(Id);
        call.enqueue(new Callback<LoginResponseCommision>() {
            @Override
            public void onResponse(Call<LoginResponseCommision> call, Response<LoginResponseCommision> response) {
                if (response.code() == 200) {

                    progressDialog.dismiss();

                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Commissions1.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<LoginResponseCommision.DataBean3> walletdata = response.body().getData();

                    RecyclerView recyclerView = findViewById(R.id.recyclerview);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Commissions1.this);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    RecyclerAdapterWallet3 adapterWallet = new RecyclerAdapterWallet3(getApplicationContext(), walletdata);
                    recyclerView.setAdapter(adapterWallet);

                   // Toast.makeText(Commissions1.this, "Wallet Transaction Data", Toast.LENGTH_SHORT).show();

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(Commissions1.this, "No Data", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<LoginResponseCommision> call, Throwable t) {
                progressDialog.dismiss();
               // Toast.makeText(Commissions1.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(Commissions1.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });



    }
}
