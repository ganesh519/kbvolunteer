package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Activities.Login;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePin1 extends BaseActivity {

    ImageView backn;
    Button save00;
    EditText edittext1,edittext2,edittext0;
    ApiInterface apiInterface;
    PrefManager prefManager;
    String Id;
    TextInputLayout textInputLayout,textInputLayout1,textInputLayout2;
    Typeface typeface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin1);

        backn=findViewById(R.id.backn);
        save00=findViewById(R.id.save1);
        edittext0=findViewById(R.id.edittext0);
        edittext1=findViewById(R.id.edittext1);
        edittext2=findViewById(R.id.edittext2);
        textInputLayout=findViewById(R.id.text_input_layout00);
        textInputLayout1=findViewById(R.id.text_input_layout001);
        textInputLayout2=findViewById(R.id.text_input_layout002);

        backn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

        typeface = Typeface.createFromAsset(getAssets(), "font/Avenir-Light.ttf");
        textInputLayout.setTypeface(typeface);
        textInputLayout2.setTypeface(typeface);
        textInputLayout1.setTypeface(typeface);


        save00.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String password=edittext0.getText().toString();
                String new_password=edittext1.getText().toString();
                String new_password1=edittext2.getText().toString();
                prefManager=new PrefManager(getApplicationContext());
                HashMap<String, String> profile=prefManager.getUserDetails();
                Id=profile.get("id");

                if (!TextUtils.isEmpty(new_password) && !TextUtils.isEmpty(new_password1))
                {
                    if(new_password.equals(new_password1))
                    {

                        final ProgressDialog progressDialog=new ProgressDialog(ChangePin1.this);
                progressDialog.setMessage("Verifying Details.....");
                progressDialog.show();

                apiInterface= ApiClient.getClient().create(ApiInterface.class);
                Call<StatusResponse6> call=apiInterface.Statuss6(Id,password,new_password);
                call.enqueue(new Callback<StatusResponse6>() {
                    @Override
                    public void onResponse(Call<StatusResponse6> call, Response<StatusResponse6> response) {
                        if (response.isSuccessful()) ;

                            StatusResponse6 statusResponse = response.body();
                            StatusDataResponse6 statusDataResponse = statusResponse.status;

                            if (statusDataResponse.code == 200) {
                                progressDialog.dismiss();


                                Data6 data = statusResponse.data;

                                String name = data.firstName;

                               // Toast.makeText(ChangePin1.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ChangePin1.this, Login.class);
                                intent.putExtra("Home",false);
                                prefManager.clearSession();
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                            }

                        else if (statusDataResponse.code != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(ChangePin1.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }


                    @Override
                    public void onFailure(Call<StatusResponse6> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast toast= Toast.makeText(ChangePin1.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();
                        //Toast.makeText(ChangePin1.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
                    }
                    else {
                        Toast.makeText(ChangePin1.this, "Please check the Password", Toast.LENGTH_SHORT).show();
                    }
                }  else {
                    Toast.makeText(ChangePin1.this, "Please Change your PIN", Toast.LENGTH_SHORT).show();

                }
            }


        });
    }

}




