package com.igrand.koinbagvolunterrapp.Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.Data;
import com.igrand.koinbagvolunterrapp.Data7;
import com.igrand.koinbagvolunterrapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerAdapterWallet1 extends RecyclerView.Adapter<RecyclerAdapterWallet1.ViewHolderClass> {

    List<Data7> data14;
    ApiInterface apiInterface;



    Context context;
    // String amount,requesteddate;

    public RecyclerAdapterWallet1(Context context, List<Data7> walletdata) {

        this.context=context;
        this.data14=walletdata;
    }

    @NonNull
    @Override
    public ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(context).inflate(R.layout.listview2,viewGroup,false);
        ViewHolderClass viewHolderClass=new ViewHolderClass(view);
        return viewHolderClass;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderClass viewHolderClass, final int i) {

        Data7 data=data14.get(i);

/*

        SharedPreferences sharedPreferences=context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        String id=sharedPreferences.getString("ID","");
        String request_id=data7.get(i).request_id;
*/

//        viewHolderClass.i200.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final Dialog dialog = new Dialog(context);
//                dialog.setContentView(R.layout.dialogbox1);
//                dialog.show();
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                Window window = dialog.getWindow();
//                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
//
//                final TextView reqdate1,reqrs1,reqdes1;
//                final ImageView image200;
//
//                reqdate1=dialog.findViewById(R.id.reqdate1);
//                reqrs1=dialog.findViewById(R.id.reqrs1);
//                reqdes1=dialog.findViewById(R.id.reqdes1);
//                image200=dialog.findViewById(R.id.image200);
//
//
//                SharedPreferences sharedPreferences=context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//                String id=sharedPreferences.getString("ID","");
//                String request_id=data14.get(i).request_id;
//
//                final ProgressDialog progressDialog1 = new ProgressDialog(context);
//                progressDialog1.setMessage("Loading.....");
//                progressDialog1.show();
//
//                apiInterface = ApiClient.getClient().create(ApiInterface.class);
//                Call<StatusResponse10> call1 = apiInterface.Statuss10(id,request_id);
//                call1.enqueue(new Callback<StatusResponse10>() {
//                    @Override
//                    public void onResponse(Call<StatusResponse10> call, Response<StatusResponse10> response) {
//                        if (response.isSuccessful()) ;
//
//                        StatusResponse10 statusResponse = response.body();
//                        StatusDataResponse10 statusDataResponse = statusResponse.status;
//
//                        if (statusDataResponse.code == 200) {
//                            progressDialog1.dismiss();
//
//                            Data10 data = statusResponse.data;
//                            String amount = data.amount;
//                            String created_at = data.createdAt;
//                            String description = data.description;
//                            String attachments = data.attachments;
//
//
//                            reqdate1.setText(created_at);
//                            reqrs1.setText(amount);
//                            reqdes1.setText(description);
//                            Picasso.get().load("http://igranddeveloper.live/kbig/" + attachments).into(image200);
//                            Toast.makeText(context, statusDataResponse.message, Toast.LENGTH_SHORT).show();
//                            //    Intent intent = new Intent(NavigationDrawerDashboard.this, PersonalDetails.class);
//
//                        } else if (statusDataResponse.code == 409) {
//                            progressDialog1.dismiss();
//                            Toast.makeText(context, statusDataResponse.message, Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<StatusResponse10> call, Throwable t) {
//                        progressDialog1.dismiss();
//                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
//
//                    }
//                });
//
//
//                ImageView close=(ImageView) dialog.findViewById(R.id.close);
//
//                close.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//
//                    }
//                });
//
//            }
//        });

    }


    @Override
    public int getItemCount() {
        return data14.size();
    }

    public static class ViewHolderClass extends RecyclerView.ViewHolder {

        private final Context context;
        TextView requesteddate,amount;
        ImageView i200;

        public ViewHolderClass(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            i200=itemView.findViewById(R.id.image);

        }
    }
}


