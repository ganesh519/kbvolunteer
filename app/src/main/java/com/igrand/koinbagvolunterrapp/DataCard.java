package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.APIClient1;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataCard extends BaseActivity {

    private static final int RQS_PICK_CONTACT = 1;
    ImageView backrecharge;
    ImageView phonebook;
    EditText mobilenumber;
    RadioButton prepaid, postpaid;
    PrepaidResponse.StatusBean statusBean;
    ApiInterface apiInterface;
    RadioGroup rg;
    String type,operator_name,operator_type;
    private static final int REQUEST_CODE = 1;
    private String TAG="mobile";
    List<String> oname = new ArrayList<String>();
    List<String> otype = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_card);



        backrecharge = findViewById(R.id.backrecharge);
        phonebook = findViewById(R.id.phonebook);
        mobilenumber = findViewById(R.id.mobilenumber);
        rg = (RadioGroup) findViewById(R.id.rg);
        prepaid = findViewById(R.id.prepaid);
        postpaid = findViewById(R.id.postpaid);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.prepaid){

                   type="prepaid-mobile";
                    //getprepaid(type);



                }
                else {
                    type="postpaid-mobile";
                    //getpostpaid(type);


                }
            }
        });








        mobilenumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String value = String.valueOf(s.length());


                if (value.equals("10")) {

                    if (type == null) {
                        Toast.makeText(DataCard.this, "Please Select Prepaid or Postpaid..", Toast.LENGTH_LONG).show();

                    }

                    else {

                        final String MOBILENUMBER = mobilenumber.getText().toString();
                        String PASSWORD = "ilk328hd843f4r5";
                        String APIID = "AP135358";
                        String FORMAT = "json";


                        final ProgressDialog progressDialog=new ProgressDialog(DataCard.this);
                        progressDialog.setMessage("Loading...");
                        progressDialog.show();
                        TextView text = (TextView) progressDialog.findViewById(R.id.text);
                        ImageView image = (ImageView) progressDialog.findViewById(R.id.image);


                        apiInterface = APIClient1.getClient().create(ApiInterface.class);
                        Call<OperatorFetch> call = apiInterface.operator(APIID, PASSWORD, MOBILENUMBER, FORMAT);
                        call.enqueue(new Callback<OperatorFetch>() {
                            @Override
                            public void onResponse(Call<OperatorFetch> call, Response<OperatorFetch> response) {

                                if (response.isSuccessful()) ;
                                OperatorFetch operatorFetch = response.body();

                                if (response.code() == 200) {
                                    progressDialog.dismiss();

                                    String operatorcode = operatorFetch.operatorCode;
                                    String circlecode = operatorFetch.circleCode;

                                    Toast.makeText(DataCard.this, "Success..", Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(DataCard.this, ProceedToRechargeDataCard.class);
                                    intent.putExtra("Operator", operatorcode);
                                    intent.putExtra("Circle", circlecode);
                                    intent.putExtra("Mobile", MOBILENUMBER);
                                    intent.putExtra("Type", type);
                                    startActivity(intent);


                                /*if (type.equals("") || type.equals(null)){
                                    Toast.makeText(MobileRechargeActivity.this, "Please Select prepaid or postpaid..", Toast.LENGTH_SHORT).show();
                                }
                                else {



                                }
*/


                                } else if (response.code() != 200) {
                                    progressDialog.dismiss();
                                    //Toast.makeText(MobileRechargeActivity.this, "Volunteer Not Exist ,Please Contact Koinbag Ngo..", Toast.LENGTH_SHORT).show();
                                    finish();

                                }

                            }


                            @Override
                            public void onFailure(Call<OperatorFetch> call, Throwable t) {
                                progressDialog.dismiss();
                                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                finish();
                                Toast toast = Toast.makeText(DataCard.this,
                                        t.getMessage(), Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                toast.show();


                            }
                        });

                    }



                }


            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        backrecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        phonebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent=new Intent(MobileRechargeActivity.this,ContactsActivity.class);
                startActivity(intent);*/


//                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//                startActivityForResult(intent, 1);

                Uri uri = Uri.parse("content://contacts");
                Intent intent = new Intent(Intent.ACTION_PICK, uri);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                String[] projection = { ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME };

                Cursor cursor = getContentResolver().query(uri, projection,
                        null, null, null);
                cursor.moveToFirst();

                int numberColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(numberColumnIndex);

                String contactNumber = number.replace("+91", "");


                Log.e(TAG, "mobile"+contactNumber );

                mobilenumber.setText(contactNumber);



                int nameColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                String name = cursor.getString(nameColumnIndex);


            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    }

