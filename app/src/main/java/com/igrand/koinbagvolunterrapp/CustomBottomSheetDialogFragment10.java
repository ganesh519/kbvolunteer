package com.igrand.koinbagvolunterrapp;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.igrand.koinbagvolunterrapp.Adapters.MyAdapter7;

public class CustomBottomSheetDialogFragment10 extends BottomSheetDialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sort_filter, container, false);
        TabLayout tabLayout6 = (TabLayout)v.findViewById(R.id.tabLayout6);
        final ViewPager viewPager6 = (ViewPager)v.findViewById(R.id.viewPager6);

        ImageView exit18;

        exit18=(ImageView)v.findViewById(R.id.exit18);

        exit18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tabLayout6.addTab(tabLayout6.newTab().setText("SORT Cheapest"));
        tabLayout6.addTab(tabLayout6.newTab().setText("FILTERS"));
        tabLayout6.setTabGravity(TabLayout.GRAVITY_FILL);

        final MyAdapter7 adapter = new MyAdapter7(this,getChildFragmentManager(), tabLayout6.getTabCount());


        viewPager6.setAdapter(adapter);
        viewPager6.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout6));

        tabLayout6.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager6.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return v;
    }


}
