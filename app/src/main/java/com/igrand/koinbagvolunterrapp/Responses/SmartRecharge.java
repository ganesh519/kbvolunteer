package com.igrand.koinbagvolunterrapp.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SmartRecharge {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("operator_id")
    @Expose
    private String operatorId;
    @SerializedName("circle_id")
    @Expose
    private String circleId;
    @SerializedName("recharge_amount")
    @Expose
    private String rechargeAmount;
    @SerializedName("recharge_talktime")
    @Expose
    private String rechargeTalktime;
    @SerializedName("recharge_validity")
    @Expose
    private String rechargeValidity;
    @SerializedName("recharge_short_desc")
    @Expose
    private String rechargeShortDesc;
    @SerializedName("recharge_long_desc")
    @Expose
    private String rechargeLongDesc;
    @SerializedName("recharge_type")
    @Expose
    private String rechargeType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getCircleId() {
        return circleId;
    }

    public void setCircleId(String circleId) {
        this.circleId = circleId;
    }

    public String getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getRechargeTalktime() {
        return rechargeTalktime;
    }

    public void setRechargeTalktime(String rechargeTalktime) {
        this.rechargeTalktime = rechargeTalktime;
    }

    public String getRechargeValidity() {
        return rechargeValidity;
    }

    public void setRechargeValidity(String rechargeValidity) {
        this.rechargeValidity = rechargeValidity;
    }

    public String getRechargeShortDesc() {
        return rechargeShortDesc;
    }

    public void setRechargeShortDesc(String rechargeShortDesc) {
        this.rechargeShortDesc = rechargeShortDesc;
    }

    public String getRechargeLongDesc() {
        return rechargeLongDesc;
    }

    public void setRechargeLongDesc(String rechargeLongDesc) {
        this.rechargeLongDesc = rechargeLongDesc;
    }

    public String getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(String rechargeType) {
        this.rechargeType = rechargeType;
    }
}
