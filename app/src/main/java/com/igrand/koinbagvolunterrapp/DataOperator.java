package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class DataOperator {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("operator_name")
    @Expose
    public String operatorName;
    @SerializedName("operator_code")
    @Expose
    public String operatorCode;
    @SerializedName("operator_type")
    @Expose
    public String operatorType;
    @SerializedName("image")
    @Expose
    public String image;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("operatorName", operatorName).append("operatorCode", operatorCode).append("operatorType", operatorType).append("image", image).toString();
    }
}
