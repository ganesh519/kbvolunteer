package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.APIClient1;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.igrand.koinbagvolunterrapp.Activities.AppController.TAG;

public class SpecialRecharge extends Fragment {

    ApiInterface apiInterface;
    BrowseRecyclerAdapter1 allRecyclerAdapter1;
    ArrayList<TopupResponse> topupResponseArrayList=new ArrayList<>();



    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_topup, container, false);


        final RecyclerView recyclerView=v.findViewById(R.id.recycler_browse);

        SharedPreferences sharedPreferences=getActivity().getSharedPreferences("Data", Context.MODE_PRIVATE);
        String Operator_Code=sharedPreferences.getString("Operator","'");
        String Circle_Code=sharedPreferences.getString("Circle","'");
        final String MobileNumber=sharedPreferences.getString("Mobile","'");
        String APIID = "AP135358";
        String PASSWORD = "ki832h74td3ff";
        String PageID = "0";
        String FORMAT = "JSON/XML";

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading.....");
        progressDialog.show();


        apiInterface = APIClient1.getClient().create(ApiInterface.class);
        Call<BrowseFetch> call = apiInterface.browseplans(APIID, PASSWORD, Operator_Code, Circle_Code, MobileNumber, PageID, FORMAT);
        call.enqueue(new Callback<BrowseFetch>() {
            @Override
            public void onResponse(Call<BrowseFetch> call, Response<BrowseFetch> response) {

                if (response.isSuccessful()) ;
                BrowseFetch operatorFetch = response.body();

                if (response.code() == 200) {
                    progressDialog.dismiss();

                    List<PlanDescription> planDescriptions = operatorFetch.planDescription;


                    if(planDescriptions!=null) {

                        for (int i=0; i< planDescriptions.size();i++) {

                            if(planDescriptions.get(i).rechargeType.equals("Other"))


                            {

                                Log.e(TAG, "onResponse:"+planDescriptions.get(i).id );

                                topupResponseArrayList.add(new TopupResponse(planDescriptions.get(i).rechargeShortDesc,planDescriptions.get(i).rechargeAmount,
                                        planDescriptions.get(i).rechargeTalktime,planDescriptions.get(i).rechargeValidity,planDescriptions.get(i).rechargeLongDesc));

                                allRecyclerAdapter1 = new BrowseRecyclerAdapter1(getContext(), topupResponseArrayList, MobileNumber);

                            } else {
                                Toast.makeText(getContext(), "No TopUp's...", Toast.LENGTH_SHORT).show();
                            }

                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(allRecyclerAdapter1);
                        Toast.makeText(getContext(), "Success..", Toast.LENGTH_SHORT).show();

                    } else
                    {
                        Toast.makeText(getContext(), "No Data Found...", Toast.LENGTH_SHORT).show();
                    }





                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    //Toast.makeText(MobileRechargeActivity.this, "Volunteer Not Exist ,Please Contact Koinbag Ngo..", Toast.LENGTH_SHORT).show();

                }

            }



            @Override
            public void onFailure(Call<BrowseFetch> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(getContext(),
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


        return v;
    }
}

