package com.igrand.koinbagvolunterrapp;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.Adapters.MyAdapter8;

public class SelectSeat extends BaseActivity {

    TabLayout tabLayout7;
    ViewPager viewPager7;
    ImageView seat,backblack12;
    PopupWindow popupWindow;
    LinearLayout linearLayout1,linearlayout0,linear0;
    TextView busmorning,acgold;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_seat);

        tabLayout7 = (TabLayout) findViewById(R.id.tabLayout7);
        viewPager7 = (ViewPager) findViewById(R.id.viewPager7);
        backblack12=findViewById(R.id.backblack12);
        busmorning=findViewById(R.id.busmorning);
        acgold=findViewById(R.id.acgold);

        typeface=Typeface.createFromAsset(getAssets(),"font/Avenir-Medium.ttf");
        busmorning.setTypeface(typeface);
        acgold.setTypeface(typeface);
        tabLayout7.addTab(tabLayout7.newTab().setText("Select seat"));
        tabLayout7.addTab(tabLayout7.newTab().setText("Reviews"));
        tabLayout7.addTab(tabLayout7.newTab().setText("Cancellation Policy"));
        tabLayout7.addTab(tabLayout7.newTab().setText("Boarding & Drop Point"));
        tabLayout7.setTabGravity(TabLayout.GRAVITY_FILL);

        final MyAdapter8 adapter = new MyAdapter8(this,getSupportFragmentManager(), tabLayout7.getTabCount());

        backblack12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SelectSeat.this,BusDetails.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
        viewPager7.setAdapter(adapter);
        viewPager7.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout7));

        tabLayout7.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager7.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        seat=findViewById(R.id.seat);
        linearLayout1 = (LinearLayout) findViewById(R.id.linear1);
        linear0=findViewById(R.id.linear0);
        seat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater layoutInflater = (LayoutInflater) SelectSeat.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = layoutInflater.inflate(R.layout.popup,null);

                popupWindow = new PopupWindow(customView, ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
                popupWindow.setOutsideTouchable(true);

                //display the popup window
                popupWindow.showAtLocation(linearLayout1, Gravity.TOP, 0, 300);

                dimBehind();

            }

            private void dimBehind() {

                View container;
                if (popupWindow.getBackground() == null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        container = (View) popupWindow.getContentView().getParent();
                    } else {
                        container = popupWindow.getContentView();
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        container = (View) popupWindow.getContentView().getParent().getParent();
                    } else {
                        container = (View) popupWindow.getContentView().getParent();
                    }
                }
                Context context = popupWindow.getContentView().getContext();
                WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
                p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                p.dimAmount = 0.7f;
                wm.updateViewLayout(container, p);



            }

        });

        linear0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

      /*  popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setTouchInterceptor(new View.OnTouchListener()
        { @Override public boolean onTouch(View v, MotionEvent event) {
            if (AppContext.isDebugMode()) Log.d("POPUP_WINDOW", "v: "+v.getTag() + " | event: "+event.getAction());
            popupWindow.dismiss();
            return true; } });
*/
    }
}
