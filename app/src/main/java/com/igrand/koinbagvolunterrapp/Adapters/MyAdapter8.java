package com.igrand.koinbagvolunterrapp.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.igrand.koinbagvolunterrapp.BoardingFragment;
import com.igrand.koinbagvolunterrapp.CancellationPolcyFragment;
import com.igrand.koinbagvolunterrapp.ReviewsFragment;
import com.igrand.koinbagvolunterrapp.SelectSeatFragment;

public class MyAdapter8 extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;
    public MyAdapter8(Context context, FragmentManager supportFragmentManager, int tabCount) {

        super(supportFragmentManager);
        myContext = context;
        this.totalTabs = tabCount;
    }

    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                SelectSeatFragment selectSeatFragment = new SelectSeatFragment();
                return selectSeatFragment;
            case 1:
                ReviewsFragment reviewsFragment = new ReviewsFragment();
                return reviewsFragment;
            case 2:
                CancellationPolcyFragment cancellationPolcyFragment = new CancellationPolcyFragment();
                return cancellationPolcyFragment;

            case 3:
                BoardingFragment boardingFragment = new BoardingFragment();
                return boardingFragment;

            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;

    }
}

