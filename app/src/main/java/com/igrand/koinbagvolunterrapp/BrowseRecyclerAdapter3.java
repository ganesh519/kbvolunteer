package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class BrowseRecyclerAdapter3 extends RecyclerView.Adapter<BrowseRecyclerAdapter3.Holder> {

    List<PlanDescription> planDescriptions;
    Context context;
    String MobileNumber;

    public BrowseRecyclerAdapter3(Context context, List<PlanDescription> planDescriptions, String mobileNumber) {

        this.planDescriptions=planDescriptions;
        this.context=context;
        this.MobileNumber=mobileNumber;
    }

    @NonNull
    @Override
    public BrowseRecyclerAdapter3.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.browseplan, viewGroup, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BrowseRecyclerAdapter3.Holder holder, int i) {
        String rechargetype=planDescriptions.get(i).rechargeType;


        if(rechargetype.equals("2G")|| rechargetype.equals("3G") || rechargetype.equals("4G")) {

            holder.desclong.setText(planDescriptions.get(i).rechargeLongDesc);
            holder.rechargeamount.setText(planDescriptions.get(i).rechargeAmount);



        }
        final String amount=planDescriptions.get(i).rechargeAmount;
        final String talktime=planDescriptions.get(i).rechargeTalktime;
        final String validity=planDescriptions.get(i).rechargeValidity;
        final String longdes=planDescriptions.get(i).rechargeLongDesc;

        holder.listlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SharedPreferences sharedPreferences=context.getSharedPreferences("BrowseData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Amount",amount);
                editor.putString("TalkTime",talktime);
                editor.putString("Validity",validity);
                editor.putString("Long",longdes);
                editor.putString("Mobile",MobileNumber);
                editor.apply();


                Fragment planDetailsFragment=new PlanDetailsFragment();
                FragmentTransaction fragmentTransaction =((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                //fragmentTransaction.add(R.id.container4, planDetailsFragment);
                //fragmentTransaction.addToBackStack(null);

                BottomSheetDialog dialog = new BottomSheetDialog(context);
                dialog.setContentView(R.layout.fragment_plan_details);
                //dialog.show();
                new CustomBottomSheetDialogFragment1().show(((AppCompatActivity)context).getSupportFragmentManager(), "Dialog");
                dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);

                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return planDescriptions.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        LinearLayout listlayout;
        TextView desclong,rechargeamount;
        public Holder(@NonNull View itemView) {
            super(itemView);
            listlayout=itemView.findViewById(R.id.listlayout);
            desclong=itemView.findViewById(R.id.desclong);
            rechargeamount=itemView.findViewById(R.id.rechargeamount);
        }
    }
}
