package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class DataCircle {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("circle_name")
    @Expose
    public String circleName;
    @SerializedName("circle_code")
    @Expose
    public String circleCode;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("circleName", circleName).append("circleCode", circleCode).toString();
    }

}
