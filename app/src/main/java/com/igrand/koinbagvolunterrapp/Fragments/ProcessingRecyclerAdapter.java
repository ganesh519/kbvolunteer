package com.igrand.koinbagvolunterrapp.Fragments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.ProcessRecyclerAdapter;
import com.igrand.koinbagvolunterrapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProcessingRecyclerAdapter  extends RecyclerView.Adapter<ProcessingRecyclerAdapter.Holder> {

    List<Pending> pendings;
    Context context;
    ApiInterface apiInterface;
    PrefManager prefManager;
    String id;
    StatusDataResponse15 statusDataResponse7;
    WalletResponse2 walletResponse1;
    public ProcessingRecyclerAdapter(Context context, List<Pending> pendings, StatusDataResponse15 statusDataResponse7, WalletResponse2 walletResponse1) {

        this.context=context;
        this.pendings=pendings;
        this.statusDataResponse7=statusDataResponse7;
        this.walletResponse1=walletResponse1;
    }



    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview8, viewGroup, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        String upperString = pendings.get(i).apiType.substring(0,1).toUpperCase() + pendings.get(i).apiType.substring(1);

        holder.api_type.setText(upperString);
        holder.digitalnumber.setText(pendings.get(i).digitalNumber);
        holder.prevbal.setText(pendings.get(i).prevBal);
        holder.curbal.setText(pendings.get(i).curBal);
        holder.createdat.setText(pendings.get(i).createdAt);
        holder.amount.setText(pendings.get(i).amount);
        Picasso.get().load(pendings.get(i).operatorimage).error(R.drawable.kb).into(holder.jio);
        //holder.scomrs1.setText(pendings.get(i).commission);

    }

    @Override
    public int getItemCount() {

   return pendings.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView api_type,digitalnumber,prevbal,curbal,createdat,status,amount,scomrs1;
        ImageView image,jio;
        public Holder(@NonNull View itemView) {
            super(itemView);

            api_type=itemView.findViewById(R.id.papi_type11);
            digitalnumber=itemView.findViewById(R.id.pdigitalnumber11);
            prevbal=itemView.findViewById(R.id.pprevbal11);
            curbal=itemView.findViewById(R.id.pcurbal11);
            createdat=itemView.findViewById(R.id.pcreated_at11);
            amount=itemView.findViewById(R.id.pdrs11);
            jio=itemView.findViewById(R.id.jio);
           // scomrs1=itemView.findViewById(R.id.scomrs111);


        }
    }
}
