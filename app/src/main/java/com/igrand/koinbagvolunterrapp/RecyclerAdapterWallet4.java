package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class RecyclerAdapterWallet4 extends RecyclerView.Adapter<RecyclerAdapterWallet4.ViewHolderClass> {


    Context context;

    List<Data12> data11;


    public RecyclerAdapterWallet4(Context commissions1, List<Data12> id) {

        this.context=commissions1;
        this.data11= (List<Data12>) id;
    }



    @NonNull
    @Override
    public RecyclerAdapterWallet4.ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view= LayoutInflater.from(context).inflate(R.layout.recycler_list2,viewGroup,false);
        ViewHolderClass viewHolderClass=new ViewHolderClass(view);
        return viewHolderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterWallet4.ViewHolderClass viewHolderClass, int i) {


        viewHolderClass.message1.setText(data11.get(i).message);
        viewHolderClass.description1.setText(data11.get(i).description);
        viewHolderClass.createdat1.setText(data11.get(i).createdAt);


      // Activity act = (Activity) mContext; act.getPreferences(int); and i


        SharedPreferences.Editor editor = context.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE).edit();
        editor.putInt("YourKeyNameHere", data11.size());
        editor.apply();

    }


    @Override
    public int getItemCount() {
        return data11.size();
    }

    public static class ViewHolderClass extends RecyclerView.ViewHolder {

        private final Context context;
        TextView message1,description1,createdat1;

        public ViewHolderClass(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            message1=itemView.findViewById(R.id.message1);
            description1=itemView.findViewById(R.id.description101);
            createdat1=itemView.findViewById(R.id.createdat1);

        }
    }
}
