package com.igrand.koinbagvolunterrapp;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletRequest1 extends BaseActivity {

    Button submit,ok;
    LinearLayout upload;
    public static final int REQUEST_CODE = 1;
    EditText amount1,description1;
    ApiInterface apiInterface;
    ImageView back1001,imageupload;
    PrefManager prefManager;
    String Ngo_Name,attachments;
    Bitmap bitmap;
    TextView ngoname1;

    private static int RESULT_LOAD_IMAGE = 1;
    File file=null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_request1);


        final SharedPreferences sharedPreferences = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        final String id = sharedPreferences.getString("ID", "");


        amount1 = findViewById(R.id.amount1);
        description1 = findViewById(R.id.description1);
        submit = findViewById(R.id.submit1);
        back1001 = findViewById(R.id.back1001);
        imageupload = findViewById(R.id.imageupload);
        ngoname1 = findViewById(R.id.ngoname1);



        back1001.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               finish();
            }
        });


        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        Ngo_Name = profile.get("NgoName");
        ngoname1.setText(Ngo_Name);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String amount = amount1.getText().toString();
                String description = description1.getText().toString();

                if (amount.equals("")){
                    Toast.makeText(WalletRequest1.this, "Enter amount..", Toast.LENGTH_SHORT).show(); }
                else {

                    getData(id,amount,description);
                }

            }

        });


        upload = findViewById(R.id.upload1);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                        "content://media/internal/images/media"));
                startActivity(intent);*/


                Intent i = new Intent(
                        Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }

        });

    }

    private void getData(final String amount, String description,String id) {



        RequestBody Amount = RequestBody.create(MediaType.parse("text/plain"), amount);
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody iD = RequestBody.create(MediaType.parse("text/plain"), id);


        MultipartBody.Part body = null;
        if (file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        }


        final ProgressDialog progressDialog = new ProgressDialog(WalletRequest1.this);
        progressDialog.setMessage("Verifying Details.....");
        progressDialog.show();

      /*  apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusResponse8> call = apiInterface.Statuss8(id, amount, description);
        call.enqueue(new Callback<StatusResponse8>() {
            @Override
            public void onResponse(Call<StatusResponse8> call, Response<StatusResponse8> response) {
                if (response.isSuccessful()) ;

                StatusResponse8 statusResponse = response.body();
                StatusDataResponse8 statusDataResponse = statusResponse.status;

                if (statusDataResponse.code == 200) {
                    progressDialog.dismiss();


                    Data8 data = statusResponse.data;

                    // String name=data.firstName;

                    Toast.makeText(WalletRequest1.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();

                    final Dialog dialog = new Dialog(WalletRequest1.this);
                    dialog.setContentView(R.layout.dialogbox);
                    dialog.show();

                    TextView walletrs, walletrs1;
                    walletrs = dialog.findViewById(R.id.walletrs);
                    walletrs1 = dialog.findViewById(R.id.walletrs1);
                    walletrs1.setText(amount);
                    walletrs.setText(amount);



                *//*InsetDrawable inset = new InsetDrawable(,20);
                dialog.getWindow().setBackgroundDrawable(inset);*//*
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    Button ok = (Button) dialog.findViewById(R.id.ok);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(WalletRequest1.this, WalletTransactions1.class);
                            startActivity(intent);

                            // dialog.dismiss();
                        }
                    });
                } else if (statusDataResponse.code == 409) {
                    progressDialog.dismiss();
                    Toast.makeText(WalletRequest1.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<StatusResponse8> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(WalletRequest1.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });*/


    }

    @Override
        protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                imageupload.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            }


        }
}
