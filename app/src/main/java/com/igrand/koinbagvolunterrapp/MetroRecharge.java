package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MetroRecharge extends BaseActivity {

    Button cardbalance;
    ImageView backblack17;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metro_recharge);

        cardbalance=findViewById(R.id.cardbalance);
        backblack17=findViewById(R.id.backblack17);
        cardbalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MetroRecharge.this,MetroRechageDetails.class);
                startActivity(intent);
            }
        });
        backblack17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MetroRecharge.this,Flight.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
