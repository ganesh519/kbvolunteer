package com.igrand.koinbagvolunterrapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

@SuppressLint("ValidFragment")
public class BestOffersFragment extends BottomSheetDialogFragment {
    public BestOffersFragment() {

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_bestoffers, container, false);

        LinearLayout listlayout=(LinearLayout)v.findViewById(R.id.listlayout);
        listlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                     /*Intent intent=new Intent(getActivity().getApplicationContext(),PlanDetails.class);
                     startActivity(intent);*/

                Fragment planDetailsFragment=new PlanDetailsFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                //fragmentTransaction.add(R.id.container4, planDetailsFragment);
                //fragmentTransaction.addToBackStack(null);

                BottomSheetDialog dialog = new BottomSheetDialog(getContext());
                dialog.setContentView(R.layout.fragment_plan_details);
                //dialog.show();
                new CustomBottomSheetDialogFragment1().show(getActivity().getSupportFragmentManager(), "Dialog");
                dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);

                fragmentTransaction.commit();
            }
        });

        return v;
    }
}

