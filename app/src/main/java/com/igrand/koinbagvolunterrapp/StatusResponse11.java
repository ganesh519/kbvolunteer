package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class StatusResponse11 {


    @SerializedName("status")
    @Expose
    public StatusDataResponse11 status;
    @SerializedName("data")
    @Expose
    public List<Data11> data = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }

}
