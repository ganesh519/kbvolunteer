package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Activities.RechargePaymentMobileResponse;
import com.igrand.koinbagvolunterrapp.Client.APIClient3;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class Paymentelectricity extends BaseActivity {

    private static final String TAG = "payment";
    Button payment;
    ImageView backbutton;
    String amount,operator,circle_code,recharge_type,mobile_number;
    String circle_code1="1";
    TextView finalamount,walbal;
    PrefManager prefManager;
    String id,walletbals,req_id,refno,token,consumer_name,consumer_mobile;
    ApiInterface apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paymentelectricity);

        payment=findViewById(R.id.payment);
        backbutton=findViewById(R.id.back);
        finalamount=findViewById(R.id.finalamount);
        walbal=findViewById(R.id.walbal);

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        prefManager=new PrefManager(getApplicationContext());
        HashMap<String, String> profile=prefManager.getUserDetails();
        id=profile.get("id");
        walletbals=profile.get("Walletbals");

        walbal.setText(walletbals);
        if (getIntent() !=null){
            amount=getIntent().getStringExtra("Amount");
            operator=getIntent().getStringExtra("Operator");
            circle_code=getIntent().getStringExtra("CircleCode");
            recharge_type=getIntent().getStringExtra("RechargeType");
            mobile_number=getIntent().getStringExtra("MobileNumber");


            req_id=getIntent().getStringExtra("req_id");
            refno=getIntent().getStringExtra("refno");
            token=getIntent().getStringExtra("token");

            consumer_name = getIntent().getStringExtra("consumer_name");
            consumer_mobile = getIntent().getStringExtra("consumer_mobile");


        }
        finalamount.setText(amount);


        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String amount=finalamount.getText().toString().trim();
                if(amount.equals("")||amount.equals(null)) {

                    Toast.makeText(Paymentelectricity.this, "Please enter amount...", Toast.LENGTH_SHORT).show();


                } else {

                    getRecharge(id,amount,req_id,refno,consumer_name,consumer_mobile,operator);
                }

            }
        });


    }

    private void getRecharge(String id, String amount, String req_id, String refno, String consumer_name, String consumer_mobile,String operator) {




        final ProgressDialog progressDialog=new ProgressDialog(Paymentelectricity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();


        apiInterface = APIClient3.getClient().create(ApiInterface.class);
        Call<ElectricityPayment> call = apiInterface.electrictypaymentdetails(id,amount,req_id,refno,consumer_name,consumer_mobile,operator);
        call.enqueue(new Callback<ElectricityPayment>() {
            @Override
            public void onResponse(Call<ElectricityPayment> call, Response<ElectricityPayment> response) {

                if (response.isSuccessful()) ;
                ElectricityPayment operatorFetch = response.body();



                if (response.code() == 200) {
                    progressDialog.dismiss();
                    Toast.makeText(Paymentelectricity.this, "Payment Success", Toast.LENGTH_LONG).show();

                    Intent intent=new Intent(Paymentelectricity.this,NavigationDrawerDashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);




                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(Paymentelectricity.this, "Failed. Refund Complete,Please Try Again", Toast.LENGTH_LONG).show();
                    Intent intent=new Intent(Paymentelectricity.this,NavigationDrawerDashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }


            }

            @Override
            public void onFailure(Call<ElectricityPayment> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
                Toast toast = Toast.makeText(Paymentelectricity.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }


        });


    }


}
