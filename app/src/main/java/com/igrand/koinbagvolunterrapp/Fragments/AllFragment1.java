package com.igrand.koinbagvolunterrapp.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.igrand.koinbagvolunterrapp.Adapters.ApprovedRecyclerAdapter;
import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.R;
import com.igrand.koinbagvolunterrapp.StatusDataResponse7;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

@SuppressLint("ValidFragment")
public class AllFragment1 extends Fragment {
    ApiInterface apiInterface;
    RecyclerView recycler_all;
    PrefManager prefManager;
    TextView textview0;
    String Id;
    AllRecyclerAdapter allRecyclerAdapter;
    ApproveRecyclerAdapter approveRecyclerAdapter;
    ProcessingRecyclerAdapter processingRecyclerAdapter;
    FailureRecyclerAdapter failureRecyclerAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_allone, container, false);

        recycler_all=v.findViewById(R.id.recycler_all);
        textview0=v.findViewById(R.id.textview0);


        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");


        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletResponse2> call = apiInterface.WalletResponse1(Id);
        call.enqueue(new Callback<WalletResponse2>() {
            @Override
            public void onResponse(Call<WalletResponse2> call, Response<WalletResponse2> response) {
                if (response.isSuccessful());

                    WalletResponse2 walletResponse1 = response.body();
                    if (walletResponse1!=null) {


                        StatusDataResponse15 statusDataResponse7 = walletResponse1.status;
                        if (statusDataResponse7.code == 200) {
                            progressDialog.dismiss();

                            // List<Data15> data15 = walletResponse1.data;

                            Data15 data15 = walletResponse1.data;

                            if (data15.all != null) {


                                List<All> all = data15.all;

                                List<Success> approvedList = data15.success;

                                List<Pending> proceesingList = data15.pending;

                                List<Failure> rejectedList = data15.failed;


                                recycler_all.setLayoutManager(new LinearLayoutManager(getActivity()));
                                allRecyclerAdapter = new AllRecyclerAdapter(getContext(), all, textview0);
                                recycler_all.setAdapter(allRecyclerAdapter);


                            } else {
                                Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                            }
                        } else if (statusDataResponse7.code != 200) {

                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Internal Server Error", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                    }

            }

            @Override
            public void onFailure(Call<WalletResponse2> call, Throwable t) {
                progressDialog.dismiss();
               // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(getActivity(), t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });





        return v;
    }
}
