package com.igrand.koinbagvolunterrapp;

public class LoginResponse1 {


    /**
     * status : {"code":200,"message":"Login Successfully"}
     * data : {"id":26,"first_name":"Chaitanya","surname":"S","mobile_number":"7286882452","user_id":"KBV26","created_id":"23","ngo_name":"Ramalingesh","ngo_id":23,"photo":"http://igranddeveloper.live/kbig/uploads/download.jpg","wallet_bal":"23945.03"}
     */

    public StatusBean status;
    public DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Login Successfully
         */

        public int code;
        public String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 26
         * first_name : Chaitanya
         * surname : S
         * mobile_number : 7286882452
         * user_id : KBV26
         * created_id : 23
         * ngo_name : Ramalingesh
         * ngo_id : 23
         * photo : http://igranddeveloper.live/kbig/uploads/download.jpg
         * wallet_bal : 23945.03
         */

        public int id;
        public String first_name;
        public String surname;
        public String mobile_number;
        public String user_id;
        public String created_id;
        public String ngo_name;
        public String ngo_id;
        public String photo;
        public String wallet_bal;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getMobile_number() {
            return mobile_number;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getCreated_id() {
            return created_id;
        }

        public void setCreated_id(String created_id) {
            this.created_id = created_id;
        }

        public String getNgo_name() {
            return ngo_name;
        }

        public void setNgo_name(String ngo_name) {
            this.ngo_name = ngo_name;
        }

        public String getNgo_id() {
            return ngo_id;
        }

        public void setNgo_id(String ngo_id) {
            this.ngo_id = ngo_id;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getWallet_bal() {
            return wallet_bal;
        }

        public void setWallet_bal(String wallet_bal) {
            this.wallet_bal = wallet_bal;
        }
    }
}