package com.igrand.koinbagvolunterrapp;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.igrand.koinbagvolunterrapp.Activities.CustomDialogActivity;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomDialogActivity2 extends BaseActivity {

    LoginResponse2.StatusBean2 statusBean1;
    ApiInterface apiInterface;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);

        prefManager=new PrefManager(CustomDialogActivity2.this);

        final String mobile_number = getIntent().getStringExtra("Mobile_number");
        final String name1 = getIntent().getStringExtra("Name1");
        final String surname = getIntent().getStringExtra("SurName1");
        final Dialog progressDialog = new Dialog(CustomDialogActivity2.this, R.style.Theme_AppCompat_Light_NoActionBar);
        progressDialog.setContentView(R.layout.custom_dialog);
        Window window = progressDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        LottieAnimationView animationView = progressDialog.findViewById(R.id.lottie_view);
        animationView.setAnimation("refresh.json");
        animationView.playAnimation();
        animationView.loop(true);
        progressDialog.show();
        TextView text = (TextView) progressDialog.findViewById(R.id.text);
        ImageView image = (ImageView) progressDialog.findViewById(R.id.image);


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponse2> call = apiInterface.Statuss12(mobile_number);
        call.enqueue(new Callback<LoginResponse2>() {
            @Override
            public void onResponse(Call<LoginResponse2> call, Response<LoginResponse2> response) {



                if (response.code() == 200) {

                    progressDialog.dismiss();
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    LoginResponse2.DataBean2 dataBean = response.body().getData();
                    //Integer otp = dataBean.otp;
                    // Toast.makeText(WelcomeBack.this, statusBean1.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(CustomDialogActivity2.this, OTP1.class);
                    intent.putExtra("Mobile_number", mobile_number);
                    intent.putExtra("Name1",name1);
                    intent.putExtra("SurName1",surname);
                    //intent.putExtra("OTP", otp);
                    startActivity(intent);

                }else if (response.code() != 200) {
                    progressDialog.dismiss();
                   // Toast.makeText(CustomDialogActivity2.this, "Please Check the OTP", Toast.LENGTH_SHORT).show();
                    Toast toast= Toast.makeText(CustomDialogActivity2.this,
                            "Please Check the OTP", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();
                    finish();

                }

            }


            @Override
            public void onFailure(Call<LoginResponse2> call, Throwable t) {
                progressDialog.dismiss();
               // Toast.makeText(CustomDialogActivity2.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                finish();
                Toast toast= Toast.makeText(CustomDialogActivity2.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });
    }
}
