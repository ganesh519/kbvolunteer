package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectOperatorBroadBand extends BaseActivity{

    RecyclerView recycler_landline;
    ApiInterface apiInterface;
    BroadBandRecyclerAdapter landLineecyclerAdapter;
    ImageView back4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_operator_landline);

        recycler_landline=findViewById(R.id.recycler_landline);
        back4=findViewById(R.id.back4);

        back4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final ProgressDialog progressDialog=new ProgressDialog(SelectOperatorBroadBand.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();


        String type="broadband";
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PrepaidResponse> call = apiInterface.DTH(type);
        call.enqueue(new Callback<PrepaidResponse>() {
            @Override
            public void onResponse(Call<PrepaidResponse> call, Response<PrepaidResponse> response) {

                if (response.isSuccessful()) ;

                PrepaidResponse prepaidResponse=response.body();

                PrepaidResponse.StatusBean statusBean=prepaidResponse.getStatus();
                if (statusBean.getCode() == 200){
                    progressDialog.dismiss();

                    List<PrepaidResponse.DataBean> dataBeanList=prepaidResponse.getData();

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
                    recycler_landline.setLayoutManager(mLayoutManager);
                    landLineecyclerAdapter=new BroadBandRecyclerAdapter(dataBeanList,SelectOperatorBroadBand.this);
                    recycler_landline.setAdapter(landLineecyclerAdapter);


                }
                else if (statusBean.getCode() == 400){
                    progressDialog.dismiss();
                    Toast.makeText(SelectOperatorBroadBand.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();
                }



                if (response.code() == 200) {

                    progressDialog.dismiss();



                } else if (response.code() == 401) {
                    progressDialog.dismiss();

                } else if(response.code() ==400) {
                    progressDialog.dismiss();
                    Toast.makeText(SelectOperatorBroadBand.this, "respond", Toast.LENGTH_SHORT).show();

                }
                else if(response.code() == 500){
                    progressDialog.dismiss();
                    Toast.makeText(SelectOperatorBroadBand.this, "true", Toast.LENGTH_SHORT).show();


                }

            }


            @Override
            public void onFailure(Call<PrepaidResponse> call, Throwable t) {
                progressDialog.dismiss();
                //Log.e(TAG, "failed"+t.getMessage() );
                Toast.makeText(SelectOperatorBroadBand.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }
}


