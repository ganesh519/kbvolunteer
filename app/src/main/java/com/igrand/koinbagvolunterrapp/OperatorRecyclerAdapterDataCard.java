package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class OperatorRecyclerAdapterDataCard extends RecyclerView.Adapter<OperatorRecyclerAdapterDataCard.Holder> {

    List<PrepaidResponse.DataBean> dataBeanList;
    String MobileNumber;

    Context context;

    public OperatorRecyclerAdapterDataCard(List<PrepaidResponse.DataBean> dataBeanList, OperatorActivityDataCard operatorActivityDataCard, String mobileNumber) {
        this.dataBeanList=dataBeanList;
        this.context=operatorActivityDataCard;
        this.MobileNumber=mobileNumber;
    }

    @NonNull
    @Override
    public OperatorRecyclerAdapterDataCard.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_operator, viewGroup, false);

        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OperatorRecyclerAdapterDataCard.Holder holder, final int i) {
        holder.txt_name.setText(dataBeanList.get(i).getOperator_name());

        Picasso.get().load(dataBeanList.get(i).getImage()).error(R.drawable.kb).into(holder.image_network);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle=new Bundle();
                bundle.putString("Operator_type",dataBeanList.get(i).getOperator_type());
                bundle.putString("Operator_name",dataBeanList.get(i).getOperator_name());
                bundle.putString("Operator_image",dataBeanList.get(i).getImage());
                bundle.putString("Operator_code",dataBeanList.get(i).getOperator_code());
                bundle.putString("Mobile",MobileNumber);

                BottomSheetDialog bottomSheetDialog=new BottomSheetDialog(context);
                // bottomSheetDialog.setContentView(R.layout.activity_select_circle);
                CustomBottomSheetDialogFragmentData newFragment = new CustomBottomSheetDialogFragmentData();
                newFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), "Dialog");
                newFragment.setArguments(bundle);
//                bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);







               /* Intent intent=new Intent(context, CirclesList.class);
                intent.putExtra("Operatorname",dataBeanList.get(i).getOperator_name());
                intent.putExtra("Operatortype",dataBeanList.get(i).getOperator_type());
                intent.putExtra("Operatorimage",dataBeanList.get(i).getImage());
                intent.putExtra("Operator",dataBeanList.get(i).getOperator_code());
                context.startActivity(intent);*/
            }
        });



    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView txt_name;
        ImageView image_network;

        public Holder(@NonNull View itemView) {
            super(itemView);

            image_network=itemView.findViewById(R.id.image_network);
            txt_name=itemView.findViewById(R.id.txt_name);

        }
    }
}
