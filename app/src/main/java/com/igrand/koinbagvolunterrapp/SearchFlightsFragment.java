package com.igrand.koinbagvolunterrapp;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class SearchFlightsFragment extends Fragment {


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_search_flights, container, false);


        BottomSheetDialog dialog = new BottomSheetDialog(getContext());
        //dialog.show();
        dialog.setContentView(R.layout.fragment_search_flights);
        new CustomBottomSheetDialogFragment7().show(getActivity().getSupportFragmentManager(), "Dialog");
        return null;

    }
}