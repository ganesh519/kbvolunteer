package com.igrand.koinbagvolunterrapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Callback;

public class CustomGridViewAdapter extends BaseAdapter {

    Context context;
    List<Data13> data13;



    public CustomGridViewAdapter(Context context, int row_grid, List<Data13> data13) {

        this.context=context;
        this.data13=data13;
    }


    @Override
    public int getCount() {
        return data13.size();
    }

    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {
        if(position==12) {
            Intent intent=new Intent(context, WalletRequest.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);
        }
        if(position==0) {
            Intent intent=new Intent(context, MobileRechargeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);
        }

        if(position==1) {

            Intent intent=new Intent(context, DTHRecharge.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);

        }
        if(position==2) {

            Intent intent=new Intent(context, Electricity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);

        }

        if(position==3) {

            Intent intent=new Intent(context, LandLine.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);
        }

        if(position==4) {

            Intent intent=new Intent(context, BroadBand.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);

        }
       /* if(position==6) {

            Intent intent=new Intent(context, DataCard.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);

        }*/

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView digitalimage;
        View view = LayoutInflater.from(context).inflate(R.layout.row_grid, null);
         digitalimage= view.findViewById(R.id.digitalimage);
        TextView digitalname = view.findViewById(R.id.digitalname);

        String image=data13.get(position).image;
        Picasso.get().load(image).into(digitalimage);

        String text=data13.get(position).name;
        digitalname.setText(text);

        if (position==13) {
            digitalimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Dialog dialog = new Dialog(context);
                    TextView ok;
                    ImageView xmrk;
                    dialog.setContentView(R.layout.dialogbox4);
                    dialog.show();
                    ok = dialog.findViewById(R.id.ok);
                    xmrk = dialog.findViewById(R.id.xmrk);

                    //dialog.setCancelable(false);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    xmrk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                }
            });


        }

        return view;
    }
}