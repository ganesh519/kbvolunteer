package com.igrand.koinbagvolunterrapp;

public class TopupResponse {

    String decription,amount,talktime,validity,longdecription;

    public TopupResponse(String decription, String amount, String talktime, String validity, String longdecription) {
        this.decription = decription;
        this.amount = amount;
        this.talktime = talktime;
        this.validity = validity;
        this.longdecription = longdecription;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTalktime() {
        return talktime;
    }

    public void setTalktime(String talktime) {
        this.talktime = talktime;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getLongdecription() {
        return longdecription;
    }

    public void setLongdecription(String longdecription) {
        this.longdecription = longdecription;
    }
}
