package com.igrand.koinbagvolunterrapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.igrand.koinbagvolunterrapp.Adapters.MyAdapter9;

@SuppressLint("ValidFragment")
public class BoardingFragment extends Fragment {
    public BoardingFragment() {

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_boarding, container, false);

        TabLayout tabLayout8=(TabLayout)v.findViewById(R.id.tabLayout8);
        final ViewPager viewPager8=(ViewPager)v.findViewById(R.id.viewPager8);

        tabLayout8.addTab(tabLayout8.newTab().setText("BOARDING"));
        tabLayout8.addTab(tabLayout8.newTab().setText("DROP"));
        tabLayout8.setTabGravity(TabLayout.GRAVITY_FILL);

        final MyAdapter9 adapter = new MyAdapter9(getContext(),getActivity().getSupportFragmentManager(), tabLayout8.getTabCount());

        viewPager8.setAdapter(adapter);
        viewPager8.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout8));

        tabLayout8.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager8.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return v;

    }
}
