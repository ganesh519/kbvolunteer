package com.igrand.koinbagvolunterrapp.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.R;
import com.igrand.koinbagvolunterrapp.WalletRequest;

import java.util.ArrayList;
import java.util.List;

public class FragmentWallet1 extends Fragment {
    TabLayout tabs_wallet;
    public ViewPager viewPager;
    ImageView toggledigital,walletRequest;
    DrawerLayout drawerLayout;
    TextView textview,textview1;
    Typeface typeface;
    ViewPagerAdapterNotifications pagerAdapterNotifications;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_wallet, container, false);

        drawerLayout=(DrawerLayout)getActivity().findViewById(R.id.drawer_layout);
        toggledigital=(ImageView)v.findViewById(R.id.togglewallet);
        toggledigital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });

        textview=(TextView)v.findViewById(R.id.textview);


        typeface=Typeface.createFromAsset(getActivity().getAssets(),"font/Avenir-Heavy.ttf");
        textview.setTypeface(typeface);
        tabs_wallet = (TabLayout)v.findViewById(R.id.tabs_wallet);
        viewPager = (ViewPager)v.findViewById(R.id.viewPager);



        tabs_wallet.setSelectedTabIndicatorColor(Color.parseColor("#FFD71B"));
        tabs_wallet.setTabTextColors(Color.parseColor("#80F8F8F8"), Color.parseColor("#FFD71B"));

        pagerAdapterNotifications = new ViewPagerAdapterNotifications(getChildFragmentManager());
        pagerAdapterNotifications.addFragment(new AllFragment2(), "All");
        pagerAdapterNotifications.addFragment(new RequestsFragment1(), "Requests");
        pagerAdapterNotifications.addFragment(new ApprovedFragment1(), "Approved");
        pagerAdapterNotifications.addFragment(new RejectedFragment1(), "Rejected");

        viewPager.setAdapter(pagerAdapterNotifications);
        tabs_wallet.setupWithViewPager(viewPager);


        walletRequest=(ImageView)v.findViewById(R.id.walletRequest0);
        walletRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), WalletRequest.class);
                startActivity(intent);
            }
        });


        return v;

    }


    public class ViewPagerAdapterNotifications extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapterNotifications(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

    }

}
