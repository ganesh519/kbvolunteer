package com.igrand.koinbagvolunterrapp;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Adapters.MyAdapter4;
import com.igrand.koinbagvolunterrapp.Client.APIClient1;
import com.igrand.koinbagvolunterrapp.Client.APIClient3;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Electricity extends AppCompatActivity {


    private static final int MY_REQ_CODE = 1;
    TabLayout tabLayout2;
    ViewPager viewPager2;
    Button electricitybutton;
    ImageView backelec;
    RadioButton prepaid, postpaid;
    RadioGroup rg;
    String type, statename, stateboard, stateconsumer, circlename, circlecode, operatorname, operatorcode, ConsumerNumber;
    EditText selectboard, consumernumber,selectstate,consumername,consumermobile;
    ApiInterface apiInterface;
    String Token = "IORQPXENSFQUELDZX6OQRRDVI2FXUWYEVRRVCVWJOY5X6CBKH0X";
    RelativeLayout relative_sstate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_electricity);

        tabLayout2 = (TabLayout) findViewById(R.id.tabLayout22);
        viewPager2 = (ViewPager) findViewById(R.id.viewPager22);
        electricitybutton = findViewById(R.id.electricitybutton);
        backelec = findViewById(R.id.backelec);
        rg = (RadioGroup) findViewById(R.id.rg);
        prepaid = findViewById(R.id.prepaid);
        postpaid = findViewById(R.id.postpaid);
        selectstate = findViewById(R.id.selectstate);
        selectboard = findViewById(R.id.selectboard);
        consumernumber = findViewById(R.id.consumernumber);


        //relative_sstate=findViewById(R.id.relative_sstate);


        if (getIntent() !=null) {
            circlename = getIntent().getStringExtra("CircleName");
            circlecode = getIntent().getStringExtra("CircleCode");
            operatorname = getIntent().getStringExtra("OperatorName");
            operatorcode = getIntent().getStringExtra("OperatorCode");


            selectstate.setText(circlename);
            selectboard.setText(operatorname);
        }




        if (selectstate.getText().toString().isEmpty()) {

            selectboard.setVisibility(View.GONE);

        } else {
            selectboard.setVisibility(View.VISIBLE);
            //selectboard.setText(operatorname);
        }


      /*  selectstate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                BottomSheetDialog dialog = new BottomSheetDialog(Electricity.this);
                dialog.setContentView(R.layout.activity_select_circle1);
                //dialog.show();
                new CustomBottomSheetDialogFragment4().show(getSupportFragmentManager(), "Dialog");
                // dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);


            }
        });
*/

        selectstate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_UP){
                    new CustomBottomSheetDialogFragment4().show(getSupportFragmentManager(), "Dialog");

                    // Do what you want
                    return true;
                }

                return false;
            }
        });
       /* selectboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("CircleName", circlename);
                bundle.putString("CircleCode", circlecode);
                CustomBottomSheetDialogFragmentboard customBottomSheetDialogFragmentboard = new CustomBottomSheetDialogFragmentboard();
                customBottomSheetDialogFragmentboard.setArguments(bundle);
                customBottomSheetDialogFragmentboard.show(getSupportFragmentManager(), "Dialog");


            }
        });*/


        selectboard.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_UP){
                    Bundle bundle = new Bundle();
                    bundle.putString("CircleName", circlename);
                    bundle.putString("CircleCode", circlecode);
                    CustomBottomSheetDialogFragmentboard customBottomSheetDialogFragmentboard = new CustomBottomSheetDialogFragmentboard();
                    customBottomSheetDialogFragmentboard.setArguments(bundle);
                    customBottomSheetDialogFragmentboard.show(getSupportFragmentManager(), "Dialog");

                    // Do what you want
                    return true;
                }


                return false;
            }
        });

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.prepaid) {

                    //  type="prepaid-mobile";
                    //getprepaid(type);


                } else {
                    //type="postpaid-mobile";
                    //getpostpaid(type);


                }
            }
        });


        backelec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Electricity.this, NavigationDrawerDashboard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });





        electricitybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ConsumerNumber = consumernumber.getText().toString();

                stateconsumer = consumernumber.getText().toString();


                statename = selectstate.getText().toString();
                stateboard = selectboard.getText().toString();

                operatorcode = getIntent().getStringExtra("OperatorCode");

                String board=selectboard.getText().toString();



                if(ConsumerNumber.equals("") || board.equals("")) {
                    Toast.makeText(Electricity.this, "Please select all values...", Toast.LENGTH_SHORT).show();
                } else {




                    final ProgressDialog progressDialog = new ProgressDialog(Electricity.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                    TextView text = (TextView) progressDialog.findViewById(R.id.text);
                    ImageView image = (ImageView) progressDialog.findViewById(R.id.image);


                    apiInterface = APIClient3.getClient().create(ApiInterface.class);
                    Call<ElectricityFetch> call = apiInterface.electrictydetails(Token, ConsumerNumber, operatorcode);
                    call.enqueue(new Callback<ElectricityFetch>() {
                        @Override
                        public void onResponse(Call<ElectricityFetch> call, Response<ElectricityFetch> response) {

                            if (response.isSuccessful()) ;
                            ElectricityFetch operatorFetch = response.body();



                            if (operatorFetch.getErrorCode()==200) {
                                progressDialog.dismiss();


                                String costumer_name=operatorFetch.getCustomer_Name();
                                String customer_address=operatorFetch.getAddress();
                                String duedate=operatorFetch.getBillDueDate();
                                String billamount=operatorFetch.getBillAmount();
                                String req_id=operatorFetch.getRequestId();



                                Intent intent = new Intent(Electricity.this, ElectricityBill.class);
                                intent.putExtra("StateName", statename);
                                intent.putExtra("StateBoard", stateboard);
                                intent.putExtra("StateConsumer", stateconsumer);
                                intent.putExtra("CircleCode", circlecode);


                                intent.putExtra("costumer_name", costumer_name);
                                intent.putExtra("customer_address", customer_address);
                                intent.putExtra("duedate", duedate);
                                intent.putExtra("billamount", billamount);
                                intent.putExtra("req_id", req_id);


                                intent.putExtra("OperatorCode", operatorcode);
                                intent.putExtra("RefNo", ConsumerNumber);
                                intent.putExtra("Token", "IORQPXENSFQUELDZX6OQRRDVI2FXUWYEVRRVCVWJOY5X6CBKH0X");
                                startActivity(intent);
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


                            } else if(operatorFetch.getErrorCode()!=200){
                                progressDialog.dismiss();
                                Toast.makeText(Electricity.this, "Already Paid...", Toast.LENGTH_SHORT).show();

                            }


                        }

                        @Override
                        public void onFailure(Call<ElectricityFetch> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            finish();
                            Toast toast = Toast.makeText(Electricity.this,
                                    t.getMessage(), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }


                    });


                }



            }
        });


    }
}