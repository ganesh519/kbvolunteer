package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomSpinnerAdapter extends ArrayAdapter<String> {

    private Context activity;
    private List<Category_spinner> data;
    public Resources res;
    Category_spinner tempValues = null;
    LayoutInflater inflater;

    /*************  CustomSpinnerAdapter Constructor *****************/
    public CustomSpinnerAdapter(
            Context activity,
            int textViewResourceId,
            List data
    ) {
        super(activity, textViewResourceId, data);

        /********** Take passed values **********/
        this.activity = activity;
        this.data = data;

        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.spinner_rows, parent, false);

        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (Category_spinner) data.get(position);

        TextView label = (TextView) row.findViewById(R.id.qantity);
        TextView label1 = row.findViewById(R.id.price);
        TextView textView = row.findViewById(R.id.discount);

        // Set values for spinner each row
        label.setText(tempValues.getQuantiti()+" ");
        label.setTag(tempValues.getId());

        label1.setText(tempValues.getStartlimit());
        textView.setText("("+tempValues.getDprice()+")");
       // textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        return row;
    }

}
