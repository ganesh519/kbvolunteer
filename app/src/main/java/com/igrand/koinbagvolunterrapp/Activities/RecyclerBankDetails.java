package com.igrand.koinbagvolunterrapp.Activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.R;

import java.util.List;

public class RecyclerBankDetails extends RecyclerView.Adapter<RecyclerBankDetails.Holder> {

    List<DataSpinner0> dataSpinner0s;
    Context context;
    public RecyclerBankDetails(Context applicationContext, List<DataSpinner0> dataSpinner0s) {

        this.context=applicationContext;
        this.dataSpinner0s=dataSpinner0s;

    }

    @NonNull
    @Override
    public RecyclerBankDetails.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_bank, viewGroup, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerBankDetails.Holder holder, int i) {

        holder.bankname.setText(dataSpinner0s.get(i).getBankName());
        //holder.accounttype.setText(dataSpinner0s.get(i).acc);
        holder.accountnumber.setText(dataSpinner0s.get(i).getAccountNo());
        holder.branch.setText(dataSpinner0s.get(i).getBranch());
        holder.ifsccode.setText(dataSpinner0s.get(i).getIfscCode());
        holder.accountname.setText(dataSpinner0s.get(i).getAccountName());


    }

    @Override
    public int getItemCount() {
        return dataSpinner0s.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView bankname,accountname,accountnumber,accounttype,branch,ifsccode;
        public Holder(@NonNull View itemView) {
            super(itemView);

            bankname=itemView.findViewById(R.id.bankname);
            accountname=itemView.findViewById(R.id.accountname);
            accountnumber=itemView.findViewById(R.id.accountnumber);
            accounttype=itemView.findViewById(R.id.accounttype);
            branch=itemView.findViewById(R.id.branch);
            ifsccode=itemView.findViewById(R.id.ifsccode);

        }
    }
}
