package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.igrand.koinbagvolunterrapp.Fragments.Data15;

import java.util.List;

public class SuccessRecyclerAdapter extends RecyclerView.Adapter<SuccessRecyclerAdapter.Holder>  {

    List<Data15> data15;
    Context context;
    ApiInterface apiInterface;
    PrefManager prefManager;
    String id;

    public SuccessRecyclerAdapter(Context context, List<Data15> data15) {

        this.context=context;
        this.data15=data15;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview5, viewGroup, false);
        return new Holder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

      /*  holder.api_type.setText(data15.get(i).apiType);
        holder.digitalnumber.setText(data15.get(i).digitalNumber);
        holder.prevbal.setText(data15.get(i).prevBal);
        holder.curbal.setText(data15.get(i).curBal);
        holder.createdat.setText(data15.get(i).createdAt);
        holder.status.setText(data15.get(i).status);
        holder.amount.setText(data15.get(i).amount);
        holder.scomrs1.setText(data15.get(i).commission);*/


    }

    @Override
    public int getItemCount() {
        return data15.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView api_type,digitalnumber,prevbal,curbal,createdat,status,amount,scomrs1;

        public Holder(@NonNull View itemView) {
            super(itemView);

            api_type=itemView.findViewById(R.id.api_type1);
            digitalnumber=itemView.findViewById(R.id.digitalnumber1);
            prevbal=itemView.findViewById(R.id.prevbal1);
            curbal=itemView.findViewById(R.id.curbal1);
            createdat=itemView.findViewById(R.id.created_at1);
            status=itemView.findViewById(R.id.status1);
            amount=itemView.findViewById(R.id.drs1);
            scomrs1=itemView.findViewById(R.id.scomrs1);
        }
    }
}
