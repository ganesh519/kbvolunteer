package com.igrand.koinbagvolunterrapp.Adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.Fragments.Approved;
import com.igrand.koinbagvolunterrapp.Fragments.Data10;
import com.igrand.koinbagvolunterrapp.Fragments.Proceesing;
import com.igrand.koinbagvolunterrapp.Fragments.StatusDataResponse10;
import com.igrand.koinbagvolunterrapp.Fragments.StatusResponse10;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.ProfileImage;
import com.igrand.koinbagvolunterrapp.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApprovedRecyclerAdapter extends RecyclerView.Adapter<ApprovedRecyclerAdapter.Holder> {
    List<Approved> proceesingList;
    Context context;
    ApiInterface apiInterface;
    PrefManager prefManager;
    String id;

    public ApprovedRecyclerAdapter(Context context, List<Approved> proceesingList) {
        this.context=context;
        this.proceesingList=proceesingList;

    }

    @NonNull
    @Override
    public ApprovedRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview2, viewGroup, false);

        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ApprovedRecyclerAdapter.Holder holder, final int i) {

        holder.approved_money.setText(proceesingList.get(i).amount);
        holder.money3.setText(proceesingList.get(i).amount);

        holder.request_date.setText(proceesingList.get(i).requestedDate);
        holder.updated_date.setText(proceesingList.get(i).updatedDate);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialogbox1);
                dialog.show();
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                final TextView reqdate1,reqrs1,reqdes1,appdes,appdate,apprs;
                final ImageView image200;
                final RelativeLayout relative2;

                reqdate1=dialog.findViewById(R.id.reqdate1);
                reqrs1=dialog.findViewById(R.id.reqrs1);
                reqdes1=dialog.findViewById(R.id.reqdes1);
                image200=dialog.findViewById(R.id.image200);
                appdes=dialog.findViewById(R.id.appdes);
                appdate=dialog.findViewById(R.id.appdate);
                apprs=dialog.findViewById(R.id.apprs);
                relative2=dialog.findViewById(R.id.relative2);

                prefManager=new PrefManager(context);
                HashMap<String, String> profile=prefManager.getUserDetails();
                id=profile.get("id");
                String request_id=proceesingList.get(i).requestId;

                final ProgressDialog progressDialog1 = new ProgressDialog(context);
                progressDialog1.setMessage("Loading.....");
                progressDialog1.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<StatusResponse10> call1 = apiInterface.Statuss10(id,request_id);
                call1.enqueue(new Callback<StatusResponse10>() {
                    @Override
                    public void onResponse(Call<StatusResponse10> call, Response<StatusResponse10> response) {
                        if (response.isSuccessful()) ;

                        StatusResponse10 statusResponse = response.body();
                        if(statusResponse!=null){

                            StatusDataResponse10 statusDataResponse = statusResponse.status;

                            if (statusDataResponse.code == 200) {
                                progressDialog1.dismiss();
                                relative2.setVisibility(View.VISIBLE);

                                Data10 data = statusResponse.data;
                                String amount = data.amount;
                                String created_at = data.createdAt;
                                String updated_at = data.updatedAt;
                                String description = data.description;
                                String attachments = data.attachments;
                                String updateDescription = data.updateDescription;


                                reqdate1.setText(created_at);
                                reqrs1.setText(amount);
                                reqdes1.setText(description);
                                appdes.setText(updateDescription);
                                appdate.setText(updated_at);
                                apprs.setText(amount);
                                Picasso.get().load(attachments).error(R.drawable.kb).into(image200);

                                image200.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {


                                        image200.buildDrawingCache();
                                        Bitmap bitmap = image200.getDrawingCache();
                                        Intent intent=new Intent(context, ProfileImage.class);

                                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                        intent.putExtra("byteArray", _bs.toByteArray());
                                        context.startActivity(intent);

                                    }
                                });
                                // Toast.makeText(context, statusDataResponse.message, Toast.LENGTH_SHORT).show();

                            } else if (statusDataResponse.code == 409) {
                                progressDialog1.dismiss();
                                Toast.makeText(context, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                            }

                        }

                    }


                    @Override
                    public void onFailure(Call<StatusResponse10> call, Throwable t) {
                        progressDialog1.dismiss();
                        //Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                        Toast toast= Toast.makeText(context,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();

                    }
                });










                ImageView close=(ImageView) dialog.findViewById(R.id.close);

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });

            }
        });

    }


    @Override
    public int getItemCount() {
        return proceesingList.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        TextView request_date,amount,approved_money,updated_date,money3;
        ImageView image;

        public Holder(@NonNull View itemView) {
            super(itemView);
            approved_money=itemView.findViewById(R.id.approved_money);
            request_date=itemView.findViewById(R.id.request_date);
            updated_date=itemView.findViewById(R.id.updated_date);
            money3=itemView.findViewById(R.id.money3);
            image=itemView.findViewById(R.id.image);

        }
    }
}
