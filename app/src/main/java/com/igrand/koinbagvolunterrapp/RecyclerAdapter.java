package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolderClass> {

    Context context;


    public RecyclerAdapter(BusDetails busDetails) {
        this.context=busDetails;

    }

    @NonNull
    @Override
    public RecyclerAdapter.ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view= LayoutInflater.from(context).inflate(R.layout.recycler_list,viewGroup,false);
        ViewHolderClass viewHolderClass=new ViewHolderClass(view);
        return viewHolderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.ViewHolderClass viewHolderClass, int i) {

        viewHolderClass.typeface=Typeface.createFromAsset(context.getAssets(),"font/Avenir-Heavy.ttf");
        viewHolderClass.typeface1=Typeface.createFromAsset(context.getAssets(),"font/Avenir-roman.ttf");
        viewHolderClass.typeface2=Typeface.createFromAsset(context.getAssets(),"font/Avenir-Medium.ttf");

        viewHolderClass.morning.setTypeface(viewHolderClass.typeface);
        viewHolderClass.ac.setTypeface(viewHolderClass.typeface1);
        viewHolderClass.busrs.setTypeface(viewHolderClass.typeface);
        viewHolderClass.busseats.setTypeface(viewHolderClass.typeface2);
        viewHolderClass.bushyd.setTypeface(viewHolderClass.typeface2);
        viewHolderClass.bustym.setTypeface(viewHolderClass.typeface);
        viewHolderClass.bushr.setTypeface(viewHolderClass.typeface2);
        viewHolderClass.usmumbai.setTypeface(viewHolderClass.typeface2);
        viewHolderClass.bustym1.setTypeface(viewHolderClass.typeface);
        viewHolderClass.busrating.setTypeface(viewHolderClass.typeface2);
        viewHolderClass.bus13rating.setTypeface(viewHolderClass.typeface2);
        viewHolderClass.buscancel.setTypeface(viewHolderClass.typeface2);




        viewHolderClass.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SelectSeat.class);
                v.getContext().startActivity(intent);
            }
        });

        viewHolderClass.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialog dialog = new BottomSheetDialog(context);
                dialog.setContentView(R.layout.activity_amenities);
                dialog.show();
                new CustomBottomSheetDialogFragment13().show(((BusDetails)context).getSupportFragmentManager(), "Dialog");

            }
        });

    }






    @Override
    public int getItemCount() {
        return 5;
    }

    public static class ViewHolderClass extends RecyclerView.ViewHolder {
        private final Context context;
        public TextView more,morning,ac,busrs,busseats,bushyd,bustym,bushr,usmumbai,bustym1,busrating,bus13rating,buscancel;
        Typeface typeface,typeface1,typeface2;
        public ViewHolderClass(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            more=(TextView)itemView.findViewById(R.id.more);
            morning=(TextView)itemView.findViewById(R.id.morning);
            ac=(TextView)itemView.findViewById(R.id.ac);
            busrs=(TextView)itemView.findViewById(R.id.busrs);
            bushyd=(TextView)itemView.findViewById(R.id.bushyd);
            busseats=(TextView)itemView.findViewById(R.id.busseat);
            bustym=(TextView)itemView.findViewById(R.id.bustym);
            bushr=(TextView)itemView.findViewById(R.id.bus9hr);
            usmumbai=(TextView)itemView.findViewById(R.id.busmumbai);
            bustym1=(TextView)itemView.findViewById(R.id.bustym1);
            busrating=(TextView)itemView.findViewById(R.id.busrating);
            bus13rating=(TextView)itemView.findViewById(R.id.busrating13);
            buscancel=(TextView)itemView.findViewById(R.id.buscancel);
        }
    }
}
