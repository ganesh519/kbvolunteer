package com.igrand.koinbagvolunterrapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

@SuppressLint("ValidFragment")
public class SortFilterFragment extends Fragment {
    public SortFilterFragment() {

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_sort_filter, container, false);


        BottomSheetDialog dialog = new BottomSheetDialog(getContext());
        dialog.setContentView(R.layout.fragment_sort_filter);
        // dialog.show();
        new CustomBottomSheetDialogFragment10().show(getActivity().getSupportFragmentManager(), "Dialog");
        dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);




     /*   ImageView brown=(ImageView)v.findViewById(R.id.brown);
        brown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment SelectedSeat=new SelectedSeat();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container3, SelectedSeat);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });*/

     ImageView exit18=(ImageView)v.findViewById(R.id.exit18);
        exit18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),FlightDetails.class);
                startActivity(intent);
            }
        });
        return null;
    }
}
