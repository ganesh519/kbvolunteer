package com.igrand.koinbagvolunterrapp.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.BaseActivity;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.LoginResponse1;
import com.igrand.koinbagvolunterrapp.NavigationDrawerDashboard;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomDialogActivity1 extends BaseActivity {
    ApiInterface apiInterface;
    LoginResponse1.StatusBean statusBean;
    PrefManager prefManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);

        prefManager=new PrefManager(CustomDialogActivity1.this);

        String mobile_number = getIntent().getStringExtra("Mobile");
        String password = getIntent().getStringExtra("Pass");

       // if (!password.isEmpty()) {

            final Dialog progressDialog = new Dialog(CustomDialogActivity1.this, R.style.Theme_AppCompat_Light_NoActionBar);
            progressDialog.setContentView(R.layout.custom_dialog);
            Window window = progressDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            LottieAnimationView animationView = progressDialog.findViewById(R.id.lottie_view);
            animationView.setAnimation("refresh.json");
            animationView.playAnimation();
            animationView.loop(true);
            progressDialog.show();
            TextView text = (TextView) progressDialog.findViewById(R.id.text);
            ImageView image = (ImageView) progressDialog.findViewById(R.id.image);

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<LoginResponse1> call = apiInterface.Statuss1(mobile_number, password);
            call.enqueue(new Callback<LoginResponse1>() {
                @Override
                public void onResponse(Call<LoginResponse1> call, Response<LoginResponse1> response) {


                    if (response.code() == 200) {

                        progressDialog.dismiss();
                        statusBean = response.body() != null ? response.body().getStatus() : null;

                        //Toast.makeText(WelcomeBack.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();
                        LoginResponse1.DataBean dataBean = response.body().getData();


                        String id = String.valueOf(dataBean.id);
                        String firstName = dataBean.first_name;
                        String surname = dataBean.surname;
                        String mobile_number = dataBean.mobile_number;
                        String user_id = dataBean.user_id;
                        String wallet_bal = dataBean.wallet_bal;
                        String ngo_name=dataBean.ngo_name;
                        String ngo_id=dataBean.ngo_id;
                        String photo=dataBean.photo;
                        String created_id=dataBean.getCreated_id();

                        Intent intent = new Intent(CustomDialogActivity1.this, NavigationDrawerDashboard.class);
                        startActivity(intent);

                        prefManager.createLogin(id,ngo_id, firstName, surname, mobile_number, wallet_bal, user_id,ngo_name,photo,created_id);

                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                       // Toast.makeText(CustomDialogActivity1.this, "Please Enter Valid Password", Toast.LENGTH_SHORT).show();

                        Toast toast= Toast.makeText(CustomDialogActivity1.this,
                                "Please Enter Valid Password", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();
                        finish();

                    }

                }

                @Override
                public void onFailure(Call<LoginResponse1> call, Throwable t) {
                    progressDialog.dismiss();
                    //Toast.makeText(CustomDialogActivity1.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                    Toast toast= Toast.makeText(CustomDialogActivity1.this,
                            t.getMessage() , Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();

                }
            });

        }

}
