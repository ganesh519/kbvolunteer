package com.igrand.koinbagvolunterrapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.igrand.koinbagvolunterrapp.BaseActivity;
import com.igrand.koinbagvolunterrapp.R;
import com.igrand.koinbagvolunterrapp.TravellerInformation;

public class AddMeals extends BaseActivity {

    Button book4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meals);

        book4=findViewById(R.id.book4);
        book4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AddMeals.this,TravellerInformation.class);
                startActivity(intent);
            }
        });
    }
}
