package com.igrand.koinbagvolunterrapp.Fragments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ApproveRecyclerAdapter extends RecyclerView.Adapter<ApproveRecyclerAdapter.Holder> {

    List<Success> successes;
    Context context;
    ApiInterface apiInterface;
    PrefManager prefManager;
    String id;

    public ApproveRecyclerAdapter(Context context, List<Success> successes) {
        this.context=context;
        this.successes=successes;

    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview7, viewGroup, false);
        return new Holder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        String upperString = successes.get(i).apiType.substring(0,1).toUpperCase() + successes.get(i).apiType.substring(1);

        holder.api_type.setText(upperString);
        holder.digitalnumber.setText(successes.get(i).digitalNumber);
        holder.prevbal.setText(successes.get(i).prevBal);
        holder.curbal.setText(successes.get(i).curBal);
        holder.createdat.setText(successes.get(i).createdAt);
        holder.amount.setText(successes.get(i).amount);
        holder.scomrs1.setText(successes.get(i).commission);
        Picasso.get().load(successes.get(i).operatorimage).error(R.drawable.kb).into(holder.jio);

    }

    @Override
    public int getItemCount() {
        return successes.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView api_type,digitalnumber,prevbal,curbal,createdat,status,amount,scomrs1;
        ImageView image,jio;
        public Holder(@NonNull View itemView) {
            super(itemView);
            api_type=itemView.findViewById(R.id.sapi_type11);
            digitalnumber=itemView.findViewById(R.id.sdigitalnumber11);
            prevbal=itemView.findViewById(R.id.sprevbal11);
            curbal=itemView.findViewById(R.id.scurbal11);
            createdat=itemView.findViewById(R.id.screated_at11);
            status=itemView.findViewById(R.id.sstatus11);
            amount=itemView.findViewById(R.id.sdrs11);
            scomrs1=itemView.findViewById(R.id.sscomrs11);
            jio=itemView.findViewById(R.id.jio);
        }
    }
}
