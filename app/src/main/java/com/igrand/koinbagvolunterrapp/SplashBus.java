package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashBus extends BaseActivity {

    private static int splashscreentimeout=2000;
    ImageView backblack10;
    TextView relax,fetch;
    Typeface typeface,typeface1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_bus);

        relax=findViewById(R.id.relax);
        fetch=findViewById(R.id.fetch);

        typeface=Typeface.createFromAsset(getAssets(),"font/Avenir-Light.ttf");
        typeface1=Typeface.createFromAsset(getAssets(),"font/Avenir-Black.ttf");

        relax.setTypeface(typeface1);
        fetch.setTypeface(typeface);



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashBus.this,BusDetails.class);
                startActivity(intent);

            }
        },splashscreentimeout);

        backblack10=findViewById(R.id.backblack10);
        backblack10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SplashBus.this,Flight.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
