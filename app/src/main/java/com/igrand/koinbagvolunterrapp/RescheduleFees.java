package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class RescheduleFees extends BaseActivity {

    LinearLayout farebreakup1,baggage2,reschedule2;
    ImageView backfare3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reschedule_fees);

        farebreakup1=findViewById(R.id.farebrakup1);
        reschedule2=findViewById(R.id.reschedule2);
        baggage2=findViewById(R.id.baggage2);
        backfare3=findViewById(R.id.backfare3);

        farebreakup1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RescheduleFees.this,FareBreakup.class);
                startActivity(intent);
            }
        });

        reschedule2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RescheduleFees.this,RescheduleFees.class);
                startActivity(intent);
            }
        });

        baggage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RescheduleFees.this,Baggage.class);
                startActivity(intent);
            }
        });

        backfare3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RescheduleFees.this,PromoAppliedFlights.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
