package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProceedToRechargeDataCard extends BaseActivity {

    Button proceedtorecharge;
    TextView changeoperator, browseplans, fname, sname, umob, type1, state, longde, otype;
    ImageView back5, imgnet;
    ApiInterface apiInterface;
    PrefManager prefManager;
    EditText editamount;
    String Id, f_name, surname, mobile1, walletbals, userid, photo, Operator_Code, Circle_Code, oCODE, cCode, LongDesc, Type, Circle,
            amount, Type_Id, operator, circle_code, recharge_type, MobileNumber, Typee, operator_image, operator_name, operator_type, circle_name;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proceed_to_recharge_data_card);

        changeoperator = findViewById(R.id.changeoperator);
        fname = findViewById(R.id.fname);
        sname = findViewById(R.id.sname);
        browseplans = findViewById(R.id.browseplans);
        umob = findViewById(R.id.umob);
        type1 = findViewById(R.id.type);
        state = findViewById(R.id.stateid);
        editamount = findViewById(R.id.editamount);
        longde = findViewById(R.id.longde);
        otype = findViewById(R.id.otype);
        imgnet = findViewById(R.id.imgnet);




        if (getIntent() != null) {

            Operator_Code = getIntent().getStringExtra("Operator");
            Circle_Code = getIntent().getStringExtra("Circle");
            operator_name = getIntent().getStringExtra("Operatorname");
            operator_type = getIntent().getStringExtra("Operatortype");
            operator_image = getIntent().getStringExtra("Operatorimage");
            circle_name = getIntent().getStringExtra("CircleName");


            Picasso.get().load(operator_image).error(R.drawable.kb).into(imgnet);

            type1.setText(operator_name);
            otype.setText(operator_type);
            state.setText(circle_name);


        }

        Typee = getIntent().getStringExtra("Type");

        amount = getIntent().getStringExtra("Operate");
        LongDesc = getIntent().getStringExtra("LongDesc");


        prefManager = new PrefManager(ProceedToRechargeDataCard.this);

        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");
        f_name = profile.get("f_name");
        surname = profile.get("Surname");
        mobile1 = profile.get("Mobile");
        photo = profile.get("Photo");
        userid = profile.get("Userid");
        walletbals = profile.get("Walletbals");




        MobileNumber = getIntent().getStringExtra("Mobile");

        umob.setText(MobileNumber);
        fname.setText(f_name);
        sname.setText(surname);
        editamount.setText(amount);
        longde.setText(LongDesc);


        /*otype.setText(operator_type);
        type.setText(operator_name);*/


        //getprepaid(Typee);





        final ProgressDialog progressDialog=new ProgressDialog(ProceedToRechargeDataCard.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();


        final String type="landline";
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PrepaidResponse> call = apiInterface.Datacard(type);
        call.enqueue(new Callback<PrepaidResponse>() {
            @Override
            public void onResponse(Call<PrepaidResponse> call, Response<PrepaidResponse> response) {

                if (response.isSuccessful()) ;

                PrepaidResponse prepaidResponse=response.body();

                PrepaidResponse.StatusBean statusBean=prepaidResponse.getStatus();
                if (statusBean.getCode() == 200){
                    progressDialog.dismiss();

                    List<PrepaidResponse.DataBean> dataBeanList=prepaidResponse.getData();



                    for (int i = 0; i <= dataBeanList.size(); i++) {


                        if (dataBeanList.get(i).getOperator_code().equals(Operator_Code)) {


                            type1.setText(dataBeanList.get(i).getOperator_name());
                            otype.setText(dataBeanList.get(i).getOperator_type());
                            Type_Id = dataBeanList.get(i).getOperator_type();
                            recharge_type = dataBeanList.get(i).getOperator_type();


                            Type = type1.getText().toString();
                            Circle = state.getText().toString();

                            operator = dataBeanList.get(i).getOperator_code() + "@" + dataBeanList.get(i).getOperator_name();

                            break;
                        }


                    }

                }
                else if (statusBean.getCode() == 400){
                    progressDialog.dismiss();
                    Toast.makeText(ProceedToRechargeDataCard.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();
                }



                if (response.code() == 200) {

                    progressDialog.dismiss();



                } else if (response.code() == 401) {
                    progressDialog.dismiss();

                } else if(response.code() ==400) {
                    progressDialog.dismiss();
                    Toast.makeText(ProceedToRechargeDataCard.this, "respond", Toast.LENGTH_SHORT).show();

                }
                else if(response.code() == 500){
                    progressDialog.dismiss();
                    Toast.makeText(ProceedToRechargeDataCard.this, "true", Toast.LENGTH_SHORT).show();


                }

            }


            @Override
            public void onFailure(Call<PrepaidResponse> call, Throwable t) {
                progressDialog.dismiss();
                //Log.e(TAG, "failed"+t.getMessage() );
                Toast.makeText(ProceedToRechargeDataCard.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });




















      /*  final ProgressDialog progressDialog = new ProgressDialog(ProceedToRechargeDataCard.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Operators> call = apiInterface.operators();
        call.enqueue(new Callback<Operators>() {
            @Override
            public void onResponse(Call<Operators> call, Response<Operators> response) {
                // if (response.isSuccessful()) ;
                Operators walletResponse1 = response.body();
                if (walletResponse1 != null) {


                    StatusOperator statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog.dismiss();


                        List<DataOperator> data7 = walletResponse1.data;

                        for (int i = 0; i <= data7.size(); i++) {


                            if (data7.get(i).operatorCode.equals(Operator_Code)) {


                                type.setText(data7.get(i).operatorName);
                                otype.setText(data7.get(i).operatorType);
                                Type_Id = data7.get(i).operatorType;
                                recharge_type = data7.get(i).operatorType;

                                Picasso.get().load(data7.get(i).image).error(R.drawable.kb).into(imgnet);

                                Type = type.getText().toString();
                                Circle = state.getText().toString();

                                operator = data7.get(i).operatorCode + "@" + data7.get(i).operatorName;

                                break;
                            }


                        }

                    } else if (statusDataResponse7.code != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(ProceedToRechargeDataCard.this, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(ProceedToRechargeDataCard.this, "No Data", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<Operators> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast = Toast.makeText(ProceedToRechargeDataCard.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });*/


        final ProgressDialog progressDialog1 = new ProgressDialog(ProceedToRechargeDataCard.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Circles> call1 = apiInterface.circles();
        call1.enqueue(new Callback<Circles>() {
            @Override
            public void onResponse(Call<Circles> call, Response<Circles> response) {
                // if (response.isSuccessful()) ;
                Circles walletResponse1 = response.body();
                if (walletResponse1 != null) {


                    StatusCircle statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog1.dismiss();


                        List<DataCircle> data7 = walletResponse1.data;

                        for (int i = 0; i <= data7.size(); i++) {

                            String circlecode = data7.get(i).circleCode;

                            circle_code = circlecode + "@" + data7.get(i).circleName;


                            if (circlecode.equals(Circle_Code)) {

                                state.setText(data7.get(i).circleName);

                                break;
                            }


                        }

                    } else if (statusDataResponse7.code != 200) {
                        progressDialog1.dismiss();
                        Toast.makeText(ProceedToRechargeDataCard.this, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                } else {
                    progressDialog1.dismiss();
                    Toast.makeText(ProceedToRechargeDataCard.this, "No Data", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<Circles> call, Throwable t) {
                progressDialog1.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast = Toast.makeText(ProceedToRechargeDataCard.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });


        changeoperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getprepaid(Typee);
                Intent intent = new Intent(ProceedToRechargeDataCard.this, OperatorActivityDataCard.class);
                intent.putExtra("TYPEE", Typee);
                intent.putExtra("Mobile", MobileNumber);

                startActivity(intent);
            }
        });
        browseplans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(ProceedToRechargeDataCard.this, BrowsePlansDataCard.class);
                intent.putExtra("Operator", Operator_Code);
                intent.putExtra("Circle", Circle_Code);
                intent.putExtra("Mobile", MobileNumber);
                intent.putExtra("Type", Type);
                intent.putExtra("State", Circle);

                startActivity(intent);
            }
        });

        back5 = findViewById(R.id.back5);
        back5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(ProceedToRecharge.this,BrowsePlans.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);*/

                finish();
            }
        });


        proceedtorecharge = findViewById(R.id.proceedtorecharge);
        proceedtorecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount = editamount.getText().toString();

                if (amount.equals("")) {

                    Toast.makeText(ProceedToRechargeDataCard.this, "Please Enter Amount", Toast.LENGTH_SHORT).show();
                } else {

                    Intent intent = new Intent(ProceedToRechargeDataCard.this, PaymentDatacard.class);
                    //intent.putExtra("Amount",AMount);
                    intent.putExtra("Amount", amount);
                    intent.putExtra("Operator", operator);
                    intent.putExtra("CircleCode", circle_code);
                    intent.putExtra("RechargeType", recharge_type);
                    intent.putExtra("MobileNumber", MobileNumber);

                    startActivity(intent);


                }

               /* BottomSheetDialog dialog = new BottomSheetDialog(ProceedToRecharge.this);
                dialog.setContentView(R.layout.activity_alert_dialog);
                //dialog.show();
                new CustomBottomSheetDialogFragment2().show(getSupportFragmentManager(), "Dialog");*/
            }
        });

    }


}
