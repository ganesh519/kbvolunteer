package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class FlightDetails extends BaseActivity {

    LinearLayout spicejet;
    ImageView backblack4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_details);

        backblack4=findViewById(R.id.backblack4);
        backblack4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(FlightDetails.this,FlightFare.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        spicejet=findViewById(R.id.spicejet);
        spicejet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialog dialog = new BottomSheetDialog(FlightDetails.this);
                dialog.setContentView(R.layout.activity_searchflights);
                //dialog.show();
                new CustomBottomSheetDialogFragment9().show(getSupportFragmentManager(), "Dialog");
            }
        });
    }
}
