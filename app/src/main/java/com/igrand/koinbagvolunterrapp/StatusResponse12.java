package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class StatusResponse12 {


    @SerializedName("status")
    @Expose
    public StatusDataResponse12 status;
    @SerializedName("data")
    @Expose
    public List<Data12> data = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }

}
