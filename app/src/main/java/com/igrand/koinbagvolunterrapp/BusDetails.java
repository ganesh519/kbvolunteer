package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class BusDetails extends BaseActivity {

    CardView cardviewbus;
    TextView more,hydmum,busadult;
    ImageView backblack11;
    RecyclerView recyclerView;
    Typeface typeface;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_details);


        recyclerView= findViewById(R.id.recyler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerAdapter adapter=new RecyclerAdapter(BusDetails.this);
        recyclerView.setAdapter(adapter);

        backblack11=findViewById(R.id.backblack11);
        hydmum=findViewById(R.id.hydmum);
        busadult=findViewById(R.id.busadult);

        typeface=Typeface.createFromAsset(getAssets(),"font/Avenir-Medium.ttf");
        hydmum.setTypeface(typeface);
        busadult.setTypeface(typeface);

        backblack11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BusDetails.this,Flight.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
