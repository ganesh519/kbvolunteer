package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.HashMap;

public class BroadBand extends BaseActivity {

    EditText selectstate,selectboard,consumernumber;
    String Operator_Code,Circle_Code,id,operator,amount,circle_code,recharge_type,mobile_number;
    PrefManager prefManager;
    Button broadbandbutton;
    ImageView backelec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broad_band);

        selectstate=findViewById(R.id.selectstate);
        selectboard=findViewById(R.id.selectboard);
        consumernumber=findViewById(R.id.consumernumber);
        broadbandbutton=findViewById(R.id.broadbandbutton);
        backelec=findViewById(R.id.backelec);


        backelec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        prefManager = new PrefManager(BroadBand.this);
        HashMap<String, String> profile = prefManager.getUserDetails();

        Operator_Code = getIntent().getStringExtra("OperatorCode");
        Circle_Code = getIntent().getStringExtra("OperatorName");



        selectstate.setText(Circle_Code);


        id = profile.get("id");
        operator=Operator_Code + "@" + Circle_Code;
        circle_code="1@Andhra Pradesh";
        recharge_type="landline";



        if(selectstate.getText().toString().isEmpty()) {

            selectboard.setVisibility(View.GONE);
            consumernumber.setVisibility(View.GONE);
            selectstate.setGravity(Gravity.CENTER_VERTICAL );

        } else {
            selectboard.setVisibility(View.VISIBLE);
            consumernumber.setVisibility(View.VISIBLE);
        }



        selectstate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_UP){
                    Intent intent=new Intent(BroadBand.this,SelectOperatorBroadBand.class);
                    startActivity(intent);
                    // Do what you want
                    return true;
                }

                return false;
            }
        });



        broadbandbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount=consumernumber.getText().toString();
                mobile_number=selectboard.getText().toString();
                Intent intent=new Intent(BroadBand.this,PaymentBroadBand.class);
                intent.putExtra("Amount",amount);
                intent.putExtra("Operator",operator);
                intent.putExtra("CircleCode",circle_code);
                intent.putExtra("RechargeType",recharge_type);
                intent.putExtra("MobileNumber",mobile_number);
                startActivity(intent);
            }
        });

    }
}
