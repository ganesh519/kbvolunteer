package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data9 {

    @SerializedName("wallet_bal")
    @Expose
    public String walletBal;
    @SerializedName("transaction")
    @Expose
    public String transaction;
    @SerializedName("commission")
    @Expose
    public String commission;
    @SerializedName("notifications")
    @Expose
    public String notifications;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("walletBal", walletBal).append("transaction", transaction).append("commission", commission).append("notifications", notifications).toString();
    }
}