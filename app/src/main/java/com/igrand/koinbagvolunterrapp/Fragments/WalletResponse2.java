package com.igrand.koinbagvolunterrapp.Fragments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.igrand.koinbagvolunterrapp.StatusDataResponse7;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class WalletResponse2 {


    @SerializedName("status")
    @Expose
    public StatusDataResponse15 status;
    @SerializedName("data")
    @Expose
    public Data15 data;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }
}
