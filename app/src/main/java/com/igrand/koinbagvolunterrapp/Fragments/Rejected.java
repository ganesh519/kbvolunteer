package com.igrand.koinbagvolunterrapp.Fragments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Rejected {

    @SerializedName("request_id")
    @Expose
    public String requestId;
    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("requested_date")
    @Expose
    public String requestedDate;
    @SerializedName("updated_date")
    @Expose
    public String updatedDate;
    @SerializedName("status")
    @Expose
    public String status;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("requestId", requestId).append("amount", amount).append("requestedDate", requestedDate).append("updatedDate", updatedDate).append("status", status).toString();
    }

}
