package com.igrand.koinbagvolunterrapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.airbnb.lottie.LottieAnimationView;

public class SplashLogin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_login);

        final LottieAnimationView lottieAnimationView=(LottieAnimationView)findViewById(R.id.lottieanimation);
        lottieAnimationView.setScale(0.2f);
        lottieAnimationView.setAnimation("loadin1.json");


    }
}
