package com.igrand.koinbagvolunterrapp.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataResponseMobile {

    @SerializedName("International Roaming")
    @Expose
    private List<InternationalRoaming> internationalRoaming = null;
    @SerializedName("Talktime")
    @Expose
    private List<Talktime> talktime = null;
    @SerializedName("Unlimited Packs")
    @Expose
    private List<UnlimitedPack> unlimitedPacks = null;
    @SerializedName("Smart Recharge")
    @Expose
    private List<SmartRecharge> smartRecharge = null;
    @SerializedName("ISD Pack")
    @Expose
    private List<ISDPack> iSDPack = null;
    @SerializedName("Data")
    @Expose
    private List<Datum> data = null;

    public List<InternationalRoaming> getInternationalRoaming() {
        return internationalRoaming;
    }

    public void setInternationalRoaming(List<InternationalRoaming> internationalRoaming) {
        this.internationalRoaming = internationalRoaming;
    }

    public List<Talktime> getTalktime() {
        return talktime;
    }

    public void setTalktime(List<Talktime> talktime) {
        this.talktime = talktime;
    }

    public List<UnlimitedPack> getUnlimitedPacks() {
        return unlimitedPacks;
    }

    public void setUnlimitedPacks(List<UnlimitedPack> unlimitedPacks) {
        this.unlimitedPacks = unlimitedPacks;
    }

    public List<SmartRecharge> getSmartRecharge() {
        return smartRecharge;
    }

    public void setSmartRecharge(List<SmartRecharge> smartRecharge) {
        this.smartRecharge = smartRecharge;
    }

    public List<ISDPack> getISDPack() {
        return iSDPack;
    }

    public void setISDPack(List<ISDPack> iSDPack) {
        this.iSDPack = iSDPack;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
}
