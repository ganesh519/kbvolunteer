package com.igrand.koinbagvolunterrapp.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.igrand.koinbagvolunterrapp.HelpFragment;
import com.igrand.koinbagvolunterrapp.OffersFragment;
import com.igrand.koinbagvolunterrapp.RecentFragment;

public class MyAdapter5 extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;
    public MyAdapter5(Context context, FragmentManager supportFragmentManager, int tabCount) {

        super(supportFragmentManager);
        myContext = context;
        this.totalTabs = tabCount;
    }

    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                RecentFragment recentFragment = new RecentFragment();
                return recentFragment;
            case 1:
                OffersFragment offersFragment = new OffersFragment();
                return offersFragment;
            case 2:
                HelpFragment helpFragment = new HelpFragment();
                return helpFragment;

            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;

    }
}

