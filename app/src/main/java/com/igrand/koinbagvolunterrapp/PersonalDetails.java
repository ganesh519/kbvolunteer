package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class PersonalDetails extends AppCompatActivity {

    ImageView back77,photo1,aadharphoto1,panphoto1,tenth1,inter1,inter0011;
    TextView titleprofile1,lastname1,surname1,middlename1,dob1,mobilenumber1,email1,state1,revenuedivision1,district1,mandal1,
    village1,addressline11,addressline21,addressline31,pancard1,aadharcard1,name1,userid1,emailid1,mobilenumber3,
    tenthmarks1,intermarks1,degreemarks1,contactname1,contacttype1,contactnumber1,relation1,emailid;

    String Id,f_name,surname,email,mobile,walletbals,userid11,Ngo_Name,Photo;
    PrefManager prefManager;
    LinearLayout linear0001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_details);
        back77=findViewById(R.id.back77);



        aadharphoto1=findViewById(R.id.aadharphoto);
        emailid=findViewById(R.id.emailid);
        inter0011=findViewById(R.id.inter0011);
        name1=findViewById(R.id.name);
        userid1=findViewById(R.id.userid);
        emailid1=findViewById(R.id.email);
        panphoto1=findViewById(R.id.panphoto);
        photo1=findViewById(R.id.photo);
        titleprofile1=findViewById(R.id.titleprofile);
        lastname1=findViewById(R.id.lastname);
        surname1=findViewById(R.id.surname);
        middlename1=findViewById(R.id.middlename);
        dob1=findViewById(R.id.dob);
        mobilenumber1=findViewById(R.id.mobilenumber);
        email1=findViewById(R.id.email);
        state1=findViewById(R.id.state);
        revenuedivision1=findViewById(R.id.revenuedivision);
        district1=findViewById(R.id.district);
        mandal1=findViewById(R.id.mandal);
        village1=findViewById(R.id.village);
        addressline11=findViewById(R.id.addressline1);
        addressline21=findViewById(R.id.addressline2);
        addressline31=findViewById(R.id.addressline3);
        pancard1=findViewById(R.id.pancard);
        aadharcard1=findViewById(R.id.aadharcard);
        tenth1=findViewById(R.id.tenth);
        inter1=findViewById(R.id.inter);
        mobilenumber3=findViewById(R.id.mobilenumber3);
        linear0001=findViewById(R.id.linear0001);


        tenthmarks1=findViewById(R.id.tenthmarks);
        intermarks1=findViewById(R.id.intermarks);
        degreemarks1=findViewById(R.id.degreemarks);
        contactname1=findViewById(R.id.contactname);
        contactnumber1=findViewById(R.id.contactnumber);
        contacttype1=findViewById(R.id.contacttype);
        relation1=findViewById(R.id.relation);


        linear0001.setVisibility(View.VISIBLE);


        final String title=getIntent().getStringExtra("Title");
        final String surName=getIntent().getStringExtra("SurName");
        final String lastName=getIntent().getStringExtra("LastName");
        final String middleName=getIntent().getStringExtra("MiddleName");
        final String email=getIntent().getStringExtra("Email");
        final String photo=getIntent().getStringExtra("Photo");
        final String mobileNumber=getIntent().getStringExtra("MobileNumber");
        final String dateOfBirth=getIntent().getStringExtra("DateOfBirth");
        final String state=getIntent().getStringExtra("State");
        final String district=getIntent().getStringExtra("District");
        final String revenueDevision=getIntent().getStringExtra("RevenueDevision");
        final String mandal=getIntent().getStringExtra("Mandal");
        final String village=getIntent().getStringExtra("Village");
        final String addressLine1=getIntent().getStringExtra("AddressLine1");
        final String addressLine2=getIntent().getStringExtra("AddressLine2");
        final String addressLine3=getIntent().getStringExtra("AddressLine3");
        final String pancard=getIntent().getStringExtra("Pancard");
        final String panPhoto=getIntent().getStringExtra("PanPhoto");
        final String aadharCard=getIntent().getStringExtra("AadharCard");
        final String aadharPhoto=getIntent().getStringExtra("AadharPhoto");
        final String tenth=getIntent().getStringExtra("Tenth");
        final String inter=getIntent().getStringExtra("Inter");
        final String userid=getIntent().getStringExtra("UserId");
        final String first_name=getIntent().getStringExtra("FirstName");


        final String tenthmarks=getIntent().getStringExtra("TenthMarks");
        final String intermarks=getIntent().getStringExtra("InterMarks");
        final String degreemarks=getIntent().getStringExtra("DegreeMarks");
        final String contactname=getIntent().getStringExtra("ContactName");
        final String contacttype=getIntent().getStringExtra("ContactType");
        final String contactnumber=getIntent().getStringExtra("ContactNumber");
        final String relation=getIntent().getStringExtra("Relation");

        prefManager=new PrefManager(getApplicationContext());
        HashMap<String, String> profile=prefManager.getUserDetails();
        Id=profile.get("id");
        f_name=profile.get("f_name");
        surname=profile.get("Surname");
        mobile=profile.get("Mobile");
        userid11=profile.get("Userid");
        walletbals=profile.get("Walletbals");
        Ngo_Name=profile.get("NgoName");
        Photo=profile.get("Photo");


        Picasso.get().load(Photo).error(R.drawable.kb).into(photo1);
        photo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photo1.buildDrawingCache();
                Bitmap bitmap = photo1.getDrawingCache();
                Intent intent=new Intent(PersonalDetails.this,ProfileImage.class);

                ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                intent.putExtra("byteArray", _bs.toByteArray());
                startActivity(intent);
            }
        });



        tenthmarks1.setText(tenthmarks);
        intermarks1.setText(intermarks);
        degreemarks1.setText(degreemarks);
        contactname1.setText(contactname);
        contactnumber1.setText(contactnumber);
        // photo1.conta(Integer.parseInt(photo));
        contacttype1.setText(contacttype);
        relation1.setText(relation);




        userid1.setText(userid11);
        name1.setText(first_name);
        email1.setText(email);
        emailid.setText(email);
        titleprofile1.setText(title);
        surname1.setText(surName);
       // photo1.setImageResource(Integer.parseInt(photo));
        lastname1.setText(first_name);
        middlename1.setText(middleName);
        email1.setText(email);
        mobilenumber1.setText(mobileNumber);
        dob1.setText(dateOfBirth);
        state1.setText(state);
        district1.setText(district);
        revenuedivision1.setText(revenueDevision);
        mandal1.setText(mandal);
        village1.setText(village);
        addressline11.setText(addressLine1);
        addressline21.setText(addressLine2);
        addressline31.setText(addressLine3);
        pancard1.setText(pancard);
        mobilenumber3.setText(mobileNumber);
        aadharcard1.setText(aadharCard);





        Picasso.get().load(panPhoto).error(R.drawable.kb).into(panphoto1);
        Picasso.get().load(aadharPhoto).error(R.drawable.kb).into(aadharphoto1);
        Picasso.get().load(tenth).error(R.drawable.kb).into(tenth1);
        Picasso.get().load(inter).error(R.drawable.kb).into(inter1);



                panphoto1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        panphoto1.buildDrawingCache();
                        Bitmap bitmap = panphoto1.getDrawingCache();
                        Intent intent=new Intent(PersonalDetails.this, ProfileImage.class);

                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        startActivity(intent);

                    }
                });



        inter0011.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                inter0011.buildDrawingCache();
                Bitmap bitmap = inter0011.getDrawingCache();
                Intent intent=new Intent(PersonalDetails.this, ProfileImage.class);

                ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                intent.putExtra("byteArray", _bs.toByteArray());
                startActivity(intent);

            }
        });


                aadharphoto1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        aadharphoto1.buildDrawingCache();
                        Bitmap bitmap = aadharphoto1.getDrawingCache();
                        Intent intent=new Intent(PersonalDetails.this, ProfileImage.class);

                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        startActivity(intent);

                    }
                });




                tenth1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        tenth1.buildDrawingCache();
                        Bitmap bitmap = tenth1.getDrawingCache();
                        Intent intent=new Intent(PersonalDetails.this, ProfileImage.class);

                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        startActivity(intent);

                    }
                });



                inter1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        inter1.buildDrawingCache();
                        Bitmap bitmap = inter1.getDrawingCache();
                        Intent intent=new Intent(PersonalDetails.this, ProfileImage.class);

                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        startActivity(intent);

            }
        });




        // panphoto1.setImageResource(Integer.parseInt(panPhoto));

//        aadharphoto1.setImageResource(Integer.parseInt(aadharPhoto));
       // tenth1.setImageResource(Integer.parseInt(tenth));
       // inter1.setImageResource(Integer.parseInt(inter));

      /*  new DownloadImageTask((ImageView) findViewById(R.id.photo))
                .execute("http://igranddeveloper.live/kbig/uploads/download.jpg");

        new DownloadImageTask((ImageView)findViewById(R.id.panphoto)).execute("http://igranddeveloper.live/kbig/uploads/download.jpg");

        new DownloadImageTask((ImageView)findViewById(R.id.aadharphoto)).execute("http://igranddeveloper.live/kbig/uploads/images.jpg");

        new DownloadImageTask((ImageView)findViewById(R.id.tenth)).execute("http://igranddeveloper.live/kbig/uploads/download (1).jpg");

        new DownloadImageTask((ImageView)findViewById(R.id.inter)).execute("http://igranddeveloper.live/kbig/uploads/download (1).jpg");

*/



        back77.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent returnIntent = new Intent();
                 setResult(RESULT_OK,returnIntent);
                 finish();
                 overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

            }
        });

    }

   /* private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }


*/

/* @Override
    public void onBackPressed() {
        Intent intent=new Intent(PersonalDetails.this,NavigationDrawerDashboard.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        super.onBackPressed();
    }
*/


}
