package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ResultPojo {

    @SerializedName("status")
    @Expose
    public StatusDataResponse status;

    @SerializedName("data")
    @Expose
    public Data data;

    @SerializedName("code")
    @Expose
    public Integer code;
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;



    public StatusDataResponse getStatus() {
        return status;
    }

    public void setStatus(StatusDataResponse status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }




    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("code", code).append("message",message).append("data",data).append("status",status).append("first_name",firstName)
        .append("mobile_number",mobileNumber).toString();
    }
}
