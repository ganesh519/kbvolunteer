package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetPin extends BaseActivity {

    Button save1;
    EditText ed1,ed2;
    ApiInterface apiInterface;
    ImageView backpage000;
    TextInputLayout textInputLayout,textInputLayout1;
    Typeface typeface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pin);


        save1=findViewById(R.id.save11);
        ed1=findViewById(R.id.ed1);
        ed2=findViewById(R.id.ed2);
        backpage000=findViewById(R.id.backpage000);

        textInputLayout=findViewById(R.id.text_input_layout010);
        textInputLayout1=findViewById(R.id.text_input_layout101);

        backpage000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         finish();
            }
        });


        typeface = Typeface.createFromAsset(getAssets(), "font/Avenir-Light.ttf");
        textInputLayout.setTypeface(typeface);
        textInputLayout1.setTypeface(typeface);





                save1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final String mobile_number=getIntent().getStringExtra("Mobile_number");
                        String password=ed1.getText().toString();
                        String confirmPassword=ed2.getText().toString();


                        if (!TextUtils.isEmpty(password) && !TextUtils.isEmpty(confirmPassword))
                        {
                            if(password.equals(confirmPassword))
                            {

                        final ProgressDialog progressDialog=new ProgressDialog(SetPin.this);
                        progressDialog.setMessage("Verifying Details.....");
                        progressDialog.setContentView(R.layout.activity_splash_login);
                        progressDialog.show();
                        apiInterface= ApiClient.getClient().create(ApiInterface.class);
                        Call<StatusResponse4> call=apiInterface.Statuss4(mobile_number,password);
                        call.enqueue(new Callback<StatusResponse4>() {
                            @Override
                            public void onResponse(Call<StatusResponse4> call, Response<StatusResponse4> response) {
                                if (response.isSuccessful()) ;

                                StatusResponse4 statusResponse = response.body();
                                StatusDataResponse4 statusDataResponse=statusResponse.status;

                                if (statusDataResponse.code == 200){
                                    progressDialog.dismiss();


                                    Data4 data=statusResponse.data;

                                    String name=data.firstName;

                                    Toast.makeText(SetPin.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(SetPin.this, WelcomeBack.class);
                                    intent.putExtra("Mobile_number",mobile_number);
                                    intent.putExtra("Name1",name);
                                    startActivity(intent);

                                }
                                else if (statusDataResponse.code == 409 ){
                                    progressDialog.dismiss();
                                    Toast.makeText(SetPin.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(Call<StatusResponse4> call, Throwable t) {
                                progressDialog.dismiss();
                                Toast.makeText(SetPin.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                            }
                        });

            }
            else {
                Toast.makeText(SetPin.this, "Please check the Password", Toast.LENGTH_SHORT).show();
            }
        }  else {
                            Toast.makeText(SetPin.this, "Please Set your PIN", Toast.LENGTH_SHORT).show();

                        }
                        }


                });
            }

        }




