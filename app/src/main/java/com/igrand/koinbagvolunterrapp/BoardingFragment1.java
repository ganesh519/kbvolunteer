package com.igrand.koinbagvolunterrapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

@SuppressLint("ValidFragment")
public class BoardingFragment1 extends Fragment {
    public BoardingFragment1() {

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_boarding1, container, false);


        Button selectedseat1=(Button)v.findViewById(R.id.selectedseat1);
        selectedseat1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getActivity(),TravellerInfoBus.class);
                startActivity(intent);
            }
        });


        return v;
    }
}
