package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Activities.AirtelTV;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DTHRecharge extends BaseActivity {

    ImageView airteltv,backdth;
    RecyclerView recycler_dth;
    ApiInterface apiInterface;
    DTHRecyclerAdapter dthRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dthrecharge);


        backdth=findViewById(R.id.backdth);
        recycler_dth=findViewById(R.id.recycler_dth);


        final ProgressDialog progressDialog=new ProgressDialog(DTHRecharge.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();


        String type="dth";
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PrepaidResponse> call = apiInterface.DTH(type);
        call.enqueue(new Callback<PrepaidResponse>() {
            @Override
            public void onResponse(Call<PrepaidResponse> call, Response<PrepaidResponse> response) {

                if (response.isSuccessful()) ;

                PrepaidResponse prepaidResponse=response.body();

                PrepaidResponse.StatusBean statusBean=prepaidResponse.getStatus();
                if (statusBean.getCode() == 200){
                    progressDialog.dismiss();

                    List<PrepaidResponse.DataBean> dataBeanList=prepaidResponse.getData();

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
                    recycler_dth.setLayoutManager(mLayoutManager);
                    dthRecyclerAdapter=new DTHRecyclerAdapter(dataBeanList,DTHRecharge.this);
                    recycler_dth.setAdapter(dthRecyclerAdapter);


                }
                else if (statusBean.getCode() == 400){
                    progressDialog.dismiss();
                    Toast.makeText(DTHRecharge.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();
                }



                if (response.code() == 200) {

                    progressDialog.dismiss();



                } else if (response.code() == 401) {
                    progressDialog.dismiss();

                } else if(response.code() ==400) {
                    progressDialog.dismiss();
                    Toast.makeText(DTHRecharge.this, "respond", Toast.LENGTH_SHORT).show();

                }
                else if(response.code() == 500){
                    progressDialog.dismiss();
                    Toast.makeText(DTHRecharge.this, "true", Toast.LENGTH_SHORT).show();


                }

            }


            @Override
            public void onFailure(Call<PrepaidResponse> call, Throwable t) {
                progressDialog.dismiss();
                //Log.e(TAG, "failed"+t.getMessage() );
                Toast.makeText(DTHRecharge.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });






        backdth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
