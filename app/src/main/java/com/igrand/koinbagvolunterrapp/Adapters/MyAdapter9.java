package com.igrand.koinbagvolunterrapp.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.igrand.koinbagvolunterrapp.BoardingFragment;
import com.igrand.koinbagvolunterrapp.BoardingFragment1;
import com.igrand.koinbagvolunterrapp.CancellationPolcyFragment;
import com.igrand.koinbagvolunterrapp.DropFragment;

public class MyAdapter9 extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;
    public MyAdapter9(Context context, FragmentManager supportFragmentManager, int tabCount) {

        super(supportFragmentManager);
        myContext = context;
        this.totalTabs = tabCount;
    }

    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                BoardingFragment1 boardingFragment1 = new BoardingFragment1();
                return boardingFragment1;
            case 1:
                DropFragment dropFragment = new DropFragment();
                return dropFragment;
            case 2:
                CancellationPolcyFragment cancellationPolcyFragment = new CancellationPolcyFragment();
                return cancellationPolcyFragment;

            case 3:
                BoardingFragment boardingFragment = new BoardingFragment();
                return boardingFragment;

            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;

    }
}

