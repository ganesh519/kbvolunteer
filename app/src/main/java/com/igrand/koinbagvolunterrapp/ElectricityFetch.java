package com.igrand.koinbagvolunterrapp;

public class ElectricityFetch {


    /**
     * ErrorCode : 200
     * API_Message : Biller detail find sucsesfully
     * API_Status : Success
     * RequestId : 180405
     * CustomerId : N/A
     * Customer_Name : RAVI  VENU MADHAV
     * Discome : N/A
     * BillDueDate : 22/10/2019
     * BillAmount : 202.00
     * Message : Success
     * Status : N/A
     * Address : H/81,MADHURA NAGAR,
     * BP_Number : N/A
     * Invoice_Number : N/A
     * Invoice_Date : N/A
     */

    private int ErrorCode;
    private String API_Message;
    private String API_Status;
    private String RequestId;
    private String CustomerId;
    private String Customer_Name;
    private String Discome;
    private String BillDueDate;
    private String BillAmount;
    private String Message;
    private String Status;
    private String Address;
    private String BP_Number;
    private String Invoice_Number;
    private String Invoice_Date;

    public int getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(int ErrorCode) {
        this.ErrorCode = ErrorCode;
    }

    public String getAPI_Message() {
        return API_Message;
    }

    public void setAPI_Message(String API_Message) {
        this.API_Message = API_Message;
    }

    public String getAPI_Status() {
        return API_Status;
    }

    public void setAPI_Status(String API_Status) {
        this.API_Status = API_Status;
    }

    public String getRequestId() {
        return RequestId;
    }

    public void setRequestId(String RequestId) {
        this.RequestId = RequestId;
    }

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String CustomerId) {
        this.CustomerId = CustomerId;
    }

    public String getCustomer_Name() {
        return Customer_Name;
    }

    public void setCustomer_Name(String Customer_Name) {
        this.Customer_Name = Customer_Name;
    }

    public String getDiscome() {
        return Discome;
    }

    public void setDiscome(String Discome) {
        this.Discome = Discome;
    }

    public String getBillDueDate() {
        return BillDueDate;
    }

    public void setBillDueDate(String BillDueDate) {
        this.BillDueDate = BillDueDate;
    }

    public String getBillAmount() {
        return BillAmount;
    }

    public void setBillAmount(String BillAmount) {
        this.BillAmount = BillAmount;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getBP_Number() {
        return BP_Number;
    }

    public void setBP_Number(String BP_Number) {
        this.BP_Number = BP_Number;
    }

    public String getInvoice_Number() {
        return Invoice_Number;
    }

    public void setInvoice_Number(String Invoice_Number) {
        this.Invoice_Number = Invoice_Number;
    }

    public String getInvoice_Date() {
        return Invoice_Date;
    }

    public void setInvoice_Date(String Invoice_Date) {
        this.Invoice_Date = Invoice_Date;
    }
}
