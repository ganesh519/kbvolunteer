package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Adapters.CirclesRecyclerAdapter;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomBottomSheetDialogFragment4 extends BottomSheetDialogFragment {

    ApiInterface apiInterface;
    RecyclerView recycler_circle;
    CirclesRecyclerAdapterelectricity circlesRecyclerAdapter;
    ImageView exit9;
    FrameLayout frame3;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.activity_select_circle1, container, false);


        recycler_circle=v.findViewById(R.id.recycler_circle);
        exit9=v.findViewById(R.id.exit9);
        frame3=v.findViewById(R.id.frame3);


        final ProgressDialog progressDialog1 = new ProgressDialog(getContext());
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Circles> call1 = apiInterface.circles();
        call1.enqueue(new Callback<Circles>() {
            @Override
            public void onResponse(Call<Circles> call, Response<Circles> response) {
                if (response.isSuccessful()) ;
                Circles walletResponse1 = response.body();
                if (walletResponse1 != null) {


                    StatusCircle  statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog1.dismiss();

                        frame3.setVisibility(View.VISIBLE);
                        List<DataCircle> data7 = walletResponse1.data;


                        recycler_circle.setLayoutManager(new LinearLayoutManager(getContext()));
                        circlesRecyclerAdapter=new CirclesRecyclerAdapterelectricity(data7,getActivity());
                        recycler_circle.setAdapter(circlesRecyclerAdapter);

                    } else if (statusDataResponse7.code != 200) {
                       // progressDialog1.dismiss();
                        Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                }
                else {
                    //progressDialog1.dismiss();
                    Toast.makeText(getContext(), "No Data", Toast.LENGTH_SHORT).show();
                }
            }



            @Override
            public void onFailure(Call<Circles> call, Throwable t) {
               // progressDialog1.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(getContext(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });

        exit9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return v;
    }

}
