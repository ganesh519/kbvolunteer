package com.igrand.koinbagvolunterrapp.Fragments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FailureRecyclerAdapter extends RecyclerView.Adapter<FailureRecyclerAdapter.Holder> {

    List<Failure> failures;
    Context context;
    ApiInterface apiInterface;
    public FailureRecyclerAdapter(Context context, List<Failure> failures) {

        this.context=context;
        this.failures=failures;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview9, viewGroup, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        String upperString = failures.get(i).apiType.substring(0,1).toUpperCase() + failures.get(i).apiType.substring(1);

        holder.api_type.setText(upperString);
        holder.digitalnumber.setText(failures.get(i).digitalNumber);
        holder.prevbal.setText(failures.get(i).prevBal);
        holder.curbal.setText(failures.get(i).curBal);
        holder.createdat.setText(failures.get(i).createdAt);
        holder.amount.setText(failures.get(i).amount);
        Picasso.get().load(failures.get(i).operatorimage).error(R.drawable.kb).into(holder.jio);
        //holder.scomrs1.setText(failures.get(i).commission);

    }

    @Override
    public int getItemCount() {
        return failures.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView api_type,digitalnumber,prevbal,curbal,createdat,status,amount,scomrs1;
        ImageView image,jio;
        public Holder(@NonNull View itemView) {
            super(itemView);

            api_type=itemView.findViewById(R.id.papi_type110);
            digitalnumber=itemView.findViewById(R.id.pdigitalnumber110);
            prevbal=itemView.findViewById(R.id.pprevbal110);
            curbal=itemView.findViewById(R.id.pcurbal110);
            createdat=itemView.findViewById(R.id.pcreated_at110);
            amount=itemView.findViewById(R.id.pdrs110);
            jio=itemView.findViewById(R.id.jio);
            //scomrs1=itemView.findViewById(R.id.pscom110);
        }
    }
}
