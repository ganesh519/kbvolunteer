package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.lang.reflect.Array;

public class OperatorFetch {


    @SerializedName("Status")
    @Expose
    public String status;
    @SerializedName("OperatorCode")
    @Expose
    public String operatorCode;
    @SerializedName("CircleCode")
    @Expose
    public String circleCode;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("operatorCode", operatorCode).append("circleCode", circleCode).toString();
    }
}
