package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.Adapters.MyAdapter1;
import com.igrand.koinbagvolunterrapp.Fragments.AllFragment1;

import java.util.ArrayList;
import java.util.List;

public class DigitalTransactions1 extends BaseActivity {

    TabLayout tabLayout1;
    ViewPager viewPager1;
    ImageView back00;
    TextView textviewdigital;
    Typeface typeface;
    ImageView toggledigital;
    ViewPagerAdapterNotifications pagerAdapterNotifications;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital_transactions1);



        tabLayout1 = (TabLayout)findViewById(R.id.tabLayout111);
        viewPager1 = (ViewPager)findViewById(R.id.viewPager111);
        back00=findViewById(R.id.back00);
        back00.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        tabLayout1.setSelectedTabIndicatorColor(Color.parseColor("#FFD71B"));
        tabLayout1.setTabTextColors(Color.parseColor("#80F8F8F8"), Color.parseColor("#FFD71B"));


        pagerAdapterNotifications = new ViewPagerAdapterNotifications(getSupportFragmentManager());
        pagerAdapterNotifications.addFragment(new AllFragment1(), "All");
        pagerAdapterNotifications.addFragment(new SuccessFragment(), "Success");
        pagerAdapterNotifications.addFragment(new ProcessingFragment(), "Pending");
        pagerAdapterNotifications.addFragment(new FailedFragment(), "Failed");

        viewPager1.setAdapter(pagerAdapterNotifications);
        tabLayout1.setupWithViewPager(viewPager1);



    }



    public class ViewPagerAdapterNotifications extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapterNotifications(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

    }

}










