package com.igrand.koinbagvolunterrapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

@SuppressLint("ValidFragment")
public class BestOffersFragment1 extends Fragment {
    public BestOffersFragment1() {

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_bestoffers1, container, false);


        TextView viewdetails=(TextView)v.findViewById(R.id.viewdetails);
        viewdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent=new Intent(getActivity(),ViewDetails.class);
                startActivity(intent);*/

               /* Fragment planDetails = new ViewDetailsFragment();
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.frame2, planDetails);
                transaction.addToBackStack(null);
                transaction.commit();
*/


                BottomSheetDialog dialog = new BottomSheetDialog(getContext());
                dialog.setContentView(R.layout.fragment_view_details);
                //dialog.show();
                new CustomBottomSheetDialogFragment3().show(getActivity().getSupportFragmentManager(), "Dialog");
                dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);

            }
        });

        return v;
    }
}
