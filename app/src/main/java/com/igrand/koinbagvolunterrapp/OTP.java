package com.igrand.koinbagvolunterrapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTP extends BaseActivity {

    EditText editText1, editText2, editText3, editText4, editText5, editText6;
    Button proceed3;
    ApiInterface apiInterface;
    LinearLayout otp1;
    String text;
    boolean delete = false;
    ImageView back1122;
    TextView resendotp1, name002;
    LoginResponse2.StatusBean2 statusBean1;
    private boolean isPaused = false;
    private boolean isCanceled = false;
    BroadcastReceiver receiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        editText1 = findViewById(R.id.editText1);
        /*editText2=findViewById(R.id.editText2);
        editText3=findViewById(R.id.editText3);
        editText4=findViewById(R.id.editText4);
        editText5=findViewById(R.id.editText5);
        editText6=findViewById(R.id.editText6);
        otp1=findViewById(R.id.otp);*/


          /*receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");
                    editText1.setText(message);
                    //Do whatever you want with the code here
                }
            }
        };*/


        back1122 = findViewById(R.id.backpage11212);
        resendotp1 = findViewById(R.id.resendotp1);
        name002 = findViewById(R.id.name002);

        final String mobile_number = getIntent().getStringExtra("Mobile_number");
        final String name = getIntent().getStringExtra("Name1");
        final String surName1 = getIntent().getStringExtra("SurName1");

        name002.setText(name);

        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String value = String.valueOf(s.length());

                if (value.equals("6")) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//Hide:
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        resendotp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new CountDownTimer(30000, 1000) {


                    public void onTick(long millisUntilFinished) {

                        resendotp1.setText("Resend OTP: " + millisUntilFinished / 1000);
                        resendotp1.setEnabled(false);

                    }

                    public void onFinish() {
                        resendotp1.setText("Re-send OTP?");
                        resendotp1.setEnabled(true);
                    }


                }.start();


                final String mobile_number = getIntent().getStringExtra("Mobile_number");

                final ProgressDialog progressDialog = new ProgressDialog(OTP.this);
                progressDialog.setMessage("Verifying Details.....");
                progressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<LoginResponse2> call = apiInterface.Statuss2(mobile_number);
                call.enqueue(new Callback<LoginResponse2>() {
                    @Override
                    public void onResponse(Call<LoginResponse2> call, Response<LoginResponse2> response) {


                        if (response.code() == 200) {

                            progressDialog.dismiss();
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            LoginResponse2.DataBean2 dataBean = response.body().getData();
                            //Integer otp = dataBean.otp;

                            //Toast.makeText(OTP.this, statusBean1.getMessage(), Toast.LENGTH_SHORT).show();
                            /*Intent intent = new Intent(OTP.this, OTP1.class);
                            intent.putExtra("Mobile_number", mobile_number);
                            intent.putExtra("OTP", otp);
                            startActivity(intent);*/

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(OTP.this, "Please Check the OTP", Toast.LENGTH_SHORT).show();

                        }

                    }





                        /*if (response.isSuccessful()) ;
                        LoginResponse2 statusResponse = response.body();
                        StatusDataResponse2 statusDataResponse = statusResponse.status;

                        if (statusDataResponse.code == 200) {
                            progressDialog.dismiss();


                            Data2 data = statusResponse.data;

                            Integer otp = data.otp;

                            Toast.makeText(OTP.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                           *//* Intent intent = new Intent(OTP1.this, OTP1.class);
                            intent.putExtra("Mobile_number", mobile_number);
                            intent.putExtra("OTP", otp);
                            startActivity(intent);*//*

                        } else if (statusDataResponse.code == 409) {
                            progressDialog.dismiss();
                            Toast.makeText(OTP.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                        }

                    }*/

                    @Override
                    public void onFailure(Call<LoginResponse2> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(OTP.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });


            }
        });



        /*resendotp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendotp1.setEnabled(false);

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // This method will be executed once the timer is over
                        resendotp1.setEnabled(true);

                    }
                },30000);
                new CountDownTimer(30000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                    }
                }.onTick(3000);// set time as per your requirement
            }
        });
*/










/*

        text = editText1.getText().toString();

        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                text = editText1.getText().toString();
                if (count > after)
                    delete = true;

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

               */
/* StringBuilder sb = new StringBuilder(s.toString());
                int replacePosition = editText1.getSelectionEnd();

                if (s.length() != 6) { //where 6 is the character underline per text
                    if (!delete) {
                        if (replacePosition < s.length())
                            sb.deleteCharAt(replacePosition);
                    } else {
                        sb.insert(replacePosition, '_');
                    }

                    if (replacePosition < s.length() || delete) {
                        editText1.setText(sb.toString());
                        editText1.setSelection(replacePosition);
                    } else {
                        editText1.setText(text);
                        editText1.setSelection(replacePosition - 1);
                    }
                }
                delete = false;*//*

            }

            @Override
            public void afterTextChanged(Editable s) {

        }
    });


*/


        proceed3 = findViewById(R.id.proceed3);
        proceed3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String mobile_number = getIntent().getStringExtra("Mobile_number");
                String otp = editText1.getText().toString();
                if (!otp.isEmpty()) {
                    // String otp=getIntent().getStringExtra("OTP");

                    final ProgressDialog progressDialog = new ProgressDialog(OTP.this);
                    progressDialog.setMessage("Verifying Details.....");
                    progressDialog.show();

                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<LoginResponse2> call = apiInterface.Statuss3(mobile_number, otp);
                    call.enqueue(new Callback<LoginResponse2>() {
                        @Override
                        public void onResponse(Call<LoginResponse2> call, Response<LoginResponse2> response) {


                            if (response.code() == 200) {

                                progressDialog.dismiss();
                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                //Toast.makeText(OTP.this, statusBean1.getMessage(), Toast.LENGTH_SHORT).show();


                                LoginResponse2.DataBean2 dataBean = response.body().getData();

                                Intent intent = new Intent(OTP.this, SetPin.class);
                                intent.putExtra("Mobile_number", mobile_number);
                                intent.putExtra("Name1", name);
                                intent.putExtra("SurName1", surName1);
                                startActivity(intent);


                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(OTP.this, "Please Check the OTP", Toast.LENGTH_SHORT).show();

                            }


                        }


                /*Intent intent=new Intent(OTP.this,NavigationDrawerDashboard.class);
                startActivity(intent);*/
               /* final String mobile_number=getIntent().getStringExtra("Mobile_number");
                String otp=getIntent().getStringExtra("OTP");

                if (!otp.isEmpty()) {

                final ProgressDialog progressDialog=new ProgressDialog(OTP.this);
                progressDialog.setMessage("Verifying Details.....");
                progressDialog.setContentView(R.layout.activity_splash_login);
                progressDialog.show();

                apiInterface= ApiClient.getClient().create(ApiInterface.class);
                Call<LoginResponse2> call=apiInterface.Statuss3(mobile_number,otp);
                call.enqueue(new Callback<LoginResponse2>() {
                    @Override
                    public void onResponse(Call<LoginResponse2> call, Response<LoginResponse2> response) {

                            if (response.code() == 200) {

                                progressDialog.dismiss();
                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                Toast.makeText(OTP.this, statusBean1.getMessage(), Toast.LENGTH_SHORT).show();
                                LoginResponse2.DataBean2 dataBean = response.body().getData();
                               // Integer otp = dataBean.otp;


                                Intent intent = new Intent(OTP.this, SetPin.class);
                                intent.putExtra("Mobile_number", mobile_number);
                                //intent.putExtra("OTP", otp);
                                startActivity(intent);

                            }else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(OTP.this, "Please Check the OTP", Toast.LENGTH_SHORT).show();

                            }

                        }

                */


                        @Override
                        public void onFailure(Call<LoginResponse2> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(OTP.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                } else {
                    Toast.makeText(getApplicationContext(), "Please enter OTP", Toast.LENGTH_LONG).show();
                }

            }


        });

        back1122.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(OTP.this, WelcomeBack.class);
                intent.putExtra("Mobile_number", mobile_number);
                intent.putExtra("Name1", name);
                intent.putExtra("SurName1", surName1);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });


    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }




}








   /*     editText1.addTextChangedListener(new GenericTextWatcher(editText1));
        editText2.addTextChangedListener(new GenericTextWatcher(editText2));
        editText3.addTextChangedListener(new GenericTextWatcher(editText3));
        editText4.addTextChangedListener(new GenericTextWatcher(editText4));
        editText5.addTextChangedListener(new GenericTextWatcher(editText5));
        editText6.addTextChangedListener(new GenericTextWatcher(editText6));

        GenericTextWatcher genericTextWatcher=new GenericTextWatcher();

    }

    public class GenericTextWatcher implements TextWatcher
    {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        public GenericTextWatcher() {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            boolean allOtherFilled = false;
            EditText nextEdit = null;
            EditText previousEdit = null;
            switch(view.getId())
            {

                case R.id.editText1:
                    if(text.length()==1)
                        editText2.requestFocus();
                    break;
                case R.id.editText2:
                    if(text.length()==1)
                        editText3.requestFocus();
                    else if(text.length()==0)
                        editText1.requestFocus();
                    break;
                case R.id.editText3:
                   if(text.length()==1)
                        editText4.requestFocus();
                    else if(text.length()==0)
                        editText2.requestFocus();
                    break;
                case R.id.editText4:
                    if(text.length()==1)
                        editText5.requestFocus();
                    else if(text.length()==0)
                        editText3.requestFocus();
                    break;

                case R.id.editText5:
                    if(text.length()==1)
                        editText6.requestFocus();
                    else if(text.length()==0)
                        editText4.requestFocus();
                    break;
                case R.id.editText6:
                    if(text.length()==1)
                        proceed3.requestFocus();
                    else if(text.length()==0)
                        editText5.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }*/


