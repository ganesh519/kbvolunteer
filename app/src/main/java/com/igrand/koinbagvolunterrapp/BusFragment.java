package com.igrand.koinbagvolunterrapp;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

@SuppressLint("ValidFragment")
public class BusFragment extends Fragment {
    public BusFragment() {

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_bus, container, false);

        TextView bushyd,busto,busdeparture,busdate,busmonth,busday,bustoday,bustomorrow,buspopular,busflat;
        Typeface typeface,typeface1,typeface2,typeface3;

        typeface=Typeface.createFromAsset(getActivity().getAssets(),"font/Avenir-Heavy.ttf");
        typeface1=Typeface.createFromAsset(getActivity().getAssets(),"font/Avenir-roman.ttf");
        typeface2=Typeface.createFromAsset(getActivity().getAssets(),"font/Avenir-Light.ttf");
        typeface3=Typeface.createFromAsset(getActivity().getAssets(),"font/Avenir-Medium.ttf");


        bushyd=(TextView)v.findViewById(R.id.bushyd);
        busto=(TextView)v.findViewById(R.id.busto);
        busdeparture=(TextView)v.findViewById(R.id.busdeparture);
        busdate=(TextView)v.findViewById(R.id.busdate);
        busmonth=(TextView)v.findViewById(R.id.busmonth);
        busday=(TextView)v.findViewById(R.id.busday);
        bustoday=(TextView)v.findViewById(R.id.bustoday);
        bustomorrow=(TextView)v.findViewById(R.id.bustommorow);
        buspopular=(TextView)v.findViewById(R.id.buspopular);
        busflat=(TextView)v.findViewById(R.id.busflat);
        bushyd.setTypeface(typeface);
        busto.setTypeface(typeface);
        busdeparture.setTypeface(typeface1);
        busdate.setTypeface(typeface1);
        busmonth.setTypeface(typeface2);
        busday.setTypeface(typeface3);
        bustoday.setTypeface(typeface);
        bustomorrow.setTypeface(typeface3);
        buspopular.setTypeface(typeface);
        busflat.setTypeface(typeface);



        Button searchbus=(Button)v.findViewById(R.id.searchbus);
        searchbus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(getActivity().getApplicationContext(),SelectCity.class);
                startActivity(intent);*/

                BottomSheetDialog dialog = new BottomSheetDialog(getContext());
                //dialog.setContentView(R.layout.activity_select_city);
                //dialog.show();
                new CustomBottomSheetDialogFragment12().show(getActivity().getSupportFragmentManager(), "Dialog");
//                dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
            }
        });
        return v;
    }
}
