package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class BrowseFetch {

    @SerializedName("Status")
    @Expose
    public String status;
    @SerializedName("PlanDescription")
    @Expose
    public List<PlanDescription> planDescription = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("planDescription", planDescription).toString();
    }
}
