package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class PromoAppliedFlights extends BaseActivity {

    TextView faredetails;
    Button book;
    ImageView backblack7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_applied_flights);

        faredetails=findViewById(R.id.faredetails);
        backblack7=findViewById(R.id.backblack7);
        book=findViewById(R.id.book);
        faredetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PromoAppliedFlights.this,FareBreakup.class);
                startActivity(intent);
            }
        });

        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PromoAppliedFlights.this,TravellerInfo.class);
                startActivity(intent);
            }
        });
        backblack7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PromoAppliedFlights.this,PromoFlights.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });


    }

}
