package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomBottomSheetDialogFragment19 extends BottomSheetDialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_promocode, container, false);
        Button exit=(Button) v.findViewById(R.id.proceedtopay11);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Fragment metroPromo = new MetroPromo();
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.frame, metroPromo);
                transaction.addToBackStack(null);
                transaction.commit();*/

                Intent intent=new Intent(getActivity(),Payment.class);
                startActivity(intent);
            }
        });

        ImageView exit25=(ImageView)v.findViewById(R.id.exit25);
        exit25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();

            }
        });

       /* TextView textviewmetro=(TextView)v.findViewById(R.id.textviewmetro);
        textviewmetro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),ApplyPromocode2.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });*/
        return v;
    }


}
