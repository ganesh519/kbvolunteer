package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class FareBreakup extends BaseActivity {

    LinearLayout cancellationfee,reschedule,baggage;
    ImageView backfare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fare_breakup);

        cancellationfee=findViewById(R.id.cancellationfee);
        reschedule=findViewById(R.id.reschedule);
        baggage=findViewById(R.id.baggage);
        backfare=findViewById(R.id.backfare);

        cancellationfee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(FareBreakup.this,CancellationFees.class);
                startActivity(intent);
            }
        });


        reschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(FareBreakup.this,RescheduleFees.class);
                startActivity(intent);
            }
        });


        baggage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(FareBreakup.this,Baggage.class);
                startActivity(intent);
            }
        });

        backfare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(FareBreakup.this,PromoAppliedFlights.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
