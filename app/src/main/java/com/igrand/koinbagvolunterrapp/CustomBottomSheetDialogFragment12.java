package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomBottomSheetDialogFragment12 extends BottomSheetDialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_select_city, container, false);


        TextView b1 = (TextView) v.findViewById(R.id.b1);
        TextView hyderabad = (TextView) v.findViewById(R.id.hyderabad);
        TextView arunachal = (TextView) v.findViewById(R.id.arunachal);
        TextView delhi = (TextView) v.findViewById(R.id.delhi);
        TextView pune = (TextView) v.findViewById(R.id.pune);
        TextView chennai = (TextView) v.findViewById(R.id.chennai);
        TextView jaipur = (TextView) v.findViewById(R.id.jaipur);
        TextView mumbai = (TextView) v.findViewById(R.id.mumbai);
        TextView rajkot = (TextView) v.findViewById(R.id.rajkot);
        TextView surat = (TextView) v.findViewById(R.id.surat);
        TextView kolkata = (TextView) v.findViewById(R.id.kolkata);

        Typeface typeface,typeface1;
        typeface=Typeface.createFromAsset(getActivity().getAssets(),"font/Avenir-Heavy.ttf");
        typeface1=Typeface.createFromAsset(getActivity().getAssets(),"font/Avenir-Medium.ttf");

        b1.setTypeface(typeface);
        hyderabad.setTypeface(typeface1);
        arunachal.setTypeface(typeface1);
        delhi.setTypeface(typeface1);
        pune.setTypeface(typeface1);
        chennai.setTypeface(typeface1);
        jaipur.setTypeface(typeface1);
        mumbai.setTypeface(typeface1);
        rajkot.setTypeface(typeface1);
        surat.setTypeface(typeface1);
        kolkata.setTypeface(typeface1);







        TextView searchflight=(TextView) v.findViewById(R.id.hyderabad);
        searchflight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), SplashBus.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);



            }
        });

        ImageView exit20=(ImageView) v.findViewById(R.id.exit20);
        exit20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  Intent intent=new Intent(getActivity(),Flight.class);
                startActivity(intent);*/

                getFragmentManager().beginTransaction().remove(CustomBottomSheetDialogFragment12.this).commit();
            }
        });


        return v;
    }

}

