package com.igrand.koinbagvolunterrapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.Fragments.Data15;
import com.igrand.koinbagvolunterrapp.Fragments.Failure;
import com.igrand.koinbagvolunterrapp.Fragments.FailureRecyclerAdapter;
import com.igrand.koinbagvolunterrapp.Fragments.Pending;
import com.igrand.koinbagvolunterrapp.Fragments.ProcessingRecyclerAdapter;
import com.igrand.koinbagvolunterrapp.Fragments.StatusDataResponse15;
import com.igrand.koinbagvolunterrapp.Fragments.WalletResponse2;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static android.support.constraint.Constraints.TAG;

@SuppressLint("ValidFragment")
public class FailedFragment extends Fragment {

    ApiInterface apiInterface;
    RecyclerView recycler_failure;
    PrefManager prefManager;
    String Id;
    FailureRecyclerAdapter failureRecyclerAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_failed, container, false);

        recycler_failure=v.findViewById(R.id.recycler_failure);

        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");


        //Toast.makeText(getActivity(), "Internal Error", Toast.LENGTH_SHORT).show();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletResponse2> call = apiInterface.WalletResponse1(Id);
        call.enqueue(new Callback<WalletResponse2>() {
            @Override
            public void onResponse(Call<WalletResponse2> call, Response<WalletResponse2> response) {
                // if (response.isSuccessful()) ;

                WalletResponse2 walletResponse1 = response.body();
                if(walletResponse1!=null) {


                    StatusDataResponse15 statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog.dismiss();

                        Data15 data15 = walletResponse1.data;

                        if (walletResponse1.data.failed != null) {


                            List<Failure> failureList = data15.failed;

                            recycler_failure.setLayoutManager(new LinearLayoutManager(getActivity()));
                            failureRecyclerAdapter = new FailureRecyclerAdapter(getContext(), failureList);
                            recycler_failure.setAdapter(failureRecyclerAdapter);

                        } else {
                            progressDialog.dismiss();
                            //Toast.makeText(getActivity(), "Internal error", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        progressDialog.dismiss();
                       // Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                }

            }



            @Override
            public void onFailure(Call<WalletResponse2> call, Throwable t) {
                progressDialog.dismiss();
               // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(getActivity(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });
        return v;
    }
}
