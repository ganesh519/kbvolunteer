package com.igrand.koinbagvolunterrapp;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class SourceStation extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_source_station, container, false);


        BottomSheetDialog dialog = new BottomSheetDialog(getContext());
            dialog.setContentView(R.layout.fragment_source_station);
            dialog.show();
            new CustomBottomSheetDialogFragment18().show(getActivity().getSupportFragmentManager(), "Dialog");
            dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);

            return v;

    }
}
