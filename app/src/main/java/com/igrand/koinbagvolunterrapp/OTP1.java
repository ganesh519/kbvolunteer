package com.igrand.koinbagvolunterrapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Client.ApiClient;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTP1 extends BaseActivity {

    EditText editText1,editText2,editText3,editText4,editText5,editText6;
    Button proceed3;
    ApiInterface apiInterface;
    LinearLayout otp1;
    String text;
    boolean delete=false;
    ImageView back1122;
    TextView resendotp,name001;
    LoginResponse2.StatusBean2 statusBean1;
    EditText editotp;
    PrefManager prefManager;
    String ngo_name,photo,f_name,surname;
    BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp1);
       // editText1=findViewById(R.id.editText1);

        prefManager=new PrefManager(OTP1.this);



        final String mobile_number=getIntent().getStringExtra("Mobile_number");
        final String name=getIntent().getStringExtra("Name1");
        final String surName1=getIntent().getStringExtra("SurName1");






        name001=findViewById(R.id.name001);
        proceed3=findViewById(R.id.proceed003);
        back1122=findViewById(R.id.back1122);
        resendotp=findViewById(R.id.resendotp);
        editotp=findViewById(R.id.editotp);


        /*receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");
                    editText1.setText(message);
                    //Do whatever you want with the code here
                }
            }
        };*/

        prefManager=new PrefManager(getApplicationContext());
        HashMap<String, String> profile=prefManager.getUserDetails();
        ngo_name=profile.get("NgoName");
        photo=profile.get("Photo");
        f_name=profile.get("f_name");
        surname=profile.get("Surname");

        String name1 = getIntent().getStringExtra("Name");

        name001.setText(name1);
        name001.setText(name);

        editotp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String value=String.valueOf(s.length());

                if (value.equals("6")) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//Hide:
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {



            }
        });


        resendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new CountDownTimer(30000, 1000) {


                    public void onTick(long millisUntilFinished) {

                        resendotp.setText("Resend OTP: " + millisUntilFinished / 1000);
                        resendotp.setEnabled(false);

                    }

                    public void onFinish() {
                        resendotp.setText("Re-send OTP?");
                        resendotp.setEnabled(true);
                    }



                }.start();



                final String mobile_number = getIntent().getStringExtra("Mobile_number");



                    final ProgressDialog progressDialog = new ProgressDialog(OTP1.this);
                    progressDialog.setMessage("Verifying Details.....");
                    progressDialog.show();

                    String otp = editotp.getText().toString();

                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<LoginResponse2> call = apiInterface.Statuss12(mobile_number);
                    call.enqueue(new Callback<LoginResponse2>() {
                        @Override
                        public void onResponse(Call<LoginResponse2> call, Response<LoginResponse2> response) {


                            if (response.code() == 200) {

                                progressDialog.dismiss();
                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                LoginResponse2.DataBean2 dataBean = response.body().getData();
                                //Integer otp = dataBean.otp;

                                //Toast.makeText(OTP1.this, statusBean1.getMessage(), Toast.LENGTH_SHORT).show();
                               /* Intent intent = new Intent(OTP1.this, NavigationDrawerDashboard.class);
                                intent.putExtra("Mobile_number", mobile_number);
                                //intent.putExtra("OTP", otp);
                                startActivity(intent);*/

                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(OTP1.this, "Please Check the OTP", Toast.LENGTH_SHORT).show();

                            }

                        }




                        /*if (response.isSuccessful()) ;
                        LoginResponse2 statusResponse = response.body();
                        StatusDataResponse2 statusDataResponse = statusResponse.status;

                        if (statusDataResponse.code == 200) {
                            progressDialog.dismiss();


                            Data2 data = statusResponse.data;

                            Integer otp = data.otp;

                            Toast.makeText(OTP1.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                           *//* Intent intent = new Intent(OTP1.this, OTP1.class);
                            intent.putExtra("Mobile_number", mobile_number);
                            intent.putExtra("OTP", otp);
                            startActivity(intent);*//*

                        } else if (statusDataResponse.code == 409) {
                            progressDialog.dismiss();
                            Toast.makeText(OTP1.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                        }

                    }
*/
                        @Override
                        public void onFailure(Call<LoginResponse2> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(OTP1.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });


                }


        });




        back1122.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(OTP1.this,WelcomeBack.class);
                intent.putExtra("Mobile_number",mobile_number);
                intent.putExtra("Name1",name);
                intent.putExtra("SurName1",surName1);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);


            }
        });
        proceed3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent=new Intent(OTP.this,NavigationDrawerDashboard.class);
                startActivity(intent);*/
                final String mobile_number=getIntent().getStringExtra("Mobile_number");
                String otp = editotp.getText().toString();
                if (!otp.isEmpty()) {
                    // String otp=getIntent().getStringExtra("OTP");

                    final ProgressDialog progressDialog = new ProgressDialog(OTP1.this);
                    progressDialog.setMessage("Verifying Details.....");
                    progressDialog.show();

                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<LoginResponse2> call = apiInterface.Statuss3(mobile_number, otp);
                    call.enqueue(new Callback<LoginResponse2>() {
                        @Override
                        public void onResponse(Call<LoginResponse2> call, Response<LoginResponse2> response) {


                            if (response.code() == 200) {

                                progressDialog.dismiss();
                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                //Toast.makeText(OTP1.this, statusBean1.getMessage(), Toast.LENGTH_SHORT).show();


                                LoginResponse2.DataBean2 dataBean = response.body().getData();
                                String id = String.valueOf(dataBean.id);
                                String first_name=dataBean.firstName;
                                String surname=dataBean.surname;
                                String mobile_number=dataBean.mobileNumber;
                                String user_id=dataBean.userId;
                                String photo=dataBean.photo;
                                String wallet_bal=dataBean.walletBal;
                                String ngo_id=dataBean.ngoId;


                                Intent intent = new Intent(OTP1.this, NavigationDrawerDashboard.class);
                                startActivity(intent);
                              //  prefManager.createLogin(id,ngo_id, first_name, surname, mobile_number, wallet_bal, user_id,ngo_name,photo, created_id);

                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(OTP1.this, "Please Check the OTP", Toast.LENGTH_SHORT).show();

                            }

                        }










                       /* LoginResponse2 statusResponse = response.body();
                        StatusDataResponse3 statusDataResponse=statusResponse.status;

                        if (statusDataResponse.code == 200){
                            progressDialog.dismiss();


                            Data3 data=statusResponse.data;

                            String first_name=data.firstName;
                            String mobile_number=data.mobileNumber;

                            Toast.makeText(OTP1.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(OTP1.this, NavigationDrawerDashboard.class);
                            intent.putExtra("Name",first_name);
                            intent.putExtra("Mobile_number",mobile_number);
                            startActivity(intent);

                        }
                        else if (statusDataResponse.code == 409 ){
                            progressDialog.dismiss();
                            Toast.makeText(OTP1.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                        }
*/

                        @Override
                        public void onFailure(Call<LoginResponse2> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(OTP1.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                }


                else {
                        Toast.makeText(getApplicationContext(), "Please enter OTP", Toast.LENGTH_LONG).show();
                    }

            }


        });

    }


    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }



}

