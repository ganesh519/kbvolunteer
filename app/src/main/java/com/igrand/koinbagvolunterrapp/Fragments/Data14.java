package com.igrand.koinbagvolunterrapp.Fragments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class Data14 {

    @SerializedName("proceesing")
    @Expose
    public List<Proceesing> proceesing = null;
    @SerializedName("approved")
    @Expose
    public List<Approved> approved = null;
    @SerializedName("rejected")
    @Expose
    public List<Rejected> rejected = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("proceesing", proceesing).append("approved", approved).append("rejected", rejected).toString();
    }
}
