package com.igrand.koinbagvolunterrapp.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class BrowsePlansResponseMobile {

    @SerializedName("status")
    @Expose
    private Statuss status;
    @SerializedName("data")
    @Expose
    private List<Datumm> data = null;

    public Statuss getStatus() {
        return status;
    }

    public void setStatus(Statuss status) {
        this.status = status;
    }

    public List<Datumm> getData() {
        return data;
    }

    public void setData(List<Datumm> data) {
        this.data = data;
    }

}



