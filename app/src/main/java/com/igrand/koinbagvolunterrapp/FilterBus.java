package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class FilterBus extends BaseActivity {

    TextView reset1;
    ImageView backblack16;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_bus);

        reset1=findViewById(R.id.reset1);
        backblack16=findViewById(R.id.backblack16);
        reset1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BottomSheetDialog dialog = new BottomSheetDialog(FilterBus.this);
                dialog.setContentView(R.layout.activity_amenities);
                //dialog.show();
                new CustomBottomSheetDialogFragment13().show(getSupportFragmentManager(), "Dialog");
            }
        });

        backblack16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(FilterBus.this,BusDetails.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
    }

