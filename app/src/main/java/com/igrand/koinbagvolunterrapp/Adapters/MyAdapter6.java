package com.igrand.koinbagvolunterrapp.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.igrand.koinbagvolunterrapp.BusFragment;
import com.igrand.koinbagvolunterrapp.FlightFragment;
import com.igrand.koinbagvolunterrapp.MetroFragment;

public class MyAdapter6 extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;
    public MyAdapter6(Context context, FragmentManager supportFragmentManager, int tabCount) {

        super(supportFragmentManager);
        myContext = context;
        this.totalTabs = tabCount;
    }

    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FlightFragment flightFragment = new FlightFragment();
                return flightFragment;
            case 1:
                BusFragment busFragment = new BusFragment();
                return busFragment;
            case 2:
                MetroFragment metroFragment = new MetroFragment();
                return metroFragment;

            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;

    }
}

