package com.igrand.koinbagvolunterrapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.BaseActivity;
import com.igrand.koinbagvolunterrapp.BrowseFetch;
import com.igrand.koinbagvolunterrapp.BrowsePlansTv;
import com.igrand.koinbagvolunterrapp.Client.APIClient1;
import com.igrand.koinbagvolunterrapp.DTHRecharge;
import com.igrand.koinbagvolunterrapp.Payment;
import com.igrand.koinbagvolunterrapp.PaymentDTH;
import com.igrand.koinbagvolunterrapp.PaymentMobileRechargeActivity;
import com.igrand.koinbagvolunterrapp.PlanDescription;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.ProceedToRecharge;
import com.igrand.koinbagvolunterrapp.R;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AirtelTV extends BaseActivity {

    TextView browseplanstv;
    Button proceedtorecharge1;
    ImageView back6,img;
    String Operator_Code,Amount,Operator_Name,MobileNumber,Operator,id,image,image1,mobile,MobileNumber1;
    ApiInterface apiInterface;
    EditText enteramount,entermob;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtel_tv);

        prefManager = new PrefManager(AirtelTV.this);

        HashMap<String, String> profile = prefManager.getUserDetails();
        id = profile.get("id");

        enteramount=findViewById(R.id.enteramount);
        entermob=findViewById(R.id.entermob);
        img=findViewById(R.id.img);





        Operator_Code = getIntent().getStringExtra("Operator");
        Operator_Name = getIntent().getStringExtra("OperatorName");
        image = getIntent().getStringExtra("Image");
        image1 = getIntent().getStringExtra("Image1");
        Amount = getIntent().getStringExtra("Amount");
        MobileNumber1 = getIntent().getStringExtra("Mobile");


        entermob.setText(MobileNumber1);

        Operator=Operator_Code + "@" + Operator_Name;



        enteramount.setText(Amount);

        if(image==null) {

            Picasso.get().load(image1).error(R.drawable.kb).into(img);
        } else
        {
            Picasso.get().load(image).error(R.drawable.kb).into(img);

        }






        browseplanstv=findViewById(R.id.browseplanstv);
        browseplanstv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobile=entermob.getText().toString();
                Intent intent=new Intent(AirtelTV.this,BrowsePlansTv.class);
                intent.putExtra("Operator",Operator_Code);
                intent.putExtra("OperatorName",Operator_Name);
                intent.putExtra("Mobile",mobile);

                startActivity(intent);
            }
        });


        proceedtorecharge1=findViewById(R.id.proceedtorecharge1);
        proceedtorecharge1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Amount = enteramount.getText().toString();
                MobileNumber=entermob.getText().toString();

                if (Amount.equals("")) {

                    Toast.makeText(AirtelTV.this, "Please Enter Amount", Toast.LENGTH_SHORT).show();
                } else {

                    Intent intent = new Intent(AirtelTV.this, PaymentDTH.class);

                    //intent.putExtra("Amount",AMount);
                    intent.putExtra("Id",id);
                    intent.putExtra("Amount", Amount);
                    intent.putExtra("Operator", Operator);
                    intent.putExtra("CircleCode", "1@Andhra Pradesh");
                    intent.putExtra("RechargeType", "dth");
                    intent.putExtra("MobileNumber", mobile);

                    startActivity(intent);
                }

                }
        });


        back6=findViewById(R.id.back6);
        back6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
    }
}
