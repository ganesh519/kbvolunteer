package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Activities.RechargePaymentMobileResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentMobileRechargeActivity extends BaseActivity {

    private static final String TAG = "payment";
    Button payment;
    ImageView backbutton;
    String amount,operator,circle_code,recharge_type,mobile_number;
    String circle_code1="1";
    TextView finalamount,walbal;
    PrefManager prefManager;
    String id,walletbals;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        payment=findViewById(R.id.payment);
        backbutton=findViewById(R.id.back);
        finalamount=findViewById(R.id.finalamount);
        walbal=findViewById(R.id.walbal);


        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        prefManager=new PrefManager(getApplicationContext());
        HashMap<String, String> profile=prefManager.getUserDetails();
        id=profile.get("id");
        walletbals=profile.get("Walletbals");

        walbal.setText(walletbals);
        if (getIntent() !=null){
            amount=getIntent().getStringExtra("Amount");
            operator=getIntent().getStringExtra("Operator");
            circle_code=getIntent().getStringExtra("CircleCode");
            recharge_type=getIntent().getStringExtra("RechargeType");
            mobile_number=getIntent().getStringExtra("MobileNumber");


        }
        finalamount.setText(amount);



        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String walletbalance=walbal.getText().toString();
                final String finalamount1=finalamount.getText().toString();


                double number = Double.parseDouble(walletbalance);
                int walletbalance1=Integer.parseInt(finalamount1);

                String amount=finalamount.getText().toString().trim();
                if(amount.equals("")||amount.equals(null)) {

                    Toast.makeText(PaymentMobileRechargeActivity.this, "Please enter amount...", Toast.LENGTH_SHORT).show();


                } else  if(number==0.00 || number<walletbalance1) {

                    Toast.makeText(PaymentMobileRechargeActivity.this, "Insufficient Balance...", Toast.LENGTH_SHORT).show();


                }

                else {

                    getRecharge(id,amount,operator,circle_code,recharge_type,mobile_number);
                }

            }
        });


    }

    private void getRecharge(String id, String amount, String operator, String circle_code, String recharge_type, String mobile_number) {

        final ProgressDialog progressDialog=new ProgressDialog(PaymentMobileRechargeActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<RechargePaymentMobileResponse> call=RetrofitClient.getInstance().getApi().MobileRecharge(id, amount, operator, circle_code, recharge_type, mobile_number);
        call.enqueue(new Callback<RechargePaymentMobileResponse>() {
            @Override
            public void onResponse(Call<RechargePaymentMobileResponse> call, Response<RechargePaymentMobileResponse> response) {
                if (response.isSuccessful());




                if (response.code()==200){
                    progressDialog.dismiss();
                    Toast.makeText(PaymentMobileRechargeActivity.this, "Payment Success", Toast.LENGTH_LONG).show();

                    Intent intent=new Intent(PaymentMobileRechargeActivity.this,NavigationDrawerDashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);


                }
                else if (response.code()!= 200){
                    progressDialog.dismiss();
                    Toast.makeText(PaymentMobileRechargeActivity.this, " Recharge Failed. Refund Complete,Please Try Again", Toast.LENGTH_LONG).show();
                    Intent intent=new Intent(PaymentMobileRechargeActivity.this,NavigationDrawerDashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                /*else if (response.code() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(PaymentMobileRechargeActivity.this, "Failed", Toast.LENGTH_LONG).show();
                }*/

            }

            @Override
            public void onFailure(Call<RechargePaymentMobileResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PaymentMobileRechargeActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }
}
