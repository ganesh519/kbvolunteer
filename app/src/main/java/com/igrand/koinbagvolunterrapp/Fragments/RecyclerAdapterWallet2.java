package com.igrand.koinbagvolunterrapp.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.Data7;
import com.igrand.koinbagvolunterrapp.R;

import java.util.List;

public class RecyclerAdapterWallet2 extends RecyclerView.Adapter<RecyclerAdapterWallet2.ViewHolderClass> {

    List<Data7> data7;



    Context context;
    // String amount,requesteddate;

    public RecyclerAdapterWallet2(Context context, List<Data7> walletdata) {

        this.context=context;
        this.data7=walletdata;
    }

    @NonNull
    @Override
    public ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(context).inflate(R.layout.listview3,viewGroup,false);
        ViewHolderClass viewHolderClass=new ViewHolderClass(view);
        return viewHolderClass;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderClass viewHolderClass, int i) {

        /* Data7 data=data7.get(i);*/

        viewHolderClass.i1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialogbox2);
                dialog.show();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                ImageView close1=(ImageView) dialog.findViewById(R.id.close1);

                close1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });

            }
        });


    }


    @Override
    public int getItemCount() {
        return data7.size();
    }

    public static class ViewHolderClass extends RecyclerView.ViewHolder {

        private final Context context;
        TextView requesteddate,amount;
        ImageView i1;

        public ViewHolderClass(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();

            requesteddate=itemView.findViewById(R.id.requested_date);
            amount=itemView.findViewById(R.id.amount);
            i1=itemView.findViewById(R.id.i1);

        }
    }
}


