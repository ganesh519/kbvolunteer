package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class CustomBottomSheetDialogFragment9 extends BottomSheetDialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_searchflights, container, false);
        Button searchflghts=(Button) v.findViewById(R.id.searchflghts);
        searchflghts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         /* Intent intent=new Intent(getActivity().getApplicationContext(),SortFilter.class);
                startActivity(intent);*/

                Fragment sortfilter = new SortFilterFragment();
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.frame5, sortfilter);
                transaction.addToBackStack(null);
                transaction.commit();


            }
        });

        ImageView exit17=(ImageView)v.findViewById(R.id.exit17);
        exit17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return v;
    }


}

