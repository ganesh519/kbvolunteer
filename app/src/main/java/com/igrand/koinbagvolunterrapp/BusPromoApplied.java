package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class BusPromoApplied extends BaseActivity {

    Button proceedtopaybus1;
    ImageView backblack15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_promo_applied);

        proceedtopaybus1=findViewById(R.id.proceedtopaybus1);
        backblack15=findViewById(R.id.backblack15);
        proceedtopaybus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialog dialog = new BottomSheetDialog(BusPromoApplied.this);
                dialog.setContentView(R.layout.activity_finalpayment);
                //dialog.show();
                new CustomBottomSheetDialogFragment15().show(getSupportFragmentManager(), "Dialog");
            }
        });
        backblack15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BusPromoApplied.this,BusPromo.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
