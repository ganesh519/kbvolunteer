package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Activities.AirtelTV;
import com.igrand.koinbagvolunterrapp.Adapters.MyAdapter3;
import com.igrand.koinbagvolunterrapp.Client.APIClient1;
import com.igrand.koinbagvolunterrapp.Client.APIClient2;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BrowsePlansTv extends BaseActivity {


    ImageView back7;
    String Operator_Code,Operator_Name,Type,Circle,MobileNumber;
    ApiInterface apiInterface;
    RecyclerView recycler_dthbrowse;
    BrowseDthRecyclerAdapter browseDthRecyclerAdapter;
    TextView type1,circle1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_plans_tv);

        recycler_dthbrowse=findViewById(R.id.recycler_dthbrowse);
        back7=findViewById(R.id.back2);
        type1=findViewById(R.id.type1);
        circle1=findViewById(R.id.circle1);

        back7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final String APIID = "AP135358";
        final String PASSWORD = "ki832h74td3ff";
        final String Circle_Code = "1";
        final String PageID = "0";
        final String FORMAT = "JSON/XML";
        Operator_Code = getIntent().getStringExtra("Operator");
        Operator_Name = getIntent().getStringExtra("OperatorName");
        MobileNumber = getIntent().getStringExtra("Mobile");





        final ProgressDialog progressDialog = new ProgressDialog(BrowsePlansTv.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PrepaidResponse> call = apiInterface.DTH("dth");
        call.enqueue(new Callback<PrepaidResponse>() {
            @Override
            public void onResponse(Call<PrepaidResponse> call, Response<PrepaidResponse> response) {
                // if (response.isSuccessful()) ;
                PrepaidResponse walletResponse1 = response.body();
                if (walletResponse1 != null) {

                    PrepaidResponse prepaidResponse=response.body();

                    PrepaidResponse.StatusBean statusBean=prepaidResponse.getStatus();
                    if (statusBean.getCode() == 200){
                        circle1.setVisibility(View.VISIBLE);

                        List<PrepaidResponse.DataBean> dataBeanList=prepaidResponse.getData();

                        for (int i = 0; i <= dataBeanList.size(); i++) {


                            if (dataBeanList.get(i).getOperator_code().equals(Operator_Code)) {


                                Type = dataBeanList.get(i).getOperator_name();
                                type1.setText(Type);
                                break;


                            }

                            }



                    } else if (statusBean.getCode() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(BrowsePlansTv.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(BrowsePlansTv.this, "No Data", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<PrepaidResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast = Toast.makeText(BrowsePlansTv.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });








     //  circle1.setText(Circle);






        final ProgressDialog progressDialog1 = new ProgressDialog(BrowsePlansTv.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();


        apiInterface = APIClient1.getClient().create(ApiInterface.class);
        Call<BrowseFetch> call1 = apiInterface.browseplansdth(APIID, PASSWORD, Operator_Code, Circle_Code, MobileNumber,PageID, FORMAT);
      /*  apiInterface = APIClient2.getClient().create(ApiInterface.class);
       Call<BrowseFetch> call1 = apiInterface.browseplansdth1( Operator_Code,MobileNumber);*/


        call1.enqueue(new Callback<BrowseFetch>() {
            @Override
            public void onResponse(Call<BrowseFetch> call, Response<BrowseFetch> response) {

                if (response.isSuccessful()) ;
                BrowseFetch operatorFetch = response.body();

                if (response.code() == 200) {
                    progressDialog1.dismiss();
                    progressDialog.dismiss();

                    List<PlanDescription> planDescriptions = operatorFetch.planDescription;
                    
                    if(planDescriptions!=null) {

                        recycler_dthbrowse.setLayoutManager(new LinearLayoutManager(BrowsePlansTv.this));
                        browseDthRecyclerAdapter = new BrowseDthRecyclerAdapter(BrowsePlansTv.this,planDescriptions,Operator_Name,MobileNumber);
                        recycler_dthbrowse.setAdapter(browseDthRecyclerAdapter);

                       // Toast.makeText(BrowsePlansTv.this, "Success..", Toast.LENGTH_SHORT).show();
                        
                    } else {
                        Toast.makeText(BrowsePlansTv.this, "NO RESULTS FOUND !", Toast.LENGTH_SHORT).show();
                    }


                   


                } else if (response.code() != 200) {
                    progressDialog1.dismiss();
                    //Toast.makeText(MobileRechargeActivity.this, "Volunteer Not Exist ,Please Contact Koinbag Ngo..", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<BrowseFetch> call, Throwable t) {
                progressDialog1.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(BrowsePlansTv.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


    }
}
