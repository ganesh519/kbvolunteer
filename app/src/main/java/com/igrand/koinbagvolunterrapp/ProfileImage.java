package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ProfileImage extends AppCompatActivity {

    ImageView profileimage,profileback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_image);


        if(getIntent().hasExtra("byteArray")) {

            profileimage=findViewById(R.id.profileimage);
            profileback=findViewById(R.id.profileback);
            profileback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            Bitmap _bitmap = BitmapFactory.decodeByteArray(
                    getIntent().getByteArrayExtra("byteArray"),0,getIntent().getByteArrayExtra("byteArray").length);
            profileimage.setImageBitmap(_bitmap);
        }
    }
}
