package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class TravellerInfo extends BaseActivity {

    TextView faredetails1;
    Button book1;
    ImageView backblack8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traveller_info);

        faredetails1=findViewById(R.id.faredetails1);
        book1=findViewById(R.id.book1);
        backblack8=findViewById(R.id.backblack8);
        faredetails1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TravellerInfo.this,FareBreakup.class);
                startActivity(intent);
            }
        });

        book1=findViewById(R.id.book1);
        book1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TravellerInfo.this,TravellerInformation.class);
                startActivity(intent);
            }
        });

        backblack8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TravellerInfo.this,PromoAppliedFlights.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
