package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomBottomSheetDialogFragment8 extends BottomSheetDialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_search_flights, container, false);

        TextView searchflight=(TextView) v.findViewById(R.id.del);
        searchflight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(getActivity().getApplicationContext(), SearchFlights.class);
                startActivity(intent);*/



                Fragment numofTravellers = new CustomBottomSheetDialogFragment23();
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();

                BottomSheetDialog dialog = new BottomSheetDialog(getContext());
                dialog.setContentView(R.layout.fragment_num_of_travellers);

                TextView selectedseat=(TextView)dialog.findViewById(R.id.done);
                selectedseat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent= new Intent(getActivity(),SplashFlight.class);
                        startActivity(intent);
                    }
                });
                dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
                dialog.show();
                transaction.replace(R.id.frame4, numofTravellers);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });

        ImageView exit16=(ImageView) v.findViewById(R.id.exit16);

        //Intent intent=new Intent(getActivity().getApplicationContext(),NumOfTravellers.class);
        // startActivity(intent);

        exit16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return v;
    }

}


