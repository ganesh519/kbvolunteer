package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class StatusResponse9 {

    @SerializedName("status")
    @Expose
    public StatusDataResponse9 status;
    @SerializedName("data")
    @Expose
    public Data9 data;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }

}
