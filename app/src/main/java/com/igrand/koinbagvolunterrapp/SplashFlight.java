package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashFlight extends BaseActivity {
    private static int splashscreentimeout=2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_flight);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashFlight.this,FlightFare.class);
                startActivity(intent);

            }
        },splashscreentimeout);

    }
}
