package com.igrand.koinbagvolunterrapp.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.igrand.koinbagvolunterrapp.FailedFragment;
import com.igrand.koinbagvolunterrapp.Fragments.AllFragment1;
import com.igrand.koinbagvolunterrapp.SuccessFragment;

public class MyAdapter1 extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;
    public MyAdapter1(Context context, FragmentManager supportFragmentManager, int tabCount) {

        super(supportFragmentManager);
        myContext = context;
        this.totalTabs = tabCount;
    }

    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                AllFragment1 allFragment1 = new AllFragment1();
                return allFragment1;
            case 1:
                FailedFragment failedFragment = new FailedFragment();
                return failedFragment;
            case 2:
                SuccessFragment successFragment = new SuccessFragment();
                return successFragment;


            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;

    }
}

