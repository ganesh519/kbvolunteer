package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.koinbagvolunterrapp.Activities.AddBag;
import com.igrand.koinbagvolunterrapp.Activities.AddMeals;

public class TravellerInformation extends BaseActivity {

    LinearLayout addbag,addmeals;
    TextView faredetails2;
    Button book2;
    ImageView backblack9;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traveller_information);

        faredetails2=findViewById(R.id.faredetails2);
        book2=findViewById(R.id.book2);
        addbag=findViewById(R.id.addbag);
        addmeals=findViewById(R.id.addmeals);
        backblack9=findViewById(R.id.backblack9);
        faredetails2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TravellerInformation.this,FareBreakup.class);
                startActivity(intent);
            }
        });

        book2=findViewById(R.id.book2);
        book2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BottomSheetDialog dialog = new BottomSheetDialog(TravellerInformation.this);
                dialog.setContentView(R.layout.activity_proceed_to_pay);
                //dialog.show();
                new CustomBottomSheetDialogFragment11().show(getSupportFragmentManager(), "Dialog");



            }
        });

        addbag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TravellerInformation.this,AddBag.class);
                startActivity(intent);
            }
        });

        addmeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TravellerInformation.this,AddMeals.class);
                startActivity(intent);
            }
        });
        backblack9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TravellerInformation.this,TravellerInfo.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
