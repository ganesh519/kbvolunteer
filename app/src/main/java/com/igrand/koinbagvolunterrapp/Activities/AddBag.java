package com.igrand.koinbagvolunterrapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.igrand.koinbagvolunterrapp.BaseActivity;
import com.igrand.koinbagvolunterrapp.R;
import com.igrand.koinbagvolunterrapp.TravellerInformation;

public class AddBag extends BaseActivity {

    Button book3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bag);

        book3=findViewById(R.id.book3);
        book3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AddBag.this,TravellerInformation.class);
                startActivity(intent);
            }
        });
    }
}
