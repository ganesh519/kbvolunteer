package com.igrand.koinbagvolunterrapp.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.ApiInterface;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.PrefManager;
import com.igrand.koinbagvolunterrapp.R;
import com.igrand.koinbagvolunterrapp.StatusDataResponse7;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FailedFragment2 extends Fragment {
    RecyclerView recycler_failure;
    PrefManager prefManager;
    String Id;
    ApiInterface apiInterface;
    FailureRecyclerAdapter failureRecyclerAdapter;
    ImageView image_error;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_failed, container, false);

        recycler_failure=v.findViewById(R.id.recycler_failure);
       // image_error=v.findViewById(R.id.image_error);

        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");

        final ProgressDialog progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        apiInterface= ApiClient.getClient().create(ApiInterface.class);

        Call<WalletResponse2> call=apiInterface.WalletResponse1(Id);
        call.enqueue(new Callback<WalletResponse2>() {
            @Override
            public void onResponse(Call<WalletResponse2> call, Response<WalletResponse2> response) {
                if (response.isSuccessful());
                WalletResponse2 walletResponse2=response.body();
                StatusDataResponse15 statusDataResponse15=walletResponse2.status;

                if (statusDataResponse15.code == 200){
                    progressDialog.dismiss();

                    Data15 data15=walletResponse2.data;

                    if (walletResponse2.data.failed != null){

                        List<Failure> failureList=data15.failed;

                        failureRecyclerAdapter=new FailureRecyclerAdapter(getActivity(),failureList);
                        recycler_failure.setAdapter(failureRecyclerAdapter);
                      //  Toast.makeText(getActivity(),statusDataResponse15.message, Toast.LENGTH_SHORT).show();

                    }
                    else {
                        //image_error.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), "Internal Error", Toast.LENGTH_SHORT).show();
                    }
                }
                else if (statusDataResponse15.code == 409){
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(),statusDataResponse15.message, Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<WalletResponse2> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        return v;

    }
}
