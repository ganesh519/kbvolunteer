package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

class StatusResponse6 {

    @SerializedName("status")
    @Expose
    public StatusDataResponse6 status;
    @SerializedName("data")
    @Expose
    public Data6 data;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }
}
