package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.json.JSONObject;

public class PlanDescription {


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("operator_id")
    @Expose
    public String operatorId;
    @SerializedName("circle_id")
    @Expose
    public String circleId;
    @SerializedName("recharge_amount")
    @Expose
    public String rechargeAmount;
    @SerializedName("recharge_talktime")
    @Expose
    public String rechargeTalktime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getCircleId() {
        return circleId;
    }

    public void setCircleId(String circleId) {
        this.circleId = circleId;
    }

    public String getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getRechargeTalktime() {
        return rechargeTalktime;
    }

    public void setRechargeTalktime(String rechargeTalktime) {
        this.rechargeTalktime = rechargeTalktime;
    }

    public String getRechargeValidity() {
        return rechargeValidity;
    }

    public void setRechargeValidity(String rechargeValidity) {
        this.rechargeValidity = rechargeValidity;
    }

    public String getRechargeShortDesc() {
        return rechargeShortDesc;
    }

    public void setRechargeShortDesc(String rechargeShortDesc) {
        this.rechargeShortDesc = rechargeShortDesc;
    }

    public String getRechargeLongDesc() {
        return rechargeLongDesc;
    }

    public void setRechargeLongDesc(String rechargeLongDesc) {
        this.rechargeLongDesc = rechargeLongDesc;
    }

    public String getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(String rechargeType) {
        this.rechargeType = rechargeType;
    }

    @SerializedName("recharge_validity")
    @Expose
    public String rechargeValidity;
    @SerializedName("recharge_short_desc")
    @Expose
    public String rechargeShortDesc;
    @SerializedName("recharge_long_desc")
    @Expose
    public String rechargeLongDesc;
    @SerializedName("recharge_type")
    @Expose
    public String rechargeType;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("operatorId", operatorId).append("circleId", circleId).append("rechargeAmount", rechargeAmount).append("rechargeTalktime", rechargeTalktime).append("rechargeValidity", rechargeValidity).append("rechargeShortDesc", rechargeShortDesc).append("rechargeLongDesc", rechargeLongDesc).append("rechargeType", rechargeType).toString();
    }
}
