package com.igrand.koinbagvolunterrapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagvolunterrapp.Activities.Login;
import com.igrand.koinbagvolunterrapp.Client.ApiClient;
import com.igrand.koinbagvolunterrapp.Fragments.FragmentWallet1;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NavigationDrawerDashboard extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    LinearLayout viewProfile, logout1;
    ImageView toggle, walletTransactions, digitalTransactions, home, profile;
    TextView volunteer, thome, tdigital, twallet, tprofile;
    Typeface typeface, typeface1, typeface2, typeface3, typeface4;
    NavigationView nav_view;
    BottomNavigationView bottomNavigationView;
    public static ApiInterface apiInterface;
    public static ProgressDialog progressDialog;
    public FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    int i;
    public static String amount, requested_date, updated_date;
    final Fragment fragment1 = new HomeFragment();
    final Fragment fragment2 = new FragmentDigital();
    final Fragment fragment3 = new FragmentWallet1();
    final Fragment fragment4 = new ProfileFragment();


    Fragment active = fragment1;
    Fragment active2 = fragment2;
    Fragment active3 = fragment3;
    Fragment active4 = fragment4;

    PrefManager prefManager;
    String Id,f_name,surname,email,mobile,walletbals,userid,Ngo_Name,Photo;
    Fragment currentfragment;
    Boolean exit=false;
    boolean doubleBackToExitPressedOnce = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer_dashboard);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, new HomeFragment(),"homee");
        fragmentTransaction.commit();




        logout1 = findViewById(R.id.logout1);
        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefManager.clearSession();
                Intent intent = new Intent(NavigationDrawerDashboard.this, Login.class);
                intent.putExtra("Home",false);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);


        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }


        navigationView.getMenu().getItem(0).setActionView(R.layout.menu_image);
        navigationView.getMenu().getItem(1).setActionView(R.layout.menu_image);
        navigationView.getMenu().getItem(2).setActionView(R.layout.menu_image);
        navigationView.getMenu().getItem(3).setActionView(R.layout.menu_image);
        navigationView.getMenu().getItem(4).setActionView(R.layout.menu_image);
        navigationView.getMenu().getItem(5).setActionView(R.layout.menu_image);

        View headerview = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        final TextView volunteer = (TextView) headerview.findViewById(R.id.volunteername1);
        final TextView volunteer2 = (TextView) headerview.findViewById(R.id.volunteername2);
        final TextView phnenum = (TextView) headerview.findViewById(R.id.phnenum);
        final TextView id1 = (TextView) headerview.findViewById(R.id.id1);
        TextView viewprofile = (TextView) headerview.findViewById(R.id.viewprofile);
        TextView wallet = (TextView) headerview.findViewById(R.id.wallet);
        final TextView rs = (TextView)headerview.findViewById(R.id.rs);
        TextView logout = (TextView)findViewById(R.id.logout);
        TextView volunteer11 = (TextView)findViewById(R.id.volunteername1);
        final ImageView imagekb=(ImageView)headerview.findViewById(R.id.imagekb);


        typeface = Typeface.createFromAsset(getAssets(), "font/Avenir-BlackOblique.ttf");
        typeface1 = Typeface.createFromAsset(getAssets(), "font/Avenir-Book.ttf");
        typeface2 = Typeface.createFromAsset(getAssets(), "font/Avenir-Oblique.ttf");
        typeface3 = Typeface.createFromAsset(getAssets(), "font/Avenir-Heavy.ttf");
        typeface4 = Typeface.createFromAsset(getAssets(), "font/Avenir-Medium.ttf");


        volunteer.setTypeface(typeface);
        phnenum.setTypeface(typeface1);
        id1.setTypeface(typeface2);
        viewprofile.setTypeface(typeface3);
        wallet.setTypeface(typeface3);
        rs.setTypeface(typeface3);
        logout.setTypeface(typeface4);


        LinearLayout viewProfile = (LinearLayout) headerview.findViewById(R.id.viewProfile);
        viewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final String id = getIntent().getStringExtra("Id");
                final String userid = getIntent().getStringExtra("User_Id");
                final String first_name = getIntent().getStringExtra("FirstName");

                final ProgressDialog progressDialog = new ProgressDialog(NavigationDrawerDashboard.this);
                progressDialog.setMessage("Verifying Details.....");
                progressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<StatusResponse5> call = apiInterface.Statuss5(Id);
                call.enqueue(new Callback<StatusResponse5>() {
                    @Override
                    public void onResponse(Call<StatusResponse5> call, Response<StatusResponse5> response) {
                        if (response.isSuccessful()) ;

                        StatusResponse5 statusResponse = response.body();
                        StatusDataResponse5 statusDataResponse = statusResponse.status;

                        if (statusDataResponse.code == 200) {
                            progressDialog.dismiss();


                            Data5 data = statusResponse.data;

                            String title = data.title;
                            String surname = data.surname;
                            String last_name = data.lastName;
                            String middle_name = data.middleName;
                            String email = data.email;
                            String photo = data.photo;
                            String mobile_number = data.mobileNumber;
                            String date_of_birth = data.dateOfBirth;
                            String state = data.state;
                            String district = data.district;
                            String revenue_devision = data.revenueDevision;
                            String mandal = data.mandal;
                            String village = data.village;
                            String addressline1 = data.addressline1;
                            String addressline2 = data.addressline2;
                            String addressline3 = data.addressline3;
                            String pancard = data.pancard;
                            String panphoto = data.panphoto;
                            String aadharcard = data.aadharcard;
                            String aadharphoto = data.aadharphoto;
                            String _10th = data.tenthPhoto;
                            String inter = data.interPhoto;
                            String firstname=data.firstName;
                            String tenthmarks=data.tenthMarks;
                            String intermarks=data.interMarks;
                            String degreemarks=data.degreeMarks;
                            String contactname=data.contactName;
                            String contacttype=data.contactType;
                            String contactnumber=data.contactNumber;
                            String relation=data.relation;


                            //Toast.makeText(NavigationDrawerDashboard.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(NavigationDrawerDashboard.this, PersonalDetails.class);
                            intent.putExtra("Title", title);
                            intent.putExtra("SurName", surname);
                            intent.putExtra("LastName", last_name);
                            intent.putExtra("MiddleName", middle_name);
                            intent.putExtra("Email", email);
                            intent.putExtra("Photo", photo);
                            intent.putExtra("MobileNumber", mobile_number);
                            intent.putExtra("DateOfBirth", date_of_birth);
                            intent.putExtra("State", state);
                            intent.putExtra("District", district);
                            intent.putExtra("RevenueDevision", revenue_devision);
                            intent.putExtra("Mandal", mandal);
                            intent.putExtra("Village", village);
                            intent.putExtra("AddressLine1", addressline1);
                            intent.putExtra("AddressLine2", addressline2);
                            intent.putExtra("AddressLine3", addressline3);
                            intent.putExtra("Pancard", pancard);
                            intent.putExtra("PanPhoto", panphoto);
                            intent.putExtra("AadharCard", aadharcard);
                            intent.putExtra("AadharPhoto", aadharphoto);
                            intent.putExtra("Tenth", _10th);
                            intent.putExtra("Inter", inter);
                            intent.putExtra("UserId", userid);
                            //intent.putExtra("FirstName", first_name);


                            intent.putExtra("FirstName", firstname);
                            intent.putExtra("TenthMarks", tenthmarks);
                            intent.putExtra("InterMarks", intermarks);
                            intent.putExtra("DegreeMarks", degreemarks);
                            intent.putExtra("ContactName", contactname);
                            intent.putExtra("ContactType", contacttype);
                            intent.putExtra("ContactNumber", contactnumber);
                            intent.putExtra("Relation", relation);
                            startActivityForResult(intent, 100);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        } else if (statusDataResponse.code == 409) {
                            progressDialog.dismiss();
                            Toast.makeText(NavigationDrawerDashboard.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<StatusResponse5> call, Throwable t) {
                        progressDialog.dismiss();
                       // Toast.makeText(NavigationDrawerDashboard.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        Toast toast= Toast.makeText(NavigationDrawerDashboard.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();

                    }
                });
            }
        });

        bottomNavigationView = findViewById(R.id.the_bottom_navigation);
        bottomNavigationView.setItemIconTintList(null);

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//        fragmentManager = getSupportFragmentManager();
//        fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.frame_layout, new HomeFragment(),"homee").hide(active);
//        fragmentTransaction.commit();
//        active = fragment1;

//        fragmentManager.beginTransaction().add(R.id.frame_layout, fragment3, "3").hide(fragment3).commit();
//        fragmentManager.beginTransaction().add(R.id.frame_layout, fragment2, "").hide(fragment2).commit();


        prefManager=new PrefManager(getApplicationContext());
        HashMap<String, String> profile=prefManager.getUserDetails();
        Id=profile.get("id");
        f_name=profile.get("f_name");
        surname=profile.get("Surname");
        mobile=profile.get("Mobile");
        userid=profile.get("Userid");
        walletbals=profile.get("Walletbals");
        Ngo_Name=profile.get("NgoName");
        Photo=profile.get("Photo");


        id1.setText(userid);
        volunteer.setText(f_name);
        phnenum.setText(mobile);
        rs.setText(walletbals);
        volunteer2.setText(surname);
        Picasso.get().load(Photo).error(R.drawable.kb).into(imagekb);

        imagekb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagekb.buildDrawingCache();
                Bitmap bitmap = imagekb.getDrawingCache();
                Intent intent=new Intent(NavigationDrawerDashboard.this,ProfileImage.class);

                ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                intent.putExtra("byteArray", _bs.toByteArray());
                startActivity(intent);
            }

        });


    }


    BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_home:

                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new HomeFragment(),"homee").hide(active);
                    fragmentTransaction.commit();
                    active = fragment1;
                    return true;


                case R.id.navigation_digital:
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new FragmentDigital(),"").hide(active2);
                    fragmentTransaction.commit();
                    active2 = fragment2;
                    return true;

                case R.id.navigation_wallet:

                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new FragmentWallet1(),"").hide(active3);
                    fragmentTransaction.commit();
                    active3=fragment3;

//                    fragmentManager.beginTransaction().hide(active).show(fragment3).commit();
//                    active = fragment3;
                    return true;

                case R.id.navigation_profile:
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new ProfileFragment(),"").hide(active4);
                    fragmentTransaction.commit();
                    active4=fragment4;
                    return true;
            }

            return false;
        }

    };

    public void WalletMethod(String id) {

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletResponse> call = apiInterface.Statuss7(id);
        call.enqueue(new Callback<WalletResponse>() {
            @Override
            public void onResponse(Call<WalletResponse> call, Response<WalletResponse> response) {
                if (response.isSuccessful()) ;

                WalletResponse statusResponse = response.body();
                StatusDataResponse7 statusDataResponse = statusResponse.status;

                if (statusDataResponse.code == 200) {
                    progressDialog.dismiss();

                    List<Data7> data = statusResponse.data;
                    for (int i = 0; i < data.size(); i++) {

                        amount = data.get(i).amount;
                        requested_date = data.get(i).requestedDate;
                        updated_date = data.get(i).updatedDate;
                    }


                  /*  Bundle bundle1 = new Bundle();

                    bundle1.putString("Amount", amount);
                    bundle1.putString("RequestedDate", requested_date);
                    bundle1.putString("UpdatedDate", updated_date);
                  */


                    Toast.makeText(NavigationDrawerDashboard.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                    //    Intent intent = new Intent(NavigationDrawerDashboard.this, PersonalDetails.class);

                } else if (statusDataResponse.code == 409) {
                    progressDialog.dismiss();
                    Toast.makeText(NavigationDrawerDashboard.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<WalletResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(NavigationDrawerDashboard.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }


    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (exit) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }

                this.exit = true;

                BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.the_bottom_navigation);
                bottomNavigationView.setSelectedItemId(R.id.navigation_home);
                Toast.makeText(NavigationDrawerDashboard.this, "Press Back again to Exit...", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 5000);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer_dashboard, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "font/Avenir-Medium.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.


        if (item.getItemId() == R.id.nav_home) {

            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout, new HomeFragment(),"homee").hide(active);
            fragmentTransaction.commit();

        } else if (item.getItemId() == R.id.nav_wallet) {

            // WalletTransactions.backbutton.setVisibility(VISIBLE);
            Intent intent = new Intent(NavigationDrawerDashboard.this, WalletTransactions1.class);
            startActivity(intent);

            // getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new WalletTransactions1()).commit();


        } else if (item.getItemId() == R.id.nav_digital) {

            Intent intent = new Intent(NavigationDrawerDashboard.this, DigitalTransactions1.class);
            startActivity(intent);


        } else if (item.getItemId() == R.id.nav_commissions) {

            //getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new Commissions()).commit();
            Intent intent = new Intent(NavigationDrawerDashboard.this, Commissions1.class);
            intent.putExtra("Id",Id);
            startActivity(intent);


        } else if (item.getItemId() == R.id.nav_changepin) {


            Intent intent = new Intent(NavigationDrawerDashboard.this, ChangePin1.class);
            String id = getIntent().getStringExtra("Id");
            String password = getIntent().getStringExtra("Password");
            intent.putExtra("Id", id);
            intent.putExtra("Password", password);
            startActivity(intent);


        } else if (item.getItemId() == R.id.nav_notifications) {

            Intent intent = new Intent(NavigationDrawerDashboard.this, Notifications.class);
            intent.putExtra("Id",Id);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    //Do action that's needed
                    break;
                }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
