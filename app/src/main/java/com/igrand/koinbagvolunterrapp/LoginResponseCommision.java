package com.igrand.koinbagvolunterrapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class LoginResponseCommision {

    @SerializedName("status")
    private StatusBean3 status;
    @SerializedName("data")
    private List<DataBean3> data=null;

    public StatusBean3 getStatus() {
        return status;
    }

    public void setStatus(StatusBean3 status) {
        this.status = status;
    }

    public List<DataBean3> getData() {
        return data;
    }

    public void setData(DataBean3 data) {
        this.data = (List<DataBean3>) data;
    }

    public static class StatusBean3 {

        @SerializedName("code")
        private int code;
        @SerializedName("message")
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean3 {

        @SerializedName("amount")
        @Expose
        public String amount;
        @SerializedName("commission")
        @Expose
        public String commission;
        @SerializedName("order_id")
        @Expose
        public String orderId;
        @SerializedName("created_at")
        @Expose
        public String createdAt;

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("amount", amount).append("commission", commission).append("orderId", orderId).append("createdAt", createdAt).toString();
        }
    }
}
