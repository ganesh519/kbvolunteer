package com.igrand.koinbagvolunterrapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

class BrowseRecyclerAdapter1 extends RecyclerView.Adapter<BrowseRecyclerAdapter1.Holder> {

    ArrayList<TopupResponse> planDescriptions;
    Context context;
    String mobileNumber;
    String type = "";
    String amount,talktime,validity,longdes;
    FragmentTransaction transaction;

    public BrowseRecyclerAdapter1(Context context, ArrayList<TopupResponse> planDescriptions, String mobileNumber) {

       this.planDescriptions=planDescriptions;
       this.context=context;
       this.mobileNumber=mobileNumber;

    }

    @NonNull
    @Override
    public BrowseRecyclerAdapter1.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.browseplan, viewGroup, false);
        return new Holder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final BrowseRecyclerAdapter1.Holder holder, final int i) {


                    holder.desclong.setText(planDescriptions.get(i).getLongdecription());
                    holder.rechargeamount.setText(planDescriptions.get(i).getAmount());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                amount = planDescriptions.get(i).getAmount();
                talktime = planDescriptions.get(i).getTalktime();
                validity = planDescriptions.get(i).getValidity();
                longdes = planDescriptions.get(i).getLongdecription();


                Bundle bundle = new Bundle();
                bundle.putString("Amount",amount);
                bundle.putString("TalkTime",talktime);
                bundle.putString("Validity",validity);
                bundle.putString("Long",longdes);
                bundle.putString("Mobile",mobileNumber);
                CustomBottomSheetDialogFragment1 customBottomSheetDialogFragment1=new CustomBottomSheetDialogFragment1();
                customBottomSheetDialogFragment1.setArguments(bundle);
                customBottomSheetDialogFragment1.show(((BrowsePlans1)context).getSupportFragmentManager(), "Dialog");



            }
        });

    }



    @Override
    public int getItemCount() {
        return planDescriptions.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        LinearLayout listlayout;
        TextView desclong,rechargeamount,showdetails;
        public Holder(@NonNull View itemView) {
            super(itemView);
            listlayout=itemView.findViewById(R.id.listlayout);
            desclong=itemView.findViewById(R.id.desclong);
            rechargeamount=itemView.findViewById(R.id.rechargeamount);
            showdetails=itemView.findViewById(R.id.showdetails);

        }
    }
}
