package com.igrand.koinbagvolunterrapp;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MetroRechargePay extends BaseActivity {

    Button proceedtopaymetro;
    ImageView backblack19;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metro_recharge_pay);

        proceedtopaymetro=findViewById(R.id.proceedtopaymetro);
        backblack19=findViewById(R.id.backblack19);
        proceedtopaymetro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               BottomSheetDialog dialog = new BottomSheetDialog(MetroRechargePay.this);
                dialog.setContentView(R.layout.activity_promocode);
                //dialog.show();
                new CustomBottomSheetDialogFragment19().show(getSupportFragmentManager(), "Dialog");

               /* Intent intent=new Intent(MetroRechargePay.this,Payment.class);
                startActivity(intent);*/
            }
        });
        backblack19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MetroRechargePay.this,MetroRechageDetails.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
