package com.igrand.koinbagvolunterrapp;

import java.util.List;

public class PrepaidResponse {


    /**
     * status : {"code":200,"message":"Operator Data"}
     * data : [{"id":"1","operator_name":"Airtel","operator_code":"AT","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/aitrel"},{"id":"2","operator_name":"BSNL 3G","operator_code":"BSG","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/bsnl.webp"},{"id":"3","operator_name":"BSNL Recharge/Validity (RCV)","operator_code":"BSV","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/bsnl.webp"},{"id":"4","operator_name":"BSNL Special (STV)","operator_code":"BSS","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/bsnl.webp"},{"id":"5","operator_name":"BSNL TopUp","operator_code":"BSNL","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/bsnl.webp"},{"id":"6","operator_name":"Idea","operator_code":"ID","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/idea.webp"},{"id":"7","operator_name":"MTNL Delhi","operator_code":"MD","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/mtnl.webp"},{"id":"8","operator_name":"MTNL Delhi Special","operator_code":"MDS","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/mtnl.webp"},{"id":"9","operator_name":"MTNL Mumbai","operator_code":"MM","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/mtnl.webp"},{"id":"10","operator_name":"MTNL Mumbai Special","operator_code":"MMS","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/mtnl.webp"},{"id":"11","operator_name":"Reliance Jio","operator_code":"JIO","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/jio.webp"},{"id":"12","operator_name":"Tata Docomo - Special Tariff","operator_code":"TSP","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/tata.webp"},{"id":"13","operator_name":"Tata Docomo - Talktime","operator_code":"TD","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/tata.webp"},{"id":"14","operator_name":"Tata Walky","operator_code":"TW","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/tata.webp"},{"id":"15","operator_name":"Telenor-Airtel","operator_code":"UN","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/aitrel"},{"id":"16","operator_name":"Telenor-Airtel Special","operator_code":"UNS","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/aitrel"},{"id":"17","operator_name":"Vodafone","operator_code":"VF","operator_type":"prepaid-mobile","image":"http://igranddeveloper.live/kbig/uploads/voda.png"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Operator Data
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * operator_name : Airtel
         * operator_code : AT
         * operator_type : prepaid-mobile
         * image : http://igranddeveloper.live/kbig/uploads/aitrel
         */

        private String id;
        private String operator_name;
        private String operator_code;
        private String operator_type;
        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOperator_name() {
            return operator_name;
        }

        public void setOperator_name(String operator_name) {
            this.operator_name = operator_name;
        }

        public String getOperator_code() {
            return operator_code;
        }

        public void setOperator_code(String operator_code) {
            this.operator_code = operator_code;
        }

        public String getOperator_type() {
            return operator_type;
        }

        public void setOperator_type(String operator_type) {
            this.operator_type = operator_type;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
